<?php

namespace App\Mail;

use App\models\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMail extends Mailable
{
    public $data;
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from($this->data['email'])->subject('New Customer Equiry')->view('frontend.mails.contact')->with('data', $this->data);
        $contact=\App\Model\Contact::create([
            'fname'=>$this->data['fname'],
            'lname'=>$this->data['lname'],
            'email'=>$this->data['email'],
            'contact'=>$this->data['contact'],
            'message'=>$this->data['message'],
        ]);
        return $contact;
    }
}
