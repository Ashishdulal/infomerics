<?php

namespace App\Http\Controllers\frontend;

use App\Model\Slider;
use App\Model\Image;
use App\Model\Blog;
use App\Model\Category;
use App\Model\Event;
use App\Model\Video;
use App\Model\Expert;
use App\Model\Institutional;
use App\Model\Management;
use App\Model\Testimonial;
use App\Model\WebInfo;
use App\Model\Service;
use App\Model\Rating;
use App\Model\Career;
use App\Model\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FrontendController extends Controller
{
    public function commingSoon(){
        return view('comingsoon.index');
    }

    public function index(){
        $data['webinfo'] = WebInfo::first();
        if ($data['webinfo']->comming_soon == 1) {
        return view('comingsoon.index');
        }
        $data['title'] = explode(' ', trim(WebInfo::first()->web_title)); 
        $data['categories'] = Category::latest()->get();
        $data['blogs'] = Blog::latest()->get();
        $data['sliders'] = Slider::latest()->get();
        $data['testimonials'] = Testimonial::latest()->get();
        $data['images'] = Image::latest()->get();
        $data['videos'] = Video::latest()->get();
        $data['pressRelease'] = Event::latest()->get();
        $data['managements'] = Management::latest()->get();
        $data['experts'] = Expert::latest()->get();
        $data['services'] = Service::latest()->get();
        return view('frontend.partials.homepage.index',$data);
    }

    public function about(){
        $data['webinfo'] = WebInfo::first();
        $data['managements'] = Management::latest()->get();
        return view('frontend.partials.homepage.about',$data);
    }
    public function service(){
        $data['services'] = Service::paginate(10);
        $data['webinfo'] = WebInfo::first();
        return view('frontend.partials.homepage.service',$data);
    }
    public function serviceDetail($id){
        $data['service'] = Service::find($id);
        $data['webinfo'] = WebInfo::first();
        return view('frontend.partials.homepage.service-detail',$data);
    }
    public function ratings(){
        $data['ratings'] = Rating::latest()->get();
        $data['webinfo'] = WebInfo::first();
        return view('frontend.partials.homepage.rating',$data);
    }
    public function ratingsSearch(Request $request){
        if ($request->search) {
            if ($request->search) {
                $data['rateSearch'] = Rating::where('company_name', 'like', '%' . $request->search . '%')->get();
           }
       }
       $data['webinfo'] = WebInfo::first();
       return view('frontend.partials.homepage.rating-search',$data);
   }
   public function ratingsDetails(Request $request,$id){
    $data['rating'] = Rating::find($id);
    $data['webinfo'] = WebInfo::first();
    return view('frontend.partials.homepage.rating-details',$data);
}
public function pressRelease(Request $request){
    $data['pressRelease'] = Event::latest()->paginate(5);
    $data['webinfo'] = WebInfo::first();
    return view('frontend.partials.homepage.press-release',$data);
}
public function pressReleaseDetail($id){
    $data['pressRelease'] = Event::find($id);
    $data['webinfo'] = WebInfo::first();
    return view('frontend.partials.homepage.press-release-detail',$data);
}
public function careers(){
    $data['careers'] = Career::orderBy('position','ASC')->where('status',1)->get();
    $data['webinfo'] = WebInfo::first();
    return view('frontend.partials.homepage.career',$data);
}
public function careerDetail($id){
    $data['career'] = Career::find($id);
    $data['webinfo'] = WebInfo::first();
    return view('frontend.partials.homepage.career-detail',$data);
}
public function careerApply(Request $request){
    $data['career'] = Career::find($request->id);
    $data['careers'] = Career::orderBy('position','ASC')->where('status',1)->get();
    $data['webinfo'] = WebInfo::first();
    return view('frontend.partials.homepage.career-apply',$data);
}
public function contact(Request $request){
    $data['career'] = Career::find($request->id);
    $data['careers'] = Career::orderBy('position','ASC')->where('status',1)->get();
    $data['webinfo'] = WebInfo::first();
    return view('frontend.partials.homepage.contact',$data);
}
public function contactUs(Request $request){
 $request->validate([
    'name' => 'required|max:120',
    'business_name'  => 'required',
    'email'     => 'required|email',
    'phone'     => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
    'business_sector' => 'required',
    'message' => 'required',
]);
 $data = Contact::create($request->all());
 $this->send($data);
 return redirect()->back()->with('status','Thank you for contacting. We will get back to you as soon as possible.');
}

      function send($data)
  {
    $pageSetting = WebInfo::first();
    $txt1 = '<html>
    <head>  
    </head>
    <body>
    <p>Hi, This is '. $data['name'] .'.</p>
    <p>Email:'. $data['email'] .'<br><br>Phone:'. $data['phone'] .'</p>
    <p>Subject:<br>'. $data['topic'] .'</p>
    <p>Subject:<br>'. $data['business_name'] .'</p>
    <p>Subject:<br>'. $data['business_sector'] .'</p>
    <p>Message:<br>'. $data['message'] .'</p>
    <p>It would be appriciative, if i receive the reply soon.</p>
    </body>
    </html>';       

    $to = $pageSetting->mail;
    $subject = "Rating / Contact";

    $headers = "From:" . $pageSetting->web_title . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";

    $result=   mail($to,$subject,$txt1,$headers);
    $result=   mail($data['email'],$subject,$txt1,$headers);
     return redirect()->back()->with('status','Thank you for contacting. We will get back to you as soon as possible.');
  }

}
