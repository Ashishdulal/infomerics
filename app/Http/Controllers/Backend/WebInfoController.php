<?php

namespace App\Http\Controllers\Backend;

use App\Model\Event;
use App\Model\WebInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WebInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data['webInfo'] = WebInfo::first();
        $data['events'] = Event::all();
        return view('backend.partials.webInfo.webInfo',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function career(Request $request)
    {
        $data['webInfo'] = WebInfo::first();
        return view('backend.partials.career.career',$data);
    }

    public function setting(Request $request)
    {
        $data['webInfo'] = WebInfo::first();
        return view('backend.partials.settings.setting',$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WebInfo  $webInfo
     * @return \Illuminate\Http\Response
     */
    public function show(WebInfo $webInfo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WebInfo  $webInfo
     * @return \Illuminate\Http\Response
     */
    public function edit(WebInfo $webInfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WebInfo  $webInfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->comming_soon == 1) {
            $comming_soon = 1;
        }
        else{
            $comming_soon = 0;
        }
        if($request->file('about_us_image')){
            $image = $request->file('about_us_image');
            $imageName = 'info' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/webinfo'), $imageName);
        }
        else{
         $imageName = (WebInfo::first() ? WebInfo::first()->about_us_image :'');
     }
     if($request->file('our_mission_image')){
            $image = $request->file('our_mission_image');
            $missionimageName = 'mission' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/webinfo'), $missionimageName);
        }
        else{
         $missionimageName = (WebInfo::first() ? WebInfo::first()->our_mission_image :'');
     }
     if($request->file('life_at_infomerics_image')){
            $image = $request->file('life_at_infomerics_image');
            $lifeimageName = 'life' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/webinfo'), $lifeimageName);
        }
        else{
         $lifeimageName = (WebInfo::first() ? WebInfo::first()->life_at_infomerics_image :'');
     }
     if($request->file('our_people_image')){
            $image = $request->file('our_people_image');
            $ourImageName = 'people' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/webinfo'), $ourImageName);
        }
        else{
         $ourImageName = (WebInfo::first() ? WebInfo::first()->our_people_image :'');
     }
     if($request->file('logo')){
            $image = $request->file('logo');
            $logoimageName = 'logo' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/webinfo'), $logoimageName);
        }
        else{
         $logoimageName = (WebInfo::first() ? WebInfo::first()->logo :'');
     }
     if($request->file('logo_white')){
            $image = $request->file('logo_white');
            $whiteimageName = 'logo_white' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/webinfo'), $whiteimageName);
        }
        else{
         $whiteimageName = (WebInfo::first() ? WebInfo::first()->logo_white :'');
     }
     if($request->file('our_vision_image')){
            $image = $request->file('our_vision_image');
            $visionimageName = 'vision' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/webinfo'), $visionimageName);
        }
        else{
         $visionimageName = (WebInfo::first() ? WebInfo::first()->our_vision_image :'');
     }
     if($id == 'create'){    
        $abt = WebInfo::create($request->all());
        $abt->about_us_image = $imageName;
        $abt->our_mission_image = $missionimageName;
        $abt->life_at_infomerics_image = $lifeimageName;
        $abt->our_people_image = $ourImageName;
        $abt->logo = $logoimageName;
        $abt->logo_white = $whiteimageName;
        $abt->our_vision_image = $visionimageName;
        $abt->comming_soon = $comming_soon;
        $abt->save();
        return redirect()->back();
    }
    else{
        $ab = WebInfo::findOrFail($id);
        $ab->update($request->all());
        $ab->about_us_image = $imageName;
        $ab->our_mission_image = $missionimageName;
        $ab->life_at_infomerics_image = $lifeimageName;
        $ab->our_people_image = $ourImageName;
        $ab->logo = $logoimageName;
        $ab->logo_white = $whiteimageName;
        $ab->our_vision_image = $visionimageName;
        $ab->comming_soon = $comming_soon;
        $ab->save();
        return redirect()->back();   
    }
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WebInfo  $webInfo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Webinfo=WebInfo::findOrFail($id);
        if(isset($webinfo->about_us_image)){
            $path = public_path('/images/Webinfo/'.$Webinfo['about_us_image']);
            unlink($path);
        }
        $Webinfo->delete();
        return redirect()->back();
    }
}
