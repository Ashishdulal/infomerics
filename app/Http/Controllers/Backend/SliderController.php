<?php

namespace App\Http\Controllers\Backend;

use App\Model\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data['sliders'] = Slider::latest()->get();
        return view('backend.partials.slider.slider',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $image = $request->file('slider_image');
        $file = New Slider();

        $imageName = time() . ('.' . $image->getClientOriginalExtension());
        $image->move(public_path('images/slider'), $imageName);
        $file->create([
            'slider_image' => $imageName,
            'title' => $request['title'],
            'description' => $request['description'],
            'button_name' => $request['button_name'],
            'url_link' => $request['url_link'],
            'status' => true,

        ]);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(slider $slider)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = $request->file('slider_image');
        $file = Slider::find($id);
        $path = public_path() . '/images/slider/' . $file['slider_image'];
        if ($image) {
            if(isset($file['slider_image'])) {
                unlink($path);
            }
            $imageName = time() . ('.'.$image->getClientOriginalExtension());
            $image->move(public_path('images/slider'), $imageName);
        } else {
            $imageName = $file['slider_image'];
        }
        $file->update([
            'slider_image' => $imageName,
            'title' => $request['title'],
            'description' => $request['description'],
            'button_name' => $request['button_name'],
            'url_link' => $request['url_link'],
        ]);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $slider = Slider::find($id);
      $path = public_path('/images/slider/' . $slider['image']);
      if(isset($slider['image'])) {
        unlink($path);
    }        
    $slider->delete();
    return redirect()->back();
}
}
