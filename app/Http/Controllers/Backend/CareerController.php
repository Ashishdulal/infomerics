<?php

namespace App\Http\Controllers\Backend;

use App\Model\Career;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class CareerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data['careers'] = Career::orderBy('position','ASC')->get();
        return view('backend.partials.career.list.careerlist',$data);
    }

    public function sort(Request $request){
       foreach($request->carer as $carer)
       {
         $career=Career::find($carer['id']);
         $career->position=$carer['position'];
         $career->save();
     }
     return json_encode(['status' => 'success', 'value' => 'Successfully reordered.'], 200);
 }

 public function statusChange($id){
    $career = Career::findOrFail($id);
    if ($career->status == 1) {
        $career->status = 0;
    } else {
        $career->status = 1;
    }
    $career->update();
    return redirect()->back()->withStatus(__('Active status changed successfully.'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['position'] = (Career::max('position'))+1;
        Career::create($request->all());
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function show(Career $career)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function edit(Career $career)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Career::findOrFail($id)->update($request->all());
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function destroy(Career $career)
    {
        //
    }
}
