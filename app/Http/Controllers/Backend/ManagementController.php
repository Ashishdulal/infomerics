<?php

namespace App\Http\Controllers\Backend;

use App\Model\Management;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $managements = Management::orderBy('created_at', 'desc')->get();
        return view('backend.partials.management.management')->with(['managements' => $managements]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file('image');
        $file = New Management();

        $imageName = time() . '.'.$image->getClientOriginalExtension();
        $image->move(public_path('images/management'), $imageName);
        $file->create([
            'image' => $imageName,
            'category' => $request['category'],
            'name' => $request['name'],
            'designation' => $request['designation'],
            'bio' => $request['bio'],
            'status' => true,

        ]);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = $request->file('image');
        $file = Management::find($id);
        $path = public_path() . '/images/management/'.$file['image'];
        if ($image) {
            if(file_exists($path)){
            unlink($path);
            }
            $imageName = time() . ('.'.$image->getClientOriginalExtension());
            $image->move(public_path('images/management'), $imageName);
        } else {
            $imageName = $file['image'];
        }


        $file->update([
            'image' => $imageName,
            'category' => $request['category'],
            'name' => $request['name'],
            'designation' => $request['designation'],
            'bio' => $request['bio'],
        ]);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $management=Management::find($id);
        $path = public_path('/images/management/'.$management['image']);
        if(file_exists($path)){
        unlink($path);
        }
        $management->delete();
        return redirect()->back();
    }
}