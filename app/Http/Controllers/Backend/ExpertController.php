<?php

namespace App\Http\Controllers\Backend;

use App\Model\Expert;
use App\Model\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExpertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $experts = Expert::orderBy('created_at', 'desc')->get();
        return view('backend.partials.expert.expert')->with(['experts' => $experts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file('image');
        $file = New Expert();

        $imageName = time() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('images/expert'), $imageName);
        $file->create([
            'image' => $imageName,
            'name' => $request['name'],
            'designation' => $request['designation'],
            'year' => $request['year'],
            'bio' => $request['bio'],
            'status' => true,

        ]);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = $request->file('image');
        $file = Expert::find($id);
        $path = public_path() . '/images/expert/'.$file['image'];
        if ($image) {
            unlink($path);
            $imageName = time() . ('.'.$image->getClientOriginalExtension());
            $image->move(public_path('images/expert'), $imageName);
        } else {
            $imageName = $file['image'];
        }


        $file->update([
            'image' => $imageName,
            'category' => $request['category'],
            'name' => $request['name'],
            'designation' => $request['designation'],
            'year' => $request['year'],
            'bio' => $request['bio'],
        ]);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expert=Expert::find($id);
        $path = public_path('/images/expert/'.$expert['image']);
        unlink($path);
        $expert->delete();
        return redirect()->back();
    }
}
