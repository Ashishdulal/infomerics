<?php

namespace App\Http\Controllers\Backend;

use App\Model\Expert;
use App\Model\Category;
use App\Model\Institutional;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InstitutionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $institutionals = Institutional::orderBy('created_at', 'desc')->get();
        return view('backend.partials.institutional.institutional')->with(['institutionals' => $institutionals]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file('image');
        $file = New Institutional();

        $imageName = time() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('images/institutional'), $imageName);
        $file->create([
            'image' => $imageName,
            'category' => $request['category'],
            'name' => $request['name'],
            'email' => $request['email'],
            'address' => $request['address'],
            'contact' => $request['contact'],
            'proprietor' => $request['proprietor'],
            'position' => $request['position'],
            'involvement' => $request['involvement'],
            'status' => true,

        ]);
        return redirect()->back();    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = $request->file('image');
        $file = Institutional::find($id);
        $path = public_path() . '/images/institutional/'.$file['image'];
        if ($image) {
            unlink($path);
            $imageName = time() . ('.'.$image->getClientOriginalExtension());
            $image->move(public_path('images/institutional'), $imageName);
        } else {
            $imageName = $file['image'];
        }


        $file->update([

            'image' => $imageName,
            'category' => $request['category'],
            'name' => $request['name'],
            'email' => $request['email'],
            'address' => $request['address'],
            'contact' => $request['contact'],
            'proprietor' => $request['proprietor'],
            'position' => $request['position'],
            'involvement' => $request['involvement'],
        ]);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $institutional=Institutional::find($id);
        $path = public_path('/images/institutional/'.$institutional['image']);
        unlink($path);
        $institutional->delete();
        return redirect()->back();    }
}
