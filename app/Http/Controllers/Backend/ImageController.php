<?php

namespace App\Http\Controllers\Backend;

use App\Model\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $images = Image::orderBy('created_at', 'desc')->get();
        return view('backend.partials.gallery.image.image')->with(['images' => $images]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file('image');
        $file = New Image();

        $imageName = time() . ($image->getClientOriginalName());
        $image->move(public_path('images/image'), $imageName);
        $file->create([
            'image' => $imageName,
            'title' => $request['title'],
            'description' => $request['description'],
            'status' => true,

        ]);
        return redirect()->back();    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = $request->file('image');
        $file = Image::find($id);
        $path = public_path() . '/images/image/'.$file['image'];
        if ($image) {
            unlink($path);
            $imageName = time() . ($image->getClientOriginalName());
            $image->move(public_path('images/image'), $imageName);
        } else {
            $imageName = $file['image'];
        }


        $file->update([
            'image' => $imageName,
            'title' => $request['title'],
            'description' => $request['description'],
        ]);
        return redirect()->back();    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image=Image::find($id);
        $path = public_path() . '/images/image/'.$image['image'];
        unlink($path);
        $image->delete();
        return redirect()->back();
    }
}
