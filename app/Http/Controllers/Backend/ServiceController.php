<?php

namespace App\Http\Controllers\Backend;

use App\Model\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data['services'] = Service::all();
        return view('backend.partials.service.service',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file('icon');
        $file = New Service();

        $imageName = 'service' . time() . ('.' . $image->getClientOriginalExtension());
        $image->move(public_path('images/service'), $imageName);
        $file->create([
            'icon' => $imageName,
            'name' => $request['name'],
            'description' => $request['description'],
            'button_name' => $request['button_name'],
            'button_url' => $request['button_url'],
            'cat_id' => $request['cat_id'],
            'status' => true,

        ]);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = $request->file('icon');
        $file = Service::find($id);
        $path = public_path() . '/images/service/' . $file['icon'];
        if ($image) {
            if(isset($file['icon'])) {
                unlink($path);
            }
            $imageName = 'service' . time() . ('.'.$image->getClientOriginalExtension());
            $image->move(public_path('images/service'), $imageName);
        } else {
            $imageName = $file['icon'];
        }


        $file->update([
            'icon' => $imageName,
            'name' => $request['name'],
            'description' => $request['description'],
            'button_name' => $request['button_name'],
            'button_url' => $request['button_url'],
            'cat_id' => $request['cat_id'],
        ]);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::find($id);
        $path = public_path('/images/service/' . $service['icon']);
        if(isset($service['icon'])) {
            unlink($path);
        }        
        $service->delete();
        return redirect()->back();
    }
}
