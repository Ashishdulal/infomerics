<?php

namespace App\Http\Controllers\Backend;

use App\Model\Blog;
use App\Model\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $blogs = Blog::orderBy('created_at', 'desc')->get();
        $categories = Category::orderBy('created_at', 'desc')->get();
        return view('backend.partials.blog.blog')->with(['blogs' => $blogs, 'categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file('image');
        $file = New Blog();

        $imageName = time() . ('.' . $image->getClientOriginalExtension());
        $image->move(public_path('images/blog'), $imageName);
        $file->create([
            'image' => $imageName,
            'category_id' => $request['category'],
            'author_name' => $request['author_name'],
            'title' => $request['title'],
            'description' => $request['description'],
            'status' => true,

        ]);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = $request->file('image');
        $file = Blog::find($id);
        $path = public_path() . '/images/blog/' . $file['image'];
        if ($image) {
            if(isset($file['image'])) {
                unlink($path);
            }
            $imageName = time() . ('.'.$image->getClientOriginalExtension());
            $image->move(public_path('images/blog'), $imageName);
        } else {
            $imageName = $file['image'];
        }


        $file->update([
            'image' => $imageName,
            'author_name' => $request['author_name'],
            'category_id' => $request['category'],
            'title' => $request['title'],
            'description' => $request['description'],
        ]);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::find($id);
        $path = public_path('/images/blog/' . $blog['image']);
        if(isset($blog['image'])) {
            unlink($path);
        }        $blog->delete();
        return redirect()->back();
    }
}
