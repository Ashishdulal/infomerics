<?php

namespace App\Http\Controllers\Backend;

use App\Model\Rating;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data['ratings'] = Rating::latest()->get();
        return view('backend.partials.rating.rating',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function statusChange($id){
        $rate = Rating::findOrFail($id);
        if ($rate->status == 1) {
            $rate->status = 0;
        } else {
            $rate->status = 1;
        }
        $rate->update();
        return redirect()->back()->withStatus(__('Active status changed successfully.'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $pdf = $request->file('pdf');
       $rating =  new Rating();

       $pdfName = time() . ('.'.$pdf->getClientOriginalName());
       $pdf->move(public_path('images/rating'), $pdfName);
       $rating->create([
        'pdf' => $pdfName,
        'company_name'  => $request['company_name'],
        'industry'  => $request['industry'],
        'instrument_category'  => $request['instrument_category'],
        'rating'  => $request['rating'],
        'outlook'  => $request['outlook'],
        'details'  => $request['details'],
        'status' => true,
    ]);
       return redirect()->back();
   }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function show(Rating $rating)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function edit(Rating $rating)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $pdf = $request->file('pdf');
        $rating =  Rating::findOrFail($id);
        if ($pdf) {
        $pdfName = time() . ('.'.$pdf->getClientOriginalName());
        $pdf->move(public_path('images/rating'), $pdfName);
        }
        else{
        $pdfName = $rating->pdf;
        }
        $rating->update([
            'pdf' => $pdfName,
            'company_name'  => $request['company_name'],
            'industry'  => $request['industry'],
            'instrument_category'  => $request['instrument_category'],
            'rating'  => $request['rating'],
            'outlook'  => $request['outlook'],
            'details'  => $request['details'],
        ]);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rating $rating)
    {
        //
    }
}
