<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\CareerApplies;
use App\Model\WebInfo;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class CareerAppliesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data['applicants'] = CareerApplies::latest()->get();
        return view('backend.partials.career.applicant.career-applicant',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function statusUpdate(Request $request){
        $status = CareerApplies::findOrFail($request->id);
        $status->status = $request->status;
        $status->save();
        return json_encode(['status' => 'success', 'value' => 'Status Changed.'], 200);
    }


    public function store(Request $request)
    {
        $request->validate([
            'full_name' => 'required|max:120',
            'cv_file'   => 'required',
            'cover_letter'  => 'required',
            'email'     => 'required|email',
            'phone'     => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'career' => 'required',
            'expected_salary' => 'required',
        ]);
        $file = $request->file('cv_file');
        $cFile = $request->file('cover_letter');
        $career = New CareerApplies();

        $fileName = 'applicant' . time() . ('.' . $file->getClientOriginalExtension());
        $file->move(public_path('files/career/applicant'), $fileName);
        $cFileName = 'applicant' . time() . ('.' . $cFile->getClientOriginalExtension());
        $cFile->move(public_path('files/career/applicant'), $cFileName);
        $data = $career->create([
            'cv_file' => $fileName,
            'cover_letter' => $cFileName,
            'full_name' => $request['full_name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'career_id' => $request['career'],
            'expected_salary' => $request['expected_salary'],
            'additional_message' => $request['additional_message'],
            'status' => 1,

        ]);
        $this->sendJob($data);
        return redirect()->back()->with('status','Thank you for applying. We will get back to you as soon as possible.');
 //         if($result == true){
 //  return redirect()->back()->with('status','Thank you for contacting. We will get back to you as soon as possible.');
 // }
 // else{
 //       return redirect()->back()->with('status','Error in sending message. Please try again.');
 // }
    }
    function sendJob($data)
    {
        $pageSetting = WebInfo::first();;

        $txt1 = '<html>
        <head>  
        </head>
        <body>
        <p>Hi, This is '. $data['full_name'] .'.</p>
        <p>Email:'. $data['email'] .'<br><br>Phone:'. $data['phone'] .'</p>
        <p>Job Needed:'. $data->career->job_title .'</p>
        <p>Expected Salary:<br>'. $data['expected_salary'] .'</p>
        <p>It would be appriciative, if i receive the reply soon.</p>
        </body>
        </html>';       

        $to = $pageSetting->mail;
        $subject = "Inquiry";

        $headers = "From:" . $pageSetting->web_title . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";
        $files = [
            asset('files/career/applicant/'.$data['cv_file']),
            asset('files/career/applicant/'.$data['cover_letter']),
        ];
        $result=   mail($to,$subject,$txt1,$headers);
        $result=   mail($data['email'],$subject,$txt1,$headers);
        return redirect()->back()->with('status','Thank you for applying. We will get back to you as soon as possible.');
        // return back()->with('success','Thanks for contacting us. We will reach out to you soon!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
