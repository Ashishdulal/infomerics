<?php

namespace App\Http\Controllers\Backend;

use App\Model\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Testing\File;

class EventController extends Controller
{
    /**
     * EventController constructor.
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $events = Event::orderBy('created_at', 'desc')->get();
        return view('backend.partials.press-release.press-release')->with(['events' => $events]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:pdf,xlx,csv|max:2048',
        ]);

        $image = $request->file('file');
        $files = New Event();

        $imageName = time() . ('.'.$image->getClientOriginalName());
        $image->move(public_path('images/event'), $imageName);
        $files->create([
            'image' => $imageName,
            'author_name' => $request['author'],
            'title' => $request['title'],
            'description' => $request['description'],
            'status' => true,

        ]);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $image = $request->file('image');
        $file = Event::find($id);
        $path = public_path() . '/images/event/'.$file['image'];
        if ($image) {
            $request->validate([
                'image' => 'required|mimes:pdf,xlx,csv|max:2048',
            ]);
            if(isset($file['image'])) {
                unlink($path);
            }
            $imageName = time() . ('.'.$image->getClientOriginalExtension());
            $image->move(public_path('/images/event'), $imageName);
        } else {
            $imageName = $file['image'];
        }
        $file->update([
            'image' => $imageName,
            'author_name' => $request['author'],
            'title' => $request['title'],
            'description' => $request['description'],
        ]);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event=Event::find($id);
        $path = public_path('/images/event/'.$event['image']);
        if(isset($event['image'])) {
            unlink($path);
        }
        $event->delete();
        return redirect()->back();
    }
}
