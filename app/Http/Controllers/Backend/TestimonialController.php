<?php

namespace App\Http\Controllers\Backend;

use App\Model\Testimonial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $testimonials = Testimonial::orderBy('created_at', 'desc')->get();
        return view('backend.partials.testimonial.testimonial')->with(['testimonials' => $testimonials]);    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = $request->file('image');
        $file = New Testimonial();

        $imageName = time() . '.'.$image->getClientOriginalExtension();
        $image->move(public_path('images/testimonial'), $imageName);
        $file->create([
            'image' => $imageName,
            'name' => $request['name'],
            'designation' => $request['designation'],
            'testimonial' => $request['testimonial'],
            'status' => true,

        ]);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = $request->file('image');
        $file = Testimonial::find($id);
        $path = public_path() . '/images/testimonial/'.$file['image'];
        if ($image) {
            unlink($path);
            $imageName = time() . ('.'.$image->getClientOriginalExtension());
            $image->move(public_path('images/testimonial'), $imageName);
        } else {
            $imageName = $file['image'];
        }


        $file->update([
            'image' => $imageName,
            'name' => $request['name'],
            'designation' => $request['designation'],
            'testimonial' => $request['testimonial'],
        ]);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testimonial=Testimonial::find($id);
        $path = public_path('/images/testimonial/'.$testimonial['image']);
        unlink($path);
        $testimonial->delete();
        return redirect()->back();    }
}
