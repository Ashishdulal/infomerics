<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $fillable = ['company_name','industry','instrument_category','rating','outlook','details','pdf','status'];

   function user(){
        return $this->belongsTo('App\User');
    }
}
