<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WebInfo extends Model
{
    protected $fillable = ['our_mission','our_mission_image','our_vision','our_vision_image','life_at_infomerics','life_at_infomerics_image','our_people','our_people_image','logo','logo_white','mail','meta_keywords','meta_description','about_us','about_us_image','nepal_contact','india_contact','map','rating','web_title','text1','text2','comming_soon','service_menu'];
}
