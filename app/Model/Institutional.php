<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Institutional extends Model
{
    protected $fillable = ['name', 'address', 'contact', 'email', 'proprietor', 'position', 'involvement', 'image', 'status'];

}
