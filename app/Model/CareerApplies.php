<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CareerApplies extends Model
{
    protected $fillable = ['full_name','email','phone','career_id','expected_salary','cv_file','cover_letter','additional_message','status'];

     public function career()
    {
        return $this->belongsTo(Career::class,'career_id','id','career_id');
    }
    public function statusPrint($id)
    {
    	switch ($id) {
    		case '1':
    			return '<span class="badge badge-secondary">Requested</span>';
    			break;
    		case '2':
    			return '<span class="badge badge-secondary">Seen</span>';
    			break;
    		case '3':
    			return '<span class="badge badge-primary">Approved</span>';
    			break;
    			case '4':
    			return '<span class="badge badge-secondary">Rejected</span>';
    			break;
    		default:
    			return '<span class="badge badge-secondary">N/A</span>';
    			break;
    	}
    }
}
