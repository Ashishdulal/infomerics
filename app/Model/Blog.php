<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable=['image','category_id','author_name','title','description','status'];


    public function category()
    {
        return $this->belongsTo(Category::class,'category_id','id','category_id');
    }
}
