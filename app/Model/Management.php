<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Management extends Model
{
	protected $table = 'managements';
    protected $fillable=['category','name','designation','bio','image','status'];

}
