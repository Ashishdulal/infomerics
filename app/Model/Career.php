<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    protected $fillable = ['job_title','job_description','candidate','job_responsibilities','start_date','end_date','position','status'];
}
