<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('web_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('our_mission')->nullable();
            $table->string('our_mission_image')->nullable();
            $table->text('our_vision')->nullable();
            $table->string('our_vision_image')->nullable();
            $table->text('life_at_infomerics')->nullable();
            $table->string('life_at_infomerics_image')->nullable();
            $table->text('our_people')->nullable();
            $table->string('our_people_image')->nullable();
            $table->string('logo')->nullable();
            $table->string('logo_white')->nullable();
            $table->string('mail')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('about_us')->nullable();
            $table->string('about_us_image')->nullable();
            $table->text('nepal_contact')->nullable();
            $table->text('india_contact')->nullable();
            $table->text('map')->nullable();
            $table->text('rating')->nullable();
            $table->text('web_title')->nullable();
            $table->text('text1')->nullable();
            $table->text('text2')->nullable();
            $table->integer('comming_soon')->nullable();
            $table->integer('service_menu')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('web_infos');
    }
}
