@extends('layouts.app2') @section('content')
<div
    class="container align-items-center d-flex justify-content-center"
    style="height: 90vh"
>
    <div class="row justify-content-center login_admin" style="width: 85%">
        <div class="col-md-8 col-12">
            <div class="card row" style="padding: 50px 50px">
                <div
                    class="col-12"
                    style="text-align: center; margin-bottom: 10px"
                >
                    <b style="color: rgb(0, 0, 0); font-size: 18px">{{
                        "Admin login"
                    }}</b>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label
                                for="email"
                                class="col-12 col-md-4 col-form-label text-md-left"
                                >{{ __("E-Mail Address") }}</label
                            >

                            <div class="col-12 col-md-8">
                                <input
                                    id="email"
                                    type="email"
                                    class="form-control @error('email') is-invalid @enderror"
                                    name="email"
                                    value="{{ old('email') }}"
                                    required
                                    autocomplete="email"
                                    autofocus
                                />

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label
                                for="password"
                                class="col-12 col-md-4 col-form-label text-md-left"
                                >{{ __("Password") }}</label
                            >

                            <div class="col-12 col-md-8">
                                <input
                                    id="password"
                                    type="password"
                                    class="form-control @error('password') is-invalid @enderror"
                                    name="password"
                                    required
                                    autocomplete="current-password"
                                />

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <input class="form-check-input"
                                    type="checkbox" name="remember"
                                    id="remember"
                                    {{ old("remember") ? "checked" : "" }}>

                                    <label
                                        class="form-check-label"
                                        for="remember"
                                    >
                                        {{ __("Remember Me") }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div
                            class="form-group row mb-0"
                            style="margin-top: 70px"
                        >
                            <div class="col-md-12">
                                <button
                                    class="col-12 bg-primary"
                                    type="submit"
                                    class="btn btn-primary b_remove"
                                    style="
                                        padding: 8px 8px;
                                        border-radius: 10px;
                                        border: none;
                                        outline: none;
                                        color: white;
                                        font-size: 16px;
                                    "
                                >
                                    {{ __("Login") }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
