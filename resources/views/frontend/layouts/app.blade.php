<!DOCTYPE html>
<html lang="en">

<head>
	<?php 
    $webinfo = App\Model\WebInfo::first();
    ?>
	<title>{{$webinfo ? $webinfo->web_title : ''}}</title>

	<!--Seo Keywords-->
	<meta name="keywords" content="{{$webinfo ? $webinfo->meta_keywords : ''}}">
	<meta name="description" content="{{$webinfo ? $webinfo->meta_description : ''}}">

	<!-- Header Scripts -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="{{asset('css/main.css')}}">
	<link rel="stylesheet" href="{{asset('css/bootstrap-icons.css')}}">
	<link rel="stylesheet" href="{{asset('css/swiper-bundle.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/multilayer_resp.css')}}">

	  <link rel="shortcut icon" href="{{asset('/images/favicon.jpg')}}" type="image/png" />

</head>

<body>
	<div id="wrapper">
		@include('frontend.includes.header')

		@yield('content')

		@include('frontend.includes.footer')
	</div>

	<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
	<script>
		// BOOTSTRAP TOOLTIP INITIATE
		var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
		var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
			return new bootstrap.Tooltip(tooltipTriggerEl)
		})
	</script>
	<script src="{{asset('js/swiper-bundle.min.js')}}"></script>
	<script>
		// SWIPER JS
		var heroSlider = new Swiper('.hero-slider', {
			loop: true,
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			autoplay: {
				delay: 3000,
				disableOnInteraction: true,
			},
		});
		var cardsCarouselThreeCols = new Swiper('.three-col-carousel', {
			loop: true,
			slidesPerView: 1,
			spaceBetween: 36,
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
			breakpoints: {
				576: {
					slidesPerView: 1,
					spaceBetween: 48,
				},
				992: {
					slidesPerView: 2,
					spaceBetween: 48,
				},
				1200: {
					slidesPerView: 3,
					spaceBetween: 36,
				},
			}
		});
		var cardsCarouselFourCols = new Swiper('.four-col-carousel', {
			loop: true,
			slidesPerView: 1,
			spaceBetween: 36,
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			breakpoints: {
				576: {
					slidesPerView: 1,
					spaceBetween: 48,
				},
				768: {
					slidesPerView: 2,
					spaceBetween: 48,
				},
				992: {
					slidesPerView: 3,
					spaceBetween: 48,
				},
				1200: {
					slidesPerView: 4,
					spaceBetween: 36,
				},
			}
		});
	</script>
	<script src="{{asset('js/jquery-3.5.1.min.js')}}"></script>
	<script src="{{asset('js/main_multi.js')}}"></script>
	<script>
		$(document).ready(function () {
			// BOOTSTRAP NAV STICKY FIX
			$('body').css('padding-top', $('#header').outerHeight() + 'px');
		});

		// BOOTSTRAP NAV OFF CANVAS
		$(function () {
			$(".header-navbar-toggler").on("click", function (e) {
				e.preventDefault();
				e.stopPropagation();
				var trigger_id = $(this).attr('data-bs-target');
				$(trigger_id).toggleClass("show");
				$('body').toggleClass("offcanvas-active");
				$(".screen-overlay").toggleClass("show-overlay");
			});
			$(".btn-close").click(function (e) {
				$(".screen-overlay").removeClass("show-overlay");
				$(".navbar-collapse").removeClass("show");
				$("body").removeClass("offcanvas-active");
			});
		});
	</script>

</body>

</html>