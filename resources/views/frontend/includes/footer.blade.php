<aside id="aside-footer" class="py-lg-6 py-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 mb-lg-0 mb-5">
				<img src="{{asset('/images/webinfo/'.$webinfo->logo_white)}}" alt="" class="img-fluid w-75" alt="Infomerics Nepal">
			</div>
			<!-- COMPANY PROFILE END -->

			<div class="col-lg-3 col-md-4 col-sm-6">
				<ul class="list-arrow text-white">
					<li><a href="/about">About Us</a></li>
					<li><a href="/services">Service Sectors</a></li>
					<li><a href="/press-release">Press Releases</a></li>
					<li><a href="/contact">Contact Us</a></li>
				</ul>
			</div>
			<div class="col-lg-3 col-md-4 col-sm-6 mt-sm-0 mt-4">
				<ul class="list-arrow text-white">
					@foreach(\App\Model\Service::latest()->get() as $key=> $service )
					@if($key <= 3)
					<li><a href="/service-detail/{{$service->id}}">{{$service->name}}</a></li>
					@endif
					@endforeach
				</ul>
			</div>
			<div class="col-lg-3 col-md-4 mt-md-0 mt-4">
				<ul class="list-arrow text-white">
					<li><a href="/contact">Nepal Office</a></li>
					<!-- <li><a href="/contact">India Office</a></li> -->
				</ul>
			</div>
		</div>
	</div>
</aside>
<footer id="footer">
	<div class="container">
		<p class="text-center text-white">&copy; Informerics Ratings. All Rights Reserved. Powered By <a class="anchor-fx" href="https://www.analogueinc.com/" target="_blank">Analogue Inc</a></p>
	</div>
</footer>
