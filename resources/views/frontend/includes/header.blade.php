<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<?php 
use App\Model\Service;
use App\Model\WebInfo;
$ratingServices = Service::all();
$webinfo = WebInfo::first();
?>
<header id="header" class="fixed-top shadow-lg">
	<nav class="navbar navbar-expand-xl navbar-light bg-white p-0">
		<div class="container">
			<a class="navbar-brand logo-holder" href="/"><img src="{{asset('/images/webinfo/'.$webinfo->logo)}}"
				alt="Infomerics Nepal"></a>

				<button class="navbar-toggler header-navbar-toggler my-xl-0 my-2" type="button" data-bs-toggle="collapse"
				data-bs-target="#main-nav-dropdown"><span class="navbar-toggler-icon"></span></button>
				<div class="collapse navbar-collapse" id="main-nav-dropdown">
					<div class="offcanvas-header bg-gray-100">
						<h5 class="mb-0">Navigate</h5>
						<button class="btn btn-white btn-close p-2"><span class="visually-hidden">Close</span></button>
					</div>
					<ul class="navbar-nav ms-xl-auto" id="main-nav">
						<li class="nav-item"><a class="nav-link {{ Request::is('/') ? 'active' : '' }}" href="/">Home</a>
						</li>
						<li class="nav-item"><a class="nav-link {{ Request::segment(1)=='about' ? 'active' : '' }}"
							href="/about">About Us</a></li>

							<div class="nav__container">
								<div class="nav__mobile">
									<div class="nav__btn">
										<a aria-label="Mobile menu" class="nav-toggle fade"><span></span><span
											class="mrg"></span><span class="mrg"></span></a>
										</div>
									</div>
									<nav class="menu-toggle">
										<ul class="nav__menu" style="padding-left:0">

											<li class="nav-item dropdown"><a
												class="nav-link {{ Request::segment(1)=='services' ? 'active' : '' }} {{Request::segment(1) == 'service-detail' ? 'active' : ''}}"
												href="#">What We Do <i class="bi bi-chevron-down"></i></a>
												<ul>
													@if($ratingServices)
													<li class="dropdown second-level"><a href="#" class="sec-lev">Ratings & Gradings <i
														class="bi bi-chevron-down"></i></a>
														<ul>
															<li><a href="/services">All</a></li>
															@foreach($ratingServices as $ser)
															<li><a href="/service-detail/{{$ser->id}}">{{$ser->name}}</a>
															</li>
															@endforeach
														</ul>
													</li>
													@else
													<li><a href="#">N/A</a></li>
													@endif
													@if($webinfo->service_menu)
													<li><a href="/press-release-detail/{{$webinfo->service_menu}}">Guide to Credit Rating</a></li>
													@endif
												</ul>
											</li>

										</ul>
									</nav>
								</div>

								<li class="nav-item"><a
									class="nav-link {{ Request::segment(1)=='ratings' ? 'active' : '' }} {{Request::segment(1) == 'rating-details'? 'active' : ''}}"
									href="/ratings">Get Rated</a></li>
									<li class="nav-item"><a
										class="nav-link {{ Request::segment(1)=='press-release' ? 'active' : '' }} {{Request::segment(1) == 'press-release-detail' ? 'active' : ''}}"
										href="/press-release">Press Releases</a></li>
										<li class="nav-item"><a class="nav-link {{ Request::segment(1)=='career' ? 'active' : '' }}"
											href="/career">Work with Us</a></li>
											<li class="nav-item"><a class="nav-link {{ Request::segment(1)=='contact' ? 'active' : '' }}"
												href="/contact">Contact Us</a></li>
											</ul>
										</div>
									</div>
								</nav>
								<b class="screen-overlay"></b>
							</header>
							<script>
								var show_one = document.getElementById('to-show');
								show_one.addEventListener('click', getShow);

								function getShow() {

									show_one.classList.toggle('active');
									show_one.lastElementChild.classList.toggle('show');

								}

							</script>