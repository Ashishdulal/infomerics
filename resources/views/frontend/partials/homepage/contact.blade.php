@extends('frontend.layouts.app')

@section('content')

<div class="page-title has-bg-image has-overlay" style="background-image: url({{asset('images/page_contact.jpg')}});">
	<div class="overlay-primary"></div>
	<div class="container">
		<h1 class="display-1 align-items-center">Contact Us</h1>
	</div>
</div>

<section class="section py-lg-6 py-5">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-8">
				<div class="mb-md-5 mb-4">
					@if($errors->any())
					<div class="alert alert-danger">
						Your application failed due to :
					@foreach($errors->all() as $error)
						<ul>
							<li><strong>{{$error}}</strong></li>
						</ul>
					@endforeach
					</div>
					@elseif (Session::get('status'))
					<div class="alert alert-success">{{Session::get('status')}}</div>
					@endif
				</div>
				<form method="POST" action="{{route('contact.us')}}" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="row">
						<div class="col-md-6">
							<div class="form-group mb-4">
								<label class="form-label mb-1 fw-bold text-black" for="formno1">Your Name</label>
								<input class="form-control" type="text" name="name" id="formno1" required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group mb-4">
								<label class="form-label mb-1 fw-bold text-black" for="formno2">Your Email Address</label>
								<input class="form-control" name="email" type="email" id="formno2" required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group mb-4">
								<label class="form-label mb-1 fw-bold text-black" for="formno3">Your Phone Number</label>
								<input class="form-control" name="phone" type="tel" id="formno3" required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group mb-4">
								<label class="form-label mb-1 fw-bold text-black" for="formno4">Topic of Inquiry <span class="text-muted small">(optional)</span></label>
								<input class="form-control" name="topic" type="text" id="formno4">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group mb-4">
								<label class="form-label mb-1 fw-bold text-black" for="formno5">Your Business Name</label>
								<input class="form-control" name="business_name" type="text" id="formno5" required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group mb-4">
								<label class="form-label mb-1 fw-bold text-black" for="formno6">Your Business Sector</label>
								<input class="form-control" name="business_sector" type="text" id="formno6">
							</div>
						</div>
					</div>
					<div class="form-group mb-4">
						<label for="formno7" class="form-label mb-1 fw-bold text-black">Your Message</label>
						<textarea id="formno7" name="message" rows="6" class="form-control" required></textarea>
					</div>
					<p class="d-sm-block d-grid">
						<button type="submit" class="btn btn-primary m-sm-1">Send Message<i class="bi bi-arrow-right-short ms-2 text-white"></i></button>
						<button type="reset" class="btn btn-outline-primary m-sm-1 mt-3">Start Over<i class="bi bi-arrow-right-short ms-2"></i></button>
					</p>
				</form>
			</div>
			<div class="col-lg-4 mt-lg-0 mt-5">
				<div class="row">
					<address class="mb-0 col-lg-12 col-md-6">
						<h5 class="mb-3">Nepal Office</h5>
						{!! $webinfo? $webinfo->nepal_contact : '' !!}
					</address>
					@if(isset($webinfo->india_contact))
					<address class="mb-0 col-lg-12 col-md-6 mt-lg-5 mt-md-0 mt-4">
						<h5 class="mb-3">India Office</h5>
						{!! $webinfo? $webinfo->india_contact : '' !!}
					</address>
					@endif
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section google-map">
	{!! $webinfo? $webinfo->map : '' !!}
</section>

@endsection