@extends('frontend.layouts.app')

@section('content')
<div class="page-title has-bg-image has-overlay" style="background-image: url({{asset('images/page_press.jpg')}});">
	<div class="overlay-primary"></div>
	<div class="container">
		<h1 class="display-1 align-items-center">Press Releases</h1>
	</div>
</div>

<section class="section py-lg-6 py-5">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xxl-8 col-xl-7 col-lg-10">
				@if(isset($pressRelease))
				@foreach($pressRelease as $key => $press)
				<div class="icon-card shadow-lg rounded-3 mb-4">
					<div class="icon-card-content">
						<h5 class="mb-3"><a class="text-black text-capitalize anchor-fx" href="/press-release-detail/{{$press->id}}">{{$press->title }}</a></h5>
						<p class="small">Posted on <strong class="text-black">{{$press->created_at->format('dS F Y')}}</strong></p>
					</div>
					<a target="_blank" href="/images/event/{{$press->image}}" class="icon-holder bg-primary" data-bs-toggle="tooltip" data-bs-placement="top" title="Download PDF"><img src="images/ico_download-file.svg" alt=""></a>
				</div>
				@endforeach
				@else
				<div class="text-center">
					<h4>Nothing Found...</h4>
				</div>
				@endif
				<!-- Pagination  -->
				<div class="d-flex justify-content-center">
					{!! $pressRelease->links() !!}
				</div>
			</div>
			<div class="col-xxl-4 col-xl-5 col-lg-10 mt-xl-0 mt-5">
				<section class="section py-xl-7 py-5 rounded-3 overflow-hidden has-bg-image has-overlay" style="background-image: url({{asset('images/press_cta.jpg')}});">
					<div class="overlay-primary"></div>
					<div class="container">
						<div class="px-lg-3 px-2">
							<h6 class="section-title h1">Interested in rating your company?</h6>
										{!! $webinfo? $webinfo->rating : '' !!}
							<div class="row">
								<div class="col-xl-12 col-lg-4 col-md-5 col-sm-7">
									<p><a href="/contact" class="btn btn-white w-100">Get Started<i class="bi bi-arrow-right-short ms-2 text-black"></i></a></p>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</section>
@endsection