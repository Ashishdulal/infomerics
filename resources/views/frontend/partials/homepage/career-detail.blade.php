@extends('frontend.layouts.app')

@section('content')
<div class="page-title has-bg-image has-overlay" style="background-image: url(images/career_details.jpg);">
			<div class="overlay-primary"></div>
			<div class="container">
				<h1 class="display-1 text-capitalize">{{$career->job_title}}</h1>
			</div>
		</div>

		<section class="section py-lg-6 py-5">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-10">
						<!-- <p>Semper sem vitae cursus magna. Vitae, diam felis risus praesent. Scelerisque scelerisque dictum dignissim nulla ornare. Vitae et vel odio tincidunt viverra rhoncus. Erat nunc, interdum aenean cras orci, auctor sagittis nunc. Volutpat, magnis et a et gravida ornare. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu.</p> -->
						<p class="mt-4"><a href="/career-apply?career_id={{$career->id}}" class="btn btn-primary">Apply Now<i class="bi bi-arrow-right-short ms-2 text-white"></i></a></p>
					</div>
				</div>
			</div>
		</section>

		<section class="section pb-lg-6 pb-5">
			<div class="container">
				<div class="row gx-4">
					<div class="col-lg-6">
						<div class="p-lg-5 p-4 rounded-3 bg-gray-100 h-100">
							<h3 class="section-title">Who we're looking for</h3>
							{!!$career->job_description!!}
						</div>
					</div>
					<div class="col-lg-6 mt-lg-0 mt-3">
						<div class="p-lg-5 p-4 rounded-3 bg-gray-100 h-100">
							<h3 class="section-title">Who you are</h3>
							{!!$career->job_responsibilities!!}
						</div>
					</div>
				</div>
			</div>
		</section>

		<div class="container text-center mb-lg-6 mb-5">
			<section class="section py-lg-6 py-5 px-4 rounded-3 overflow-hidden has-bg-image has-overlay" style="background-image: url(images/career_cta.jpg);">
				<div class="overlay-primary"></div>
					<h2 class="section-title">Apply for {{$career->job_title}}</h2>
					<div class="row justify-content-center">
						<div class="col-xl-6 col-lg-8 col-sm-10">
							<p>Are you ready for your next step towards your career? This is your once in a lifetime chance to be a part of the Infomerics team.</p>
							<p class="mt-lg-5 mt-4"><a href="/career-apply?career_id={{$career->id}}" class="btn btn-white">Apply Now<i class="bi bi-arrow-right-short ms-2 text-black"></i></a></p>
						</div>
					</div>
			</section>
		</div>
@endsection