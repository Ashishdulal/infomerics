@extends('frontend.layouts.app')

@section('content')

<div class="hero-slider swiper-container">
	<div class="swiper-wrapper">
	@if(isset($sliders))
		@foreach ($sliders as $slider)
		<div class="swiper-slide has-bg-image" style="background-image: url(/images/slider/{{$slider->slider_image}});">
			<img src="/images/slider/{{$slider->slider_image}}" alt="" class="img-fluid hero-slider-mobile-image">
			<div class="caption">
				<div class="container-lg">
					<div class="row g-0">
						<div class="col-md-6">
							<div class="pe-md-5 pb-md-5 pt-md-0 ps-lg-0 mb-md-5 pt-4 pb-4 px-3 mb-4">
								<h3 class="h3 mb-1 pt-md-0 pt-2 text-white">{{$slider->title}}</h3>
								<!-- <h3 class="mb-md-4 mb-3 text-white">Subtitle goes here to read</h3> -->
								<p class="mb-4">{!! $slider->description !!}</p>
								<p><a href="{{$slider->url_link}}" class="btn btn-primary">{{$slider->button_name}}<i class="bi bi-arrow-right-short ms-2"></i></a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@endforeach
		@else
		<div class="swiper-slide has-bg-image" style="background-image: url(images/hero_slide2.jpg);">
			<img src="images/hero_slide2.jpg" alt="" class="img-fluid hero-slider-mobile-image">
			<div class="caption">
				<div class="container-lg">
					<div class="row g-0">
						<div class="col-md-6">
							<div class="pe-md-5 pb-md-5 pt-md-0 ps-lg-0 mb-md-5 pt-4 pb-4 px-3 mb-4">
								<h2 class="h1 mb-1 pt-md-0 pt-2 text-white">Title Goes Here</h2>
								<h3 class="mb-md-4 mb-3 text-white">Subtitle goes here to read</h3>
								<p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								<p><a href="/about" class="btn btn-primary">Our Process<i class="bi bi-arrow-right-short ms-2"></i></a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endif
	</div>
	<div class="navigation-holder">
		<div class="container position-relative">
			<div class="swiper-pagination"></div>
			<div class="swiper-button-prev"></div>
			<div class="swiper-button-next"></div>
		</div>
	</div>
</div>
<!-- SLIDER END -->

<section class="section py-lg-6 py-5">
	<div class="container">
		<div class="row justify-content-center mb-lg-4 pb-lg-3">
			<div class="col-lg-8">
				<div class="text-center">
					<h2 class="mb-sm-4 mb-3">Our Service Sectors</h2>
					{!! $webinfo ? $webinfo['text1'] : '' !!}
				</div>
			</div>
		</div>
		<!-- SERVICE SECTORS CONTENT END -->

		@if(isset($services))
		<div class="cards-carousel three-col-carousel swiper-container pt-4 pb-5 px-3">
			<div class="swiper-wrapper">
				@foreach($services as $service)
				<div class="swiper-slide">
					<div class="card shadow-lg rounded-3 border-0 icon-holder-trigger">
						<div class="d-flex p-sm-4 p-3 mb-sm-2 mb-0">
							<div class="icon-holder bg-primary">
								<img class="dynamic-icon" src="/images/service/{{$service->icon}}" alt="">
							</div>
						</div>
						<div class="card-body pt-0 px-sm-4 px-3 pb-sm-4 pb-3">
							<h5 class="card-title" style="min-height: 40px;">{{$service->name}}</h5>
							<p class="card-text">{!! substr_replace($service->description,"...",150) !!}</p>
							<p><a href="{{$service->button_url}}" class="btn btn-white"><span class="text-primary">{{$service->button_name}}</span><i class="bi bi-arrow-right-short ms-2 text-primary"></i></a></p>
						</div>
					</div>
				</div>
				@endforeach
			</div>
			<div class="swiper-pagination"></div>
		</div>
		@endif
		<!-- SERVICE SECTORS CAROUSEL END -->
	</div>
</section>
<!-- SERVICES SECTORS END -->

<section class="section py-lg-6 py-5 px-4 overflow-hidden has-bg-image has-overlay" style="background-image: url(images/career_cta.jpg);">
	<div class="overlay-primarydark"></div>
	<div class="container">
		<div class="py-xl-5 py-lg-4 py-3 px-3">
			<h2 class="mb-sm-4 mb-3 text-center">Find a Rating</h2>
			<div class="row justify-content-center">
				<div class="col-xl-8 col-lg-8 col-md-10">
					<form action="{{route('frontend.ratingsSearch')}}" class="" method="get">
								{{csrf_field()}}
						<div class="input-group input-group-lg">
							<input type="text" class="form-control" name="search" placeholder="Search for a company...">
							<button class="btn btn-dark" type="submit">Find Ratings<i class="bi bi-arrow-right-short ms-2"></i></button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- SEARCH AREA END -->

<div class="overflow-hidden">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<section class="section py-lg-6 py-5 pe-lg-6 faux-bg-light-left h-100">
					<h2 class="mb-sm-4 mb-3 text-capitalize">About {{isset($title)? $title[0] : 'Us'}}</h2>
					<img src="/images/webinfo/{{ $webinfo ? $webinfo['about_us_image'] : ''}}" alt="" class="img-fluid mb-sm-4 mb-4 rounded-3">
					<p class="mb-sm-4 mb-3">{!! $webinfo ? $webinfo['about_us'] : '' !!}</p>
					<p><a href="/about" class="btn btn-primary">Learn More<i class="bi bi-arrow-right-short ms-2"></i></a></p>
				</section>
			</div>
			<!-- ABOUT SECTION END -->

			<div class="col-lg-6">
				<section class="section py-lg-6 py-5 ps-lg-4">
					<h2 class="mb-sm-4 mb-3">Press Releases</h2>
					@if(isset($pressRelease))
					@foreach($pressRelease as $key => $press)
					@if($key <= 3)
					<div class="icon-card shadow-lg rounded-3 mb-4">
						<div class="icon-card-content">
							<h5 class="mb-3"><a class="text-black text-capitalize anchor-fx" href="/press-release-detail/{{$press->id}}">{{$press->title }}</a></h5>
							<p class="small">Posted on <strong class="text-black">{{$press->created_at->format('dS F Y')}}</strong></p>
						</div>
						<a target="_blank" href="/images/event/{{$press->image}}" class="icon-holder bg-primary" data-bs-toggle="tooltip" data-bs-placement="top" title="Download PDF"><img src="images/ico_download-file.svg" alt=""></a>
					</div>
					@endif
					@endforeach
					@endif
					<p><a href="/press-release" class="btn btn-primary">View All<i class="bi bi-arrow-right-short ms-2"></i></a></p>
				</section>
			</div>
			<!-- PRESS RELEASES END -->
		</div>
	</div>
</div>
<!-- INFO SECTION END -->
@endsection