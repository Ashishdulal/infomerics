@extends('frontend.layouts.app')

@section('content')
		<div class="page-title has-bg-image has-overlay" style="background-image: url({{asset('images/service_details.jpg')}});">
			<div class="overlay-primary"></div>
			<div class="container">
				<h1 class="display-1">{{substr_replace($service->name,"...",20)}}</h1>
			</div>
		</div>

		<section class="section py-lg-6 py-5">
			<div class="container">
				<h1 class="h3 mb-lg-5 mb-4">{{$service->name}}</h1>
				<div style="text-align: justify;">{!! $service->description !!}
				</div>

				<div class="fit-image vh-50 mt-lg-5 mt-4"><img src="{{asset('images/service_details.jpg')}}" alt=""></div>
			</div>
		</section>
@endsection