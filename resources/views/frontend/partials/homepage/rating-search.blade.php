@extends('frontend.layouts.app')

@section('content')
<div class="page-title has-bg-image has-overlay" style="background-image: url({{asset('images/service_details.jpg')}});">
	<div class="overlay-primary"></div>
	<div class="container">
		<h1 class="display-1">Search Ratings</h1>
	</div>
</div>

<section class="section py-lg-6 py-5">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xxl-7 col-xl-7 mt-xl-0 mt-5 order-xl-1 order-2">
				<h3>You searched for "<span class="text-primary">{{ Request::get('search') }}</span>"</h3>
				@if(count($rateSearch))
				@foreach($rateSearch as $rate)
				<div class="icon-card shadow-lg rounded-3 mb-4">
					<div class="icon-card-content">
						<h5 class="mb-3"><a class="text-black anchor-fx" href="/rating-details/{{$rate->id}}">{{$rate->company_name}}</a></h5>
						<p class="small"><span>{{$rate->industry}}</span><span class="mx-3">|</span><span>Rated on {{$rate->created_at->format('dS F Y')}}</span></p>
					</div>
					<a target="_blank" href="/images/rating/{{$rate->pdf}}" class="icon-holder bg-primary" data-bs-toggle="tooltip" data-bs-placement="top" title="Download Rating Rationale"><img src="{{asset('images/ico_download-file.svg')}}" alt=""></a>
				</div>
				@endforeach
				@else
				<div class="icon-card shadow-lg rounded-3 mb-4">
					<div style="margin: 0 auto;" class="icon-card-content">
						<h4>Nothing found ...</h4>
					</div>
				</div>
				@endif
			</div>
			<div class="col-xxl-5 col-xl-5 order-xl-2 order-1">
				<section class="section py-xl-7 py-5 rounded-3 overflow-hidden has-bg-image has-overlay" style="background-image: url({{asset('images/press_cta.jpg')}});">
					<div class="overlay-primary"></div>
					<div class="container">
						<div class="px-lg-3 px-2">
							<h6 class="section-title h1">Interested in rating your company?</h6>
							{!! $webinfo? $webinfo->rating : '' !!}
							<div class="row">
								<div class="col-xl-12 col-lg-4 col-md-5 col-sm-7">
									<p><a href="/contact" class="btn btn-white w-100">Get Started<i class="bi bi-arrow-right-short ms-2 text-black"></i></a></p>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</section>
@endsection