@extends('frontend.layouts.app')

@section('content')
<div class="page-title has-bg-image has-overlay" style="background-image: url({{asset('images/service_details.jpg')}});">
	<div class="overlay-primary"></div>
	<div class="container">
		<h1 class="display-1 align-items-center">Ratings</h1>
	</div>
</div>

<section class="section py-lg-6 py-5">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xxl-8 col-xl-10 order-xl-1 order-2">
				<div class="py-xl-5 py-lg-4 py-3 px-3">
					<h3 class="text-center mb-lg-4 mb-4 pb-lg-2 pb-0">Look for a company's rating</h3>
					<form action="{{route('frontend.ratingsSearch')}}" class="" method="get">
						{{csrf_field()}}
						<div class="input-group input-group-lg">
							<input type="text" class="form-control" name="search" placeholder="Search for a company...">
							<button class="btn btn-primary" type="submit">Find Ratings<i class="bi bi-arrow-right-short ms-2"></i></button>
						</div>
					</form>
				</div>
			</div>

			<div class="col-xxl-8 col-xl-10 mt-xl-5 mt-5 order-xl-2 order-1">
				<section class="section py-xl-6 py-5 rounded-3 overflow-hidden has-bg-image has-overlay" style="background-image: url({{asset('images/press_cta.jpg')}});">
					<div class="overlay-primary"></div>
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-xl-11 col-lg-10">
								<h6 class="section-title h1">Interested in rating your company?</h6>
								{!! $webinfo? $webinfo->rating : '' !!}
								<div class="row">
									<div class="col-xl-4 col-lg-4 col-md-5 col-sm-7">
										<p><a href="/contact" class="btn btn-white w-100">Get Started<i class="bi bi-arrow-right-short ms-2 text-black"></i></a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</section>
@endsection