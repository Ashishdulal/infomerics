@extends('frontend.layouts.app')

@section('content')
<section class="section py-lg-6 py-5">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-xxl-8 col-xl-9 col-lg-10">
						<h1 class="h3 mb-lg-5 mb-4 text-capitalize">{{$pressRelease->title}}</h1>
						<div style="text-align: justify;">
							{!! $pressRelease->description !!}
						</div>
						<div class="d-sm-flex d-block align-items-center justify-content-between py-lg-4 py-3 my-lg-6 my-5 border-top border-bottom">
							<p class="fst-italic mb-sm-0 mb-3">Posted on <strong class="text-black">{{$pressRelease->created_at->format('dS F Y')}}</strong></p>
							<a target="_blank" href="/images/event/{{$pressRelease->image}}" class="btn btn-primary btn-sm">Download PDF<i class="bi bi-file-earmark-arrow-down ms-2"></i></a>
						</div>

						<section class="section py-xl-6 py-5 rounded-3 overflow-hidden has-bg-image has-overlay" style="background-image: url({{asset('images/press_cta.jpg')}});">
							<div class="overlay-primary"></div>
							<div class="container">
								<div class="row justify-content-center">
									<div class="col-xl-11 col-lg-10">
										<h6 class="section-title h1">Interested in rating your company?</h6>
										{!! $webinfo? $webinfo->rating : '' !!}
										<div class="row">
											<div class="col-xl-4 col-lg-4 col-md-5 col-sm-7">
												<p><a href="/contact" class="btn btn-white w-100">Get Started<i class="bi bi-arrow-right-short ms-2 text-black"></i></a></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</section>
@endsection