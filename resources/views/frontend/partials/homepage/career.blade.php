@extends('frontend.layouts.app')

@section('content')
<div class="page-title has-bg-image has-overlay" style="background-image: url({{asset('images/career_overview.jpg')}});">
	<div class="overlay-primary"></div>
	<div class="container">
		<h1 class="display-1">Work with Us</h1>
	</div>
</div>

<section class="section py-lg-6 py-5">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-6">
				<div class="pe-lg-4 pe-0">
					<h2 class="section-title">Life at Infomerics</h2>
						{!! $webinfo? $webinfo->life_at_infomerics : '' !!}
				</div>
			</div>
			<div class="col-lg-6 mt-lg-0 mt-4 text-center">
				<img src="{{asset('/images/webinfo/'.$webinfo->life_at_infomerics_image)}}" alt="" class="img-fluid rounded">
			</div>
		</div>
	</div>
</section>

<section class="section py-xl-7 py-lg-6 py-5 bg-gray-100">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-6 order-lg-2 order-1">
				<div class="ps-lg-4 ps-0">
					<h2 class="section-title">Our People</h2>
						{!! $webinfo? $webinfo->our_people : '' !!}
				</div>
			</div>
			<div class="col-lg-6 mt-lg-0 mt-4 order-lg-1 order-2 text-center">
				<img src="{{asset('/images/webinfo/'.$webinfo->our_people_image)}}" alt="" class="img-fluid rounded">
			</div>
		</div>
	</div>
</section>

<section class="section py-xl-7 py-lg-6 py-5">
	<h2 class="section-title text-center">Current Openings</h2>
	<div class="container">
		<div class="row">
			@foreach($careers as $career)
			<div class="col-lg-4 col-md-6 my-3">
				<div class="card shadow-lg rounded-3 border-0">
					<div class="card-body py-lg-4 py-3 px-lg-4 px-3">
						<h4 class="card-title mb-2">{{$career->job_title}}</h4>
						<p class="text-primary fw-bold">{{$career->candidate}} positions</p>
						<div class="d-xl-flex d-lg-block d-sm-flex d-block justify-content-between align-items-end">
							<?php
							$date = new DateTime($career->end_date);
							?>
							<p class="text-gray small mb-1 ">Apply before <b>{{$date->format('dS F Y')}}</b></p>
							<p class="mb-0"><a href="/career-details/{{$career->id}}" class="btn btn-white btn-sm"><span class="text-primary">Details</span><i class="bi bi-arrow-right-short ms-1 text-primary"></i></a></p>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</section>

<section class="section py-lg-6 py-5 has-bg-image has-overlay" style="background-image: url({{asset('images/career_cta.jpg')}});">
	<div class="overlay-primary"></div>
	<div class="container text-center">
		<h2 class="section-title">Apply Now</h2>
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-8 col-sm-10">
						{!! $webinfo? $webinfo->rating : '' !!}
				<p class="mt-lg-5 mt-4"><a href="/career-apply" class="btn btn-white">Apply Now<i class="bi bi-arrow-right-short ms-2 text-black"></i></a></p>
			</div>
		</div>
	</div>
</section>
@endsection