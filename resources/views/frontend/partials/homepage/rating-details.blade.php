@extends('frontend.layouts.app')

@section('content')
<section class="section py-lg-6 py-5">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-xxl-8 col-xl-9 col-lg-10">
						<div class="mb-lg-5 mb-4">
							<h1 class="h3 mb-3">{{$rating->company_name}}</h1>
							<p>
								<span class="d-sm-inline-block d-block">Industry: <strong>{{$rating->industry}}</strong></span>
								<span class="mx-3 d-sm-inline-block d-none">|</span>
								<span class="d-sm-inline-block d-block">Rated On : <strong>{{$rating->created_at->format('dS F Y')}}</strong></span>
							</p>
						</div>

						<div class="bg-primary-light border border-primary-light p-md-4 p-3 rounded-3">
							<div class="row">
								<div class="col-md-4 mt-2 mb-3">
									<p class="fw-bold small mb-1 text-primarydark">Instrument Category</p>
									<p class="fw-bold h4 text-primary-dark">{{$rating->instrument_category}}</p>
								</div>
								<div class="col-md-4 mt-2 mb-3">
									<p class="fw-bold small mb-1 text-primarydark">Ratings</p>
									<p class="fw-bold h4 text-primary-dark">{{$rating->rating}}</p>
								</div>
								<div class="col-md-4 mt-2 mb-3">
									<p class="fw-bold small mb-1 text-primarydark">Outlook</p>
									{!! $rating->outlook !!}
								</div>
							</div>
							<div class="row align-items-end">
								<div class="col-md-6 mt-2 mb-3">
									<p class="fw-bold small mb-1 text-primarydark">Details</p>
									{!! $rating->details !!}
								</div>
								<div class="col-md-6 mt-2 mb-3">
									<a target="_blank" href="/images/rating/{{$rating->pdf}}" class="btn btn-primarydark">Download Rating PDF<i class="bi bi-file-earmark-arrow-down ms-2"></i></a>
								</div>
							</div>
						</div>


						<section class="section py-xl-6 py-5 mt-xl-6 mt-5 rounded-3 overflow-hidden has-bg-image has-overlay" style="background-image: url(images/press_cta.jpg);">
							<div class="overlay-primary"></div>
							<div class="container">
								<div class="row justify-content-center">
									<div class="col-xl-11 col-lg-10">
										<h6 class="section-title h1">Interested in rating your company?</h6>
										{!! $webinfo? $webinfo->rating : '' !!}
										<div class="row">
											<div class="col-xl-4 col-lg-4 col-md-5 col-sm-7">
												<p><a href="/contact" class="btn btn-white w-100">Get Started<i class="bi bi-arrow-right-short ms-2 text-black"></i></a></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</section>
@endsection