@extends('frontend.layouts.app')

@section('content')
<div class="page-title has-bg-image has-overlay" style="background-image: url(images/career_cta.jpg);">
	<div class="overlay-primary"></div>
	<div class="container">
		<h1 class="display-1 align-items-center">Apply Now</h1>
	</div>
</div>

<section class="section py-lg-6 py-5">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xxl-9 col-xl-10 col-lg-11">
				<div class="mb-md-5 mb-4">
					@if($errors->any())
					<div class="alert alert-danger">
						Your application failed due to :
					@foreach($errors->all() as $error)
						<ul>
							<li><strong>{{$error}}</strong></li>
						</ul>
					@endforeach
					</div>
					@elseif (Session::get('status'))
					<div class="alert alert-success">{{Session::get('status')}}</div>
					@endif
				</div>

				<form method="POST" action="{{route('frontend.career.apply.new')}}" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="row">
						<div class="col-lg-4 col-md-6">
							<div class="form-group mb-4">
								<label class="form-label mb-1 fw-bold text-black" for="formno1">Your Name</label>
								<input class="form-control" name="full_name" value="{{old('full_name')}}" type="text" id="formno1" required>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="form-group mb-4">
								<label class="form-label mb-1 fw-bold text-black" for="formno2">Your Email Address</label>
								<input class="form-control" name="email" value="{{old('email')}}" type="email" id="formno2" required>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="form-group mb-4">
								<label class="form-label mb-1 fw-bold text-black" for="formno3">Your Phone Number</label>
								<input class="form-control" name="phone" value="{{old('phone')}}" type="tel" id="formno3" required>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-6">
							<div class="form-group mb-4">
								<label class="form-label mb-1 fw-bold text-black" for="formno5">Expected Salary (in NRs)</label>
								<input class="form-control" name="expected_salary" value="{{old('expected_salary')}}" type="number" id="formno5" required>
							</div>
						</div>
						<div class="col-lg-8 col-md-6">
							<div class="form-group mb-4">
								<label class="form-label mb-1 fw-bold text-black" for="formno4">Position you're applying for(Career)</label>
								<select class="form-select" name="career">
									<option selected disabled>Choose a job position</option>
									@foreach($careers as $caree)
									<option @if(Request::get('career_id')==$caree->id) selected @endif value="{{$caree->id}}">{{$caree->job_title}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="w-100"></div>
						<div class="col-lg-6 col-md-6">
							<div class="form-group mb-4">
								<label class="form-label mb-1 fw-bold text-black" for="formno6">Your CV</label>
								<input class="form-control" type="file" name="cv_file" id="formno6" >
							</div>
						</div>
						<div class="col-lg-6 col-md-6">
							<div class="form-group mb-4">
								<label class="form-label mb-1 fw-bold text-black" for="formno7">Your Cover Letter</label>
								<input class="form-control" type="file" name="cover_letter" id="formno7" >
							</div>
						</div>
					</div>
					<p class="d-sm-block d-grid">
						<button type="submit" class="btn btn-primary m-sm-1">Apply Now<i class="bi bi-arrow-right-short ms-2 text-white"></i></button>
						<button type="reset" class="btn btn-outline-primary m-sm-1 mt-3">Start Over<i class="bi bi-arrow-right-short ms-2"></i></button>
					</p>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection