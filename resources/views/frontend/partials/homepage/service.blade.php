@extends('frontend.layouts.app')

@section('content')
<div class="page-title has-bg-image has-overlay" style="background-image: url(images/page_about.jpg);">
	<div class="overlay-primary"></div>
	<div class="container">
		<h1 class="display-1">Service Sectors</h1>
	</div>
</div>
<section class="section py-lg-6 py-5">
	<div class="container">
		<div class="row align-items-center">
			@if(isset($services))
			@foreach($services as $service)
			<div class="col-md-6 my-3">
				<div class="card shadow-lg rounded-3 border-0 icon-holder-trigger">
					<div class="d-flex p-sm-4 p-3 mb-sm-2 mb-0">
						<div class="icon-holder bg-primary">
							<img class="dynamic-icon" src="/images/service/{{$service->icon}}" alt="">
						</div>
					</div>
					<div class="card-body pt-0 px-sm-4 px-3 pb-sm-4 pb-3">
						<h5 class="card-title" style="min-height: 40px;">{{$service->name}}</h5>
						<p class="card-text">{!! substr_replace($service->description,"...",150) !!}</p>
						<p><a href="/service-detail/{{$service->id}}" class="btn btn-white"><span class="text-primary">{{$service->button_name}}</span><i class="bi bi-arrow-right-short ms-2 text-primary"></i></a></p>
					</div>
				</div>
			</div>
			@endforeach
			@else
			<div class="text-center">
				<h4>Nothing found ....</h4>
			</div>
			@endif
		</div>
		<!-- Pagination  -->
		<div class="d-flex justify-content-center">
			{!! $services->links() !!}
		</div>
	</div>
</section>
@endsection