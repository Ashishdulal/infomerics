@extends('frontend.layouts.app')

@section('content')
<div class="page-title has-bg-image has-overlay" style="background-image: url({{asset('images/page_about.jpg')}});">
	<div class="overlay-primary"></div>
	<div class="container">
		<h1 class="display-1">About Us</h1>
	</div>
</div>

<section class="section py-lg-6 py-5">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-6">
				{!! $webinfo? $webinfo->about_us : '' !!}
			</div>
			<div class="col-lg-6 mt-lg-0 mt-4">
				<img src="/images/webinfo/{{$webinfo->about_us_image}}" alt="" class="img-fluid rounded">
			</div>
		</div>
	</div>
</section>

<section class="section position-relative bg-gray-100 pb-lg-0 pb-5">
	<div class="position-absolute w-50 h-100 has-bg-image d-lg-block d-none"
		style="background-image: url(images/webinfo/{{$webinfo->our_mission_image}}); left: 0; top: 0;"></div>
	<div class="container z-1">
		<div class="row justify-content-lg-end">
			<div class="col-lg-6 d-lg-none d-block order-lg-1 order-2">
				<img src="images/webinfo/{{$webinfo->our_mission_image}}" alt="" class="img-fluid rounded">
			</div>
			<div class="col-lg-6 order-lg-2 order-1">
				<section class="section pt-lg-6 pt-5 pb-lg-6 pb-4 ps-lg-4">
					<h2 class="section-title">Our Mission</h2>
					{!! $webinfo? $webinfo->our_mission : '' !!}
				</section>
			</div>
		</div>
	</div>
</section>

<section class="section position-relative bg-gray-200 pb-lg-0 pb-5">
	<div class="position-absolute w-50 h-100 has-bg-image d-lg-block d-none"
		style="background-image: url(images//webinfo/{{$webinfo->our_vision_image}}); right: 0; top: 0;"></div>
	<div class="container z-1">
		<div class="row">
			<div class="col-lg-6">
				<section class="section pt-lg-6 pt-5 pb-lg-6 pb-4 pe-lg-4 text-gray-800">
					<h2 class="section-title">Our Vision</h2>
					{!! $webinfo? $webinfo->our_vision : '' !!}
				</section>
			</div>
			<div class="col-lg-6 d-lg-none d-block">
				<img src="images/page_about.jpg" alt="" class="img-fluid rounded">
			</div>
		</div>
	</div>
</section>

<section class="section py-lg-6 py-5">
	<div class="container">
		<div class="row mb-sm-4 mb-3">
			<div class="col-xl-6 col-lg-8">
				<h2 class="section-title">Board Of Directors</h2>
				{!! $webinfo? $webinfo->text2 : '' !!}
			</div>
		</div>

		<div class="row">
			<div class="cards-carousel four-col-carousel swiper-container pt-4 pb-5 px-3">
				<div class="swiper-wrapper">
					@foreach($managements as $team)
					<div class="swiper-slide">
						<div class="card shadow-lg rounded-3 border-0">
							<img src="/images/management/{{$team->image}}" alt="" class="img-fluid card-img-top">
							<div class="card-body">
								<h5 class="card-title mb-1">{{$team->name}}</h5>
								<p class="card-text">{{$team->designation}}</p>
							</div>
						</div>
					</div>
					@endforeach
				</div>
				<div class="swiper-pagination"></div>
				<div class="swiper-button-prev"></div>
				<div class="swiper-button-next"></div>
			</div>
		</div>
	</div>
</section>
@endsection