@extends('backend.layouts.app')

@section('content')
<style>
	.switch {
		position: relative;
		display: inline-block;
		width: 60px;
		height: 34px;
	}

	.switch input { 
		opacity: 0;
		width: 0;
		height: 0;
	}

	.slider {
		position: absolute;
		cursor: pointer;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		background-color: #ccc;
		-webkit-transition: .4s;
		transition: .4s;
	}

	.slider:before {
		position: absolute;
		content: "";
		height: 26px;
		width: 26px;
		left: 4px;
		bottom: 4px;
		background-color: white;
		-webkit-transition: .4s;
		transition: .4s;
	}

	input:checked + .slider {
		background-color: #2196F3;
	}

	input:focus + .slider {
		box-shadow: 0 0 1px #2196F3;
	}

	input:checked + .slider:before {
		-webkit-transform: translateX(26px);
		-ms-transform: translateX(26px);
		transform: translateX(26px);
	}

	/* Rounded sliders */
	.slider.round {
		border-radius: 34px;
	}

	.slider.round:before {
		border-radius: 50%;
	}
</style>

<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="banner">
			<h2>
				<a href="/admin/home">Home</a>
				<i class="fa fa-angle-right"></i>
				<span>Settings</span>
			</h2>
		</div>
		<!--//banner-->
		<!--faq-->
		<div class="blank">


			<div class="blank-page">
				@if($errors->any())
				@foreach($errors->all() as $error)
				<ul>
					<li>{{$error}}</li>
				</ul>
				@endforeach
				@endif
				@if($webInfo)
				<form action="{{route('webinfo.update',['webinfo'=>($webInfo ? $webInfo['id'] : 'create')])}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
					{{method_field('patch')}}
					{{csrf_field()}}
					<div style="text-align: right;margin: 20px 40px 0 0px;" class="home-btn">
						<button type="submit" class="btn btn-primary btn-sm">
							<i class="fa fa-dot-circle-o"></i> Update
						</button>
					</div>
					<div class="card-body card-block">
						<div class="container-fluid">
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" class="tab-gap" href="#home">General</a></li>
								<!-- <li><a data-toggle="tab" href="#menu1">Footer</a></li> -->
								<li><a data-toggle="tab" class="tab-gap" href="#menu2">SEO</a></li>
								<li></li>
							</ul>
							<div class="tab-content">
								<div id="home" class="tab-pane active">
									<div class="row form-group">
										<div class="col col-md-3">
											<label for="comming_soon" class=" form-control-label">Comming Soon</label>
										</div>
										<div class="col-12 col-md-9">
											<label class="switch">
											<input type="checkbox" name="comming_soon" {{ ($webInfo->comming_soon== 1) ? 'checked' : '' }} value="1" data-toggle="toggle" data-width="100">
												<span class="slider round"></span>
											</label>
										</div>
									</div>
									<div class="row form-group">
										<div class="col col-md-3">
											<label for="web_title" class=" form-control-label">Site Title</label>
										</div>
										<div class="col-12 col-md-9">
											<input type="text" name="web_title"  rows="9"  class="form-control" value="{{$webInfo->web_title}}" placeholder="Website title"><br>
										</div>
									</div>

						<!--	<div class="row form-group">
								<div class="col col-md-3">
									<label for="tagline" class=" form-control-label">Tagline</label>
								</div>
								<div class="col-12 col-md-9">
									<input type="text" name="tagline"  rows="9"  class="form-control" value="{{$webInfo->tagline}}" placeholder="tagline"><br>
								</div>
							</div> -->
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="mail" class=" form-control-label">Email Address</label><br>
									<span class="au-breadcrumb-span">[ Note: This will be the mail address to recieve all the mails sent from the website. ]</span>
								</div>
								<div class="col-12 col-md-9">
									<input type="email" name="mail"  rows="9"  class="form-control" value="{{$webInfo->mail}}" placeholder="Email address"><br>
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="logo" class=" form-control-label">Site Logo</label><br>
									<span class="au-breadcrumb-span">[ Note: Please upload the small image size of '.png' type & should be less than 100kb ]</span>
								</div>
								<div class="col-12 col-md-9 process">
									<input onchange="readURL(this);" type="file" id="logo" accept="image/png, image/jpg, image/jpeg" name="logo" class="form-control-file"><br>
									<img style="max-width: 40%;" id="blah" src="/images/webinfo/{{$webInfo->logo}}" alt="site Logo">
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="logo_white" class=" form-control-label">Footer Logo</label><br>
									<span class="au-breadcrumb-span">[ Note: Please upload the small image size of '.png' type & should be less than 100kb ]</span>
								</div>
								<div class="col-12 col-md-9">
									<input onchange="readURLFav(this);" type="file" id="logo_white" accept="image/png, image/jpg, image/jpeg" name="logo_white" class="form-control-file"><br>
									<img style="max-width: 40%; padding: 10px; background: #000000;" id="favBlah" src="/images/webinfo/{{$webInfo->logo_white}}" alt="site Favicon">
								</div>
							</div>
						</div>
						<div id="menu1" class="tab-pane fade">
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="footer_visibility" class=" form-control-label">Visibility</label>
								</div>
								<div class="col-12 col-md-9">
									<input type="checkbox" name="footer_visibility" {{ ($webInfo->footer_visibility==1) ? 'checked' : '' }} value="1" data-toggle="toggle" data-width="100">
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="footer_text" class=" form-control-label">Footer HTML</label>
								</div>
								<div class="col-12 col-md-9">
									<textarea name="footer_text"  rows="9"  class="form-control ckeditor">{{$webInfo->footer_text}}</textarea>
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="copyright_text" class=" form-control-label">Copyright</label>
								</div>
								<div class="col-12 col-md-9">
									<textarea name="copyright_text"  rows="9"  class="form-control ckeditor">{{$webInfo->copyright_text}}</textarea>
								</div>
							</div>
						</div>
						<div id="menu2" class="tab-pane fade-in">
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="meta_keywords" class=" form-control-label">Meta Keywords</label>
								</div>
								<div class="col-12 col-md-9">
									<input type="text" name="meta_keywords"  rows="9"  class="form-control" value="{{$webInfo->meta_keywords}}" placeholder="meta keywords for seo"><br>
								</div>
							</div>
							<div class="row form-group">
								<div class="col col-md-3">
									<label for="meta_description" class=" form-control-label">Meta Description</label>
								</div>
								<div class="col-12 col-md-9">
									<input type="text" name="meta_description"  rows="9"  class="form-control" value="{{$webInfo->meta_description}}" placeholder="meta description for seo"><br>
								</div>
							</div>
						</div>
						<button type="submit" class="btn btn-primary btn-sm">
							<i class="fa fa-dot-circle-o"></i> Update
						</button>
					</div>
				</div>
			</div>
		</form>
		@endif
		<div class="card-footer">
			<div class="row">
				<!-- <div class="col-md-6"> -->
					<div class="col-md-12 help-image">

					</div>
				</div>
			</div>
		</div>
	</div>

</div><!--/.col-->

@endsection
@section('script')
<script type="text/javascript">
	function readURLL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#blah')
				.attr('src', e.target.result)
				.width('50%')
				.height('50%');
			};

			reader.readAsDataURL(input.files[0]);
		}
	}
	$(document).on("change", ".file_multi_video", function(evt) {
		var $source = $('#video_here');
		$source[0].src = URL.createObjectURL(this.files[0]);
		$source.parent()[0].load();
	});
</script>
@endsection