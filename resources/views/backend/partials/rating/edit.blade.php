    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Edit Ratings</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{route('rating.update',['rating'=>$rating['id']])}}" enctype="multipart/form-data">
                    {{method_field('patch')}}
                    {{csrf_field()}}
                    <div class="form-group">
                            <label for="category">Company Name</label>
                            <input type="text"value="{{$rating['company_name']}}" name="company_name" class="form-control" id="category" placeholder="Enter Company Name" required>
                        </div>
                        <div class="form-group">
                            <label for="industry">Company Industry</label>
                            <input type="text" value="{{$rating['industry']}}" name="industry" class="form-control" id="industry" placeholder="Enter Company industry" required>
                        </div>
                        <br>
                        <label>Ratings</label>
                        <hr>
                        <div class="form-group">
                            <label for="instrument_category">Instrument Category</label>
                            <input type="text" value="{{$rating['instrument_category']}}" name="instrument_category" class="form-control" id="instrument_category" placeholder="Enter instrument Category" required>
                        </div>
                        <div class="form-group">
                            <label for="rating">Company Rating</label>
                            <input type="text" value="{{$rating['rating']}}" name="rating" class="form-control" id="rating" placeholder="Enter Company Rating" required>
                        </div>
                        <div class="form-group">
                            <label for="outlook">Company Outlook</label>
                            <textarea name="outlook" class="form-control" id="outlook" cols="5" rows="10">{{$rating['outlook']}}</textarea>
                        </div>
                          <div class="form-group">
                            <label for="details">Details</label>
                            <textarea name="details" class="form-control" id="details" cols="5" rows="10">{{$rating['details']}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Select File</label>
                            <input type="file" name="pdf" accept="application/pdf,application/vnd.ms-excel" id="exampleInputFile">
                            <p class="help-block">File Must Be In PDF Format.</p>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
