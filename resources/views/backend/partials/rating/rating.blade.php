@extends('backend.layouts.app')
@section('style')
<style>
    .mail-body{
        max-width: 100%;
        margin: 0;
        padding: 0;
        background-color: #EAE3DD;
    }
    .mail-content{
        max-width: 600px;
        text-align: left;
        margin: auto;
        padding: 15px 0px;


    }

    .mail-heading{
        text-align: center;
    }

    .mail-heading , .book-info {
        border: 1px solid #6f1313;
        margin: 15px ;
        background-color: #ffffff;
        padding: 10px 0px;
    }
    .book-info{
        font-size: 18px;
    }


    .mail-body{
        max-width: 100%;
        margin: 0px 10px;
        padding: 0;
        background-color: #dbe0dd;
    }
    .mail-content{
        max-width: 800px;
        text-align: left;
        margin: auto;
        padding: 15px 0px;
    }

    h1{
        font-size: 35px;
    }
    h3{
        font-size: 30px;
    }

</style>
@endsection
@section('content')

<div class="banner">
    <h2>
        <a href="/admin/home">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Ratings</span>
    </h2>
</div>
<!--//banner-->
<!--faq-->
<div class="blank">
    <div style="display: none;">
    </div>

    <div class="blank-page">
        <div class="grid_3 grid_5">
            <div class="bs-example2 bs-example-padded-bottom">
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addModal">
                    Add Rating
                </button>
            </div>
            @include('backend.partials.rating.add')
        </div>
        <table id="datatable" class="table table-striped table-bordered" cellspacing="0"
        width="100%">
        <thead>
            <tr>
                <th class="th-sm">#</th>
                <th class="th-sm">Company Name</th>
                <th class="th-sm">Industry</th>
                <th class="th-sm">PDF</th>
                <th class="th-sm">Status</th>
                <th class="th-sm">Actions
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($ratings as $rating)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$rating['company_name']}}</td>
                <td>{{$rating['industry']}}</td>
                <td>
                    <div class="col-md">
                        <div class="gallery-img">
                            <a target="_blank" href="{{asset('images/rating/'.$rating['pdf'])}}"><i class="fa fa-file-pdf-o make-large" aria-hidden="true"></i></a>

                        </div>
                    </div>
                </td>
                <td><a href="{{ route('rating.status.change', $rating->id) }}" onclick="return confirm('Are you sure you change status?')" class="btn @if($rating->status == 1)btn-success @else btn-primary @endif btn-sm" title="Click to change the front Active status.">{{ $rating->status == 1 ? 'Published' : 'Not Published' }}</a></td>
                <td>
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                    data-target="#branchedit{{$rating['id']}}">
                    <i class="fa fa-eye"></i>
                </button>&ensp;&ensp; &ensp;&ensp;
                <button data-toggle="modal" data-target="#editModal{{$rating['id']}}">
                    <i class="fa fa-pencil-square text-primary fa-2x"></i>
                </button>&ensp;&ensp;
                <form action="{{ route('rating.destroy', ['rating'=>$rating['id']]) }}" method="POST">
                    @method('DELETE')
                    @csrf
                    <br>
                    <button onclick="return confirm('Are you sure you want to delete?')">  <i class="fa fa-trash text-danger fa-2x"></i></button>
                </form>
            </td>
            <div class="modal fade" id="editModal{{$rating['id']}}" tabindex="-1" role="dialog"
            aria-labelledby="myModalLabel"
            aria-hidden="true" style="display: none;">
            @include('backend.partials.rating.edit')
        </div>
        <div class="modal fade" style="margin-left: 125px" id="branchedit{{$rating['id']}}"
        tabindex="-1"
        role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Rating Us</h5>
                    <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="mail-body text-center">
                <div class="mail-content">
                    <div class="mail-heading">
                        <h3>Rating Information</h3>
                    </div>
                    <div class="book-info">
                        <div class="row col-md-12">
                            <br>
                            <div class="client-name col-md-4">Instrument Category</div>
                            <div class="col-md-1">:</div>
                            <div class="cli-name col-md-7">{{$rating['instrument_category']}}</div>
                        </div>
                        <div class="row col-md-12">
                            <div class="client-name col-md-4">Rating</div>
                            <div class="col-md-1">:</div>
                            <div class="cli-name col-md-7">{{$rating['rating']}}</div>
                        </div>
                        <div class="row col-md-12">
                            <div class="client-name col-md-4">Outlook </div>
                            <div class="col-md-1">:</div>
                            <div class="cli-name col-md-7">
                             {!! $rating['outlook'] !!}
                         </div>
                     </div>
                     <div class="row col-md-12">
                        <div class="client-name col-md-4">Details</div>
                        <div class="col-md-1">:</div>
                        <div class="cli-name col-md-7">{!! $rating['details'] !!}</div>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
        </div>
    </div>
</div>
</div>
</tr>

@endforeach
</tbody>
<tfoot>
    <tr>
        <th class="th-sm">Company
        </th>
        <th class="th-sm">Industry
        </th>
        <th class="th-sm">Status</th>
        <th class="th-sm">Actions
        </th>
    </tr>
</tfoot>
</table>
</div>
</div>
</div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection
@section('script')
@endsection