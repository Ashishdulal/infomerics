    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Edit Press Release</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{route('press-release.update',['event'=>$event['id']])}}" enctype="multipart/form-data">
                    {{method_field('patch')}}
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="exampleInputFile">Select File</label>
                        <input type="file" name="file" accept="application/pdf,application/vnd.ms-excel" id="exampleInputFile">
                        <p class="help-block">File Must Be In PDF Format.</p>
                    </div>
                    <div class="form-group">
                            <label for="author">Subject</label>
                            <input type="text"value="{{$event['author_name']}}" name="author" class="form-control"  id="author" placeholder="Enter Subject" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Press Release Title</label>
                            <input type="text" value="{{$event['title']}}" name="title" class="form-control" id="title" placeholder="Enter Title" required>
                        </div>
                        <div class="form-group">
                            <label for="description">Press Release Description</label>
                            <textarea name="description" class="form-control" id="description" cols="5" rows="10">{{$event['description']}}</textarea>
                        </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
