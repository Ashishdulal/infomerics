@extends('backend.layouts.app')
@section('content')
    <div class="banner">
        <h2>
            <a href="/admin/home">Home</a>
            <i class="fa fa-angle-right"></i>
            <span>Press Release</span>
        </h2>
    </div>
    <!--//banner-->
    <!--faq-->
    <div class="blank">


        <div class="blank-page">

            <div class="grid_3 grid_5">
                <div class="bs-example2 bs-example-padded-bottom">
                    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addModal">
                        Add Press Release
                    </button>
                </div>
                @include('backend.partials.press-release.add')
            </div>
            <table class="table" id="datatable">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">File</th>
                    <th scope="col">Subject</th>
                    <th scope="col">Title</th>
                    <th scope="col">Description</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($events as $event)
                    <tr>
                <td scope="row">{{$loop->iteration}}</td>
                        <td>
                            <div class="col-md">
                                <div class="gallery-img">
                                    <a target="_blank" href="{{asset('images/event/'.$event['image'])}}"><i class="fa fa-file-pdf-o make-large" aria-hidden="true"></i></a>

                                </div>
                            </div>
                        </td>
                        <td>{{$event['author_name']}}</td>
                        <td>{{$event['title']}}</td>
                        <td> @if(strlen($event['description'])>300)
                                {!! substr($event['description'],0,strpos($event['description'],".",300))!!}
                            @else
                                {!!$event['description']!!}
                            @endif</td>
                        <td><button data-toggle="modal" data-target="#editModal{{$event['id']}}">
                                <i class="fa fa-pencil-square text-primary fa-2x"></i>
                            </button>&ensp;&ensp;
                            <form action="{{ route('press-release.destroy', ['event'=>$event['id']]) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button onclick="return confirm('Are you sure you want to delete?')">  <i class="fa fa-trash text-danger fa-2x"></i></button>
                            </form>
                           </td>
                        <div class="modal fade" id="editModal{{$event['id']}}" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel"
                             aria-hidden="true" style="display: none;">
                            @include('backend.partials.press-release.edit')
                        </div>

                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection