<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Add Press Release</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                    <form method="POST" action="{{route('press-release.store')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="exampleInputFile">Select File</label>
                            <input type="file" value="{{old('file')}}" name="file" accept="application/pdf,application/vnd.ms-excel" id="exampleInputFile" required>
                            <p class="help-block">File Must Be In PDF Format.</p>
                        </div>
                        <div class="form-group">
                            <label for="category">Subject</label>
                            <input type="text"value="{{old('author')}}" name="author" class="form-control" id="category" placeholder="Enter Subject" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Press Release Title</label>
                            <input type="text" value="{{old('title')}}" name="title" class="form-control" id="title" placeholder="Enter Title" required>
                        </div>
                        <div class="form-group">
                            <label for="description">Press Release Description</label>
                            <textarea name="description" value="{{old('description')}}" class="form-control" id="description" cols="5" rows="10"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
