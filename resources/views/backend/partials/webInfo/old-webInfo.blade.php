@extends('backend.layouts.app')
@section('css')
<style type="text/css">
    .gallery-img a img{
        margin: 0 auto;
    }
    .gallery-img a:hover .zoom-icon:before {
        left: 0%;
        top: -35%;
        height: 67%;
        width: 121%;
    }
</style>
@endsection
@section('content')
<div class="banner">
    <h2>
        <a href="/admin/home">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>webInfo</span>
    </h2>
</div>
<!--//banner-->
<!--faq-->
<div class="blank">


    <div class="blank-page">

        <table class="table">
     <!--        <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Overview</th>
                    <th scope="col">Our Purpose</th>
                    <th scope="col">BOD</th>
                    <th scope="col">Community</th>
                    <th scope="col">Service Title</th>
                    <th scope="col">Service Description</th>
                    <th scope="col">About us</th>
                    <th scope="col">About us image</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead> -->
            <tbody>
                <label>Page Action</label><br>
                <tr>
                    <button data-toggle="modal" data-target="#editModal{{$webInfo ? $webInfo['id'] : '1'}}" title="Edit Content">
                        <i class="fa fa-pencil-square text-primary fa-2x"></i>
                    </button>&ensp;&ensp;
                    <!-- @if(isset($webInfo))
                    <form action="{{ route('webinfo.destroy', ['webInfo'=>$webInfo ? $webInfo['id'] : '']) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button onclick="return confirm('Are you sure you want to delete?')" title="Delete All Contents">  <i class="fa fa-trash text-danger fa-2x"></i></button>
                    </form>
                    @endif -->
                </tr>
                <hr>
                <label>Overview </label>
                <tr>{!!$webInfo? substr_replace($webInfo->overview,' '.'...',500) : '--'!!}</tr>
                <hr>
                <label>Our Purpose</label>
                <tr>{!!$webInfo? substr_replace($webInfo->our_purpose,' '.'...',500) : '--'!!}</tr>
                <hr>
                <label>Board Of Directors</label>
                <tr>{!!$webInfo? substr_replace($webInfo->bod,' '.'...',500) : '--'!!}</tr>
                <hr>
                <label>Community</label>
                <tr>{!!$webInfo? substr_replace($webInfo->community,' '.'...',500) : '--'!!}</tr>
                <hr>
                <label>Service title</label>
                <tr>{!!$webInfo? substr_replace($webInfo->service_title,' '.'...',500) : '--'!!}</tr>
                <hr>
                <label>Service Description</label>
                <tr>{!!$webInfo? substr_replace($webInfo->service_description,' '.'...',500) : '--'!!}</tr>
                <hr>
                <label>About Us</label>
                <tr>{!!$webInfo? substr_replace($webInfo->about_us,' '.'...',500) : '--'!!}</tr>
                <hr>
                <label>About Us Image</label>
                <tr>
                    <div class="gallery-img">
                        <a @if(isset($webInfo)) target="_blank" href="/images/webinfo/{{$webInfo ? $webInfo->about_us_image : '--'}}" @else href="#" @endif
                         class="b-link-stripe" title="Image Title">
                         <img class="img-responsive" src="/images/webinfo/{{$webInfo ? $webInfo->about_us_image : '--'}}" alt="Image">
                         <span class="zoom-icon"> </span> </a>

                     </div>
                 </tr>
                 <hr>

                 <div class="modal fade" id="editModal{{ $webInfo ? $webInfo['id'] : '1'}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                     @include('backend.partials.webInfo.edit')
                 </div>

             </tbody>
         </table>
     </div>
 </div>

 @endsection