@extends('backend.layouts.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="banner">
			<h2>
				<a href="/admin/home">Home</a>
				<i class="fa fa-angle-right"></i>
				<span>Web Info</span>
			</h2>
		</div>
		<!--//banner-->
		<!--faq-->
		<div class="blank">


			<div class="blank-page">
				@if($errors->any())
				@foreach($errors->all() as $error)
				<ul>
					<li>{{$error}}</li>
				</ul>
				@endforeach
				@endif
				@if($webInfo)
				<form action="{{route('webinfo.update',['webinfo'=>($webInfo ? $webInfo['id'] : 'create')])}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
					{{method_field('patch')}}
					{{csrf_field()}}
					<div style="text-align: right;margin: 20px 40px 0 0px;" class="home-btn">
						<button type="submit" class="btn btn-primary btn-sm">
							<i class="fa fa-dot-circle-o"></i> Update
						</button>
					</div>
					<div class="card-body card-block">
						<div class="container-fluid">
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" class="tab-gap" href="#home">Home Page</a></li>
								<li><a data-toggle="tab" class="tab-gap" href="#menu2">About Us</a></li>
								<li><a data-toggle="tab" class="tab-gap" href="#menu1">Career</a></li>
								<li><a data-toggle="tab" class="tab-gap" href="#menu4">Rating</a></li>
								<li><a data-toggle="tab" class="tab-gap" href="#menu3">Contact Us</a></li>
								<li></li>
							</ul>
							<div class="tab-content">
								<div id="home" class="tab-pane active">
									<div class="row form-group">
										<div class="col col-md-3">
											<label for="site_title" class=" form-control-label">Service Menu</label>
										</div>
										<div class="col-12 col-md-9">
											<select class="form-control" name="service_menu">
												<option value="">Disable service menu</option>
												@foreach($events as $event)
												<option @if($webInfo->service_menu == $event->id) selected @endif value="{{$event->id}}">{{$event->title}}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="row form-group">
										<div class="col col-md-3">
											<label for="site_title" class=" form-control-label">Service Info</label>
										</div>
										<div class="col-12 col-md-9">
											<textarea name="text1"  rows="9"  class="form-control ckeditor">{{$webInfo->text1}}</textarea>
										</div>
									</div>
									<div class="row form-group">
										<div class="col col-md-3">
											<label for="site_title" class=" form-control-label">About Us Info</label>
										</div>
										<div class="col-12 col-md-9">
											<textarea name="about_us"  rows="9"  class="form-control ckeditor">{{$webInfo->about_us}}</textarea>
										</div>
									</div>
									<div class="row form-group">
										<div class="col col-md-3">
											<label for="about image" class=" form-control-label">About Us image</label><br>
											<span class="au-breadcrumb-span">[ Note: Please upload the small image size image less than 1mb ]</span>
										</div>
										<div class="col-12 col-md-9 process">
											<input onchange="readURL(this);" type="file" id="about-image" accept="image/png, image/jpg, image/jpeg" name="about_us_image" class="form-control-file"><br>
											<img style="max-width: 40%;" id="blah" src="/images/webinfo/{{$webInfo->about_us_image}}" alt="About Us">
										</div>
									</div>
							</div>
							<div id="menu1" class="tab-pane fade">
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="life_at_infomerics" class=" form-control-label">Life at Infomerics</label>
									</div>
									<div class="col-12 col-md-9">
										<textarea name="life_at_infomerics"  rows="9"  class="form-control ckeditor">{{$webInfo->life_at_infomerics}}</textarea>
									</div>
								</div>
								<div class="row form-group">
										<div class="col col-md-3">
											<label for="life-at-image" class=" form-control-label">Life at infomerics image</label><br>
											<span class="au-breadcrumb-span">[ Note: Please upload the small image size image less than 1mb ]</span>
										</div>
										<div class="col-12 col-md-9 process">
											<input onchange="readURL(this);" type="file" id="life-image" accept="image/png, image/jpg, image/jpeg" name="life_at_infomerics_image" class="form-control-file"><br>
											<img style="max-width: 40%;" id="blah" src="/images/webinfo/{{$webInfo->life_at_infomerics_image}}" alt="About Us">
										</div>
									</div>
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="our_people" class=" form-control-label">Our People</label>
									</div>
									<div class="col-12 col-md-9">
										<textarea name="our_people"  rows="9"  class="form-control ckeditor">{{$webInfo->our_people}}</textarea>
									</div>
								</div>
								<div class="row form-group">
										<div class="col col-md-3">
											<label for="about image" class=" form-control-label">Our People image</label><br>
											<span class="au-breadcrumb-span">[ Note: Please upload the small image size image less than 1mb ]</span>
										</div>
										<div class="col-12 col-md-9 process">
											<input onchange="readURL(this);" type="file" id="people-image" accept="image/png, image/jpg, image/jpeg" name="our_people_image" class="form-control-file"><br>
											<img style="max-width: 40%;" id="blah" src="/images/webinfo/{{$webInfo->our_people_image}}" alt="About Us">
										</div>
									</div>
							</div>
							<div id="menu4" class="tab-pane fade">
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="rating" class=" form-control-label">Rating Text Info</label>
									</div>
									<div class="col-12 col-md-9">
										<textarea name="rating"  rows="9"  class="form-control ckeditor">{{$webInfo->rating}}</textarea>
									</div>
								</div>
							</div>
							<div id="menu2" class="tab-pane fade-in">
									<div class="row form-group">
										<div class="col col-md-3">
											<label for="site_title" class=" form-control-label">Our Mission Info</label>
										</div>
										<div class="col-12 col-md-9">
											<textarea name="our_mission"  rows="9"  class="form-control ckeditor">{{$webInfo->our_mission}}</textarea>
										</div>
									</div>
									<div class="row form-group">
										<div class="col col-md-3">
											<label for="about image" class=" form-control-label">Our Mission image</label><br>
											<span class="au-breadcrumb-span">[ Note: Please upload the small image size image less than 1mb ]</span>
										</div>
									<div class="col-12 col-md-9">
										<input onchange="readURLFav(this);" type="file" id="our_mission_image" accept="image/png, image/jpg, image/jpeg" name="our_mission_image" class="form-control-file"><br>
										<img style="max-width: 40%;" id="favBlah" src="/images/webinfo/{{$webInfo->our_mission_image}}" alt="site Favicon">
									</div>
								</div>
								<div class="row form-group">
										<div class="col col-md-3">
											<label for="site_title" class=" form-control-label">Our Vision Info</label>
										</div>
										<div class="col-12 col-md-9">
											<textarea name="our_vision"  rows="9"  class="form-control ckeditor">{{$webInfo->our_vision}}</textarea>
										</div>
									</div>
									<div class="row form-group">
										<div class="col col-md-3">
											<label for="about image" class=" form-control-label">Our Vision image</label><br>
											<span class="au-breadcrumb-span">[ Note: Please upload the small image size image less than 1mb ]</span>
										</div>
									<div class="col-12 col-md-9">
										<input onchange="readURLFav(this);" type="file" id="our_vision_image" accept="image/png, image/jpg, image/jpeg" name="our_vision_image" class="form-control-file"><br>
										<img style="max-width: 40%;" id="favBlah" src="/images/webinfo/{{$webInfo->our_vision_image}}" alt="site Favicon">
									</div>
								</div>
								<div class="row form-group">
										<div class="col col-md-3">
											<label for="site_title" class=" form-control-label">Board Of Directors</label>
										</div>
										<div class="col-12 col-md-9">
											<textarea name="text2"  rows="9"  class="form-control ckeditor">{{$webInfo->text2}}</textarea>
										</div>
									</div>
							</div>
							<div id="menu3" class="tab-pane fade">
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="nepal_contact" class=" form-control-label">Nepal Contact</label>
									</div>
									<div class="col-12 col-md-9">
										<textarea name="nepal_contact"  rows="9"  class="form-control ckeditor">{{$webInfo->nepal_contact}}</textarea>
									</div>
								</div>
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="india_contact" class=" form-control-label">India Contact</label>
									</div>
									<div class="col-12 col-md-9">
										<textarea name="india_contact"  rows="9"  class="form-control ckeditor">{{$webInfo->india_contact}}</textarea>
									</div>
								</div>
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="map" class=" form-control-label">Map Iframe</label>
									</div>
									<div class="col-12 col-md-9">
										<input type="text" name="map"  value="{{$webInfo->map}}"  class="form-control">
									</div>
								</div>
							</div>
							<button type="submit" class="btn btn-primary btn-sm">
								<i class="fa fa-dot-circle-o"></i> Update
							</button>
						</div>
					</div>
				</div>
			</form>
			@endif
			<div class="card-footer">
				<div class="row">
					<!-- <div class="col-md-6"> -->
						<div class="col-md-12 help-image">

						</div>
					</div>
				</div>
			</div>
		</div>

	</div><!--/.col-->

	@endsection
	@section('script')
	<script type="text/javascript">
		function readURLL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#blah')
					.attr('src', e.target.result)
					.width('50%')
					.height('50%');
				};

				reader.readAsDataURL(input.files[0]);
			}
		}
		$(document).on("change", ".file_multi_video", function(evt) {
			var $source = $('#video_here');
			$source[0].src = URL.createObjectURL(this.files[0]);
			$source.parent()[0].load();
		});
	</script>
	@endsection