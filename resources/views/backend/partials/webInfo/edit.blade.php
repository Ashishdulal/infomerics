    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4>Edit webInfo</h4>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{route('webinfo.update',['webinfo'=>($webInfo ? $webInfo['id'] : 'create')])}}" enctype="multipart/form-data">
                    {{method_field('patch')}}
                    {{csrf_field()}}
                    <button style="float: right;" type="submit" class="btn btn-default">Submit</button> <br>
                    <div class="form-group">
                        <label for="overview">Overview</label>
                        <textarea class="form-control" name="overview">{{$webInfo ? $webInfo['overview'] : ''}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="ourpurpose">Our Purpose</label>
                        <textarea class="form-control" name="our_purpose">{{$webInfo ? $webInfo['our_purpose'] : ''}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="bod">BOD</label>
                        <textarea class="form-control" name="bod">{{$webInfo ? $webInfo['bod'] : ''}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="community">Community</label>
                        <textarea class="form-control" name="community">{{$webInfo ? $webInfo['community'] : ''}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="service_title">Service Title</label>
                        <textarea class="form-control" name="service_title">{{$webInfo ? $webInfo['service_title'] : ''}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="servicedescription">Service Description</label>
                        <textarea class="form-control" name="service_description">{{$webInfo ? $webInfo['service_description'] : ''}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="about_us">About us</label>
                        <textarea class="form-control" name="about_us">{{$webInfo ? $webInfo['about_us'] : ''}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="about_us_image">About Us Image</label>
                        <input type="file" name="about_us_image" accept=" .jpg, .jpeg, .png, .svg" class="form-control" id="about_us_image">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
