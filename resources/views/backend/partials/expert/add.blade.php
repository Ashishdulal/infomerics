<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Add Management Team</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{route('expert.store')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="exampleInputFile">Select Image</label>
                        <input type="file" value="{{old('image')}}" name="image" id="exampleInputFile" required>
                        <p class="help-block">File Must Be In Image Format. Insert Image of 3 X 2</p>
                    </div>

                    <div class="form-group">
                        <label for="title">Name</label>
                        <input type="text" value="{{old('name')}}" name="name" class="form-control"
                               placeholder="Enter Name" required>
                    </div>
                    <div class="form-group">
                        <label for="title">Designation</label>
                        <input type="text" value="{{old('designation')}}" name="designation" class="form-control"
                               placeholder="Enter Designation" required>
                    </div>
                    <div class="form-group">
                        <label for="title">Year Of Excellence</label>
                        <input type="text" value="{{old('year')}}" name="year" class="form-control"
                               placeholder="Enter Name" required>
                    </div>

                    <div class="form-group">
                        <label for="description">Key Areas Of Responsibilities</label>
                        <textarea name="bio" value="{{old('bio')}}" class="form-control" cols="5" rows="10"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
