@extends('backend.layouts.app')
@section('content')
    <div class="banner">
        <h2>
            <a href="/admin/home">Home</a>
            <i class="fa fa-angle-right">Expert Team</i>
        </h2>
    </div>
    <!--//banner-->
    <!--faq-->
    <div class="blank">


        <div class="blank-page">

            <div class="grid_3 grid_5">
                <div class="bs-example2 bs-example-padded-bottom">
                    <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#addModal">
                        Add Expert Team
                    </button>
                </div>
                @include('backend.partials.expert.add')
            </div>
            <table class="table" id="datatable">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Image</th>
                    <th scope="col">Name</th>
                    <th scope="col">Designation</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($experts as $expert)
                    <tr>

                        <th scope="row">1</th>
                        <td>
                            <div class="col-md">
                                <div class="gallery-img">
                                    <a href="{{asset('images/expert/'.$expert['image'])}}"
                                       class="b-link-stripe b-animate-go  swipebox" title="Image Title">
                                        <img class="img-responsive" src="{{asset('images/expert/'.$expert['image'])}}" alt="">
                                        <span class="zoom-icon"> </span> </a>

                                </div>
                            </div>
                        </td>
                        <td>{{$expert['name']}}</td>
                        <td>{{$expert['designation']}}</td>
                        <td><button data-toggle="modal" data-target="#editModal{{$expert['id']}}">
                                <i class="fa fa-pencil-square text-primary fa-2x"></i>
                            </button>&ensp;&ensp;
                            <form action="{{ route('expert.destroy', ['expert'=>$expert['id']]) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button onclick="return confirm('Are you sure you want to delete?')">  <i class="fa fa-trash text-danger fa-2x"></i></button>
                            </form>
                           </td>
                        <div class="modal fade" id="editModal{{$expert['id']}}" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel"
                             aria-hidden="true" style="display: none;">
                            @include('backend.partials.expert.edit')
                        </div>

                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection