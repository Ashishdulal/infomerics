@extends('backend.layouts.app')
@section('content')
    <div class="banner">
        <h2>
            <a href="/admin/home">Home</a>
            <i class="fa fa-angle-right"></i>
            <span>Gallery</span>
            <i class="fa fa-angle-right"></i>
            <span>Image</span>
        </h2>
    </div>
    <!--//banner-->
    <!--faq-->
    <div class="blank">


        <div class="blank-page">

            <div class="grid_3 grid_5">
                <div class="bs-example2 bs-example-padded-bottom">
                    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addModal">
                        Add Gallery Image
                    </button>
                </div>
                @include('backend.partials.gallery.image.add')
            </div>
            <table class="table" id="datatable">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Image</th>
                    <th scope="col">Title</th>
                    <th scope="col">Description</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @php $i=0; @endphp
                @foreach($images as $image)
                    <tr>

                        <th scope="row">{{++$i}}</th>
                        <td>
                            <div class="col-md">
                                <div class="gallery-img">
                                    <a href="{{asset('images/image/'.$image['image'])}}"
                                       class="b-link-stripe b-animate-go  swipebox" title="Image Title">
                                        <img class="img-responsive " src="{{asset('images/image/'.$image['image'])}}" alt="">
                                        <span class="zoom-icon"> </span> </a>

                                </div>
                            </div>
                        </td>

                        <td>{{$image['title']}}</td>
                        <td>
                            @if(strlen($image['description'])>300)
                                {!! substr($image['description'],0,strpos($image['description'],".",300))!!}
                            @else
                                {!!$image['description']!!}
                            @endif
                        </td>
                        <td><button href="" data-toggle="modal" data-target="#editModal{{$image['id']}}">
                                <i class="fa fa-pencil-square text-primary fa-2x"></i>
                            </button>&ensp;&ensp;
                            <form action="{{ route('image.destroy', ['image'=>$image['id']]) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button onclick="return confirm('Are you sure you want to delete?')">  <i class="fa fa-trash text-danger fa-2x"></i></button>
                            </form>
                        </td>
                        <div class="modal fade" id="editModal{{$image['id']}}" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel"
                             aria-hidden="true" style="display: none;">
                            @include('backend.partials.gallery.image.edit')
                        </div>

                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection