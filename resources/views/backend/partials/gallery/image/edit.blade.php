<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4>Edit Gallery Image</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
            <form method="POST" action="{{route('image.update',['image'=>$image['id']])}}" enctype="multipart/form-data">
                {{method_field('patch')}}
                {{csrf_field()}}
                <div class="form-group">
                    <label for="exampleInputFile">Select Image</label>
                    <input type="file" name="image" id="exampleInputFile">
                    <p class="help-block">File Must Be In Image Format. Insert Image of 3 X 2</p>
                </div>
                <div class="form-group">
                    <label for="title">Gallery Image Title</label>
                    <input type="text" name="title" value="{{$image['title']}}" class="form-control" id="title" placeholder="Enter Title">
                </div>
                <div class="form-group">
                    <label for="description">Gallery Image Description</label>
                    <textarea name="description" class="form-control" id="description" cols="5" rows="10">{{$image['description']}}</textarea>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
