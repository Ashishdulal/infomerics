@extends('backend.layouts.app')
@section('content')
    <div class="banner">
        <h2>
            <a href="/admin/home">Home</a>
            <i class="fa fa-angle-right"></i>
            <span>Gallery</span>
            <i class="fa fa-angle-right"></i>
            <span>Video</span>
        </h2>
    </div>
    <!--//banner-->
    <!--faq-->
    <div class="blank">


        <div class="blank-page">

            <div class="grid_3 grid_5">
                <div class="bs-example2 bs-example-padded-bottom">
                    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addModal">
                        Add Gallery Video
                    </button>
                </div>
                @include('backend.partials.gallery.videos.add')
            </div>
            <table class="table" id="datatable">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Video</th>
                    <th scope="col">Title</th>
                    <th scope="col">Description</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($videos as $video)
                    <tr>

                        <th scope="row">1</th>
                        <td>
                            <div class="col-md">
                                <div class="gallery-img">
                                    <iframe width="150" height="100"
                                            src="https://www.youtube.com/embed/{{$video['key']}}">
                                    </iframe>

                                </div>
                            </div>
                        </td>

                        <td>{{$video['title']}}</td>
                        <td>

                            @if(strlen($video['description'])>300)
                                {!! substr($video['description'],0,strpos($video['description'],".",300))!!}
                            @else
                                {!!$video['description']!!}
                            @endif
                        </td>
                        <td><button data-toggle="modal" data-target="#editModal{{$video['id']}}">
                                <i class="fa fa-pencil-square text-primary fa-2x"></i>
                            </button>&ensp;&ensp;
                            <form action="{{ route('video.destroy', ['video'=>$video['id']]) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button onclick="return confirm('Are you sure you want to delete?')">  <i class="fa fa-trash text-danger fa-2x"></i></button>
                            </form>
                        </td>
                        <div class="modal fade" id="editModal{{$video['id']}}" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel"
                             aria-hidden="true" style="display: none;">
                            @include('backend.partials.gallery.videos.edit')
                        </div>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection