<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4>Edit Gallery Video</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
            <form method="POST" action="{{route('video.update',['video'=>$video['id']])}}" enctype="multipart/form-data">
                {{method_field('patch')}}
                {{csrf_field()}}
                <div class="form-group">
                    <label for="exampleInputFile">Enter Video URL</label>
                    <input type="text" value="https://www.youtube.com/embed/{{$video['key']}}" class="form-control" name="url" id="exampleInputFile">
                    <p class="help-block">File Must Be In YouTube Video Format.</p>
                </div>
                <div class="form-group">
                    <label for="title">Gallery Video Title</label>
                    <input type="text" name="title" value="{{$video['title']}}" class="form-control" id="title" placeholder="Enter Title">
                </div>
                <div class="form-group">
                    <label for="description">Gallery Video Description</label>
                    <textarea name="description" class="form-control" id="description" cols="5" rows="10">{{$video['description']}}</textarea>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
