<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Add Gallery Video</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{route('video.store')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="exampleInputFile">Enter Video URL</label>
                        <input type="text" class="form-control" value="{{old('url')}}" name="url" id="exampleInputFile" required>
                        <p class="help-block">File Must Be In Youtube Video Format.</p>
                    </div>

                    <div class="form-group">
                        <label for="title">Gallery Video Title</label>
                        <input type="text" value="{{old('title')}}" name="title" class="form-control" id="title" placeholder="Enter Title" required>
                    </div>
                    <div class="form-group">
                        <label for="description">Gallery Video Description</label>
                        <textarea name="description" value="{{old('description')}}" class="form-control" id="description" cols="5" rows="10"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
