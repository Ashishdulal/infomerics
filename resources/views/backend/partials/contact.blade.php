@extends('backend.layouts.app')
@section('style')
    <style>
        .mail-body{
            max-width: 100%;
            margin: 0;
            padding: 0;
            background-color: #EAE3DD;
        }
        .mail-content{
            max-width: 600px;
            text-align: left;
            margin: auto;
            padding: 15px 0px;


        }

        .mail-heading{
            text-align: center;
        }

        .mail-heading , .book-info {
            border: 1px solid #6f1313;
            margin: 15px ;
            background-color: #ffffff;
            padding: 10px 0px;
        }
        .book-info{
            font-size: 18px;
        }


        .mail-body{
            max-width: 100%;
            margin: 0px 10px;
            padding: 0;
            background-color: #dbe0dd;
        }
        .mail-content{
            max-width: 800px;
            text-align: left;
            margin: auto;
            padding: 15px 0px;
        }

        h1{
            font-size: 35px;
        }
        h3{
            font-size: 30px;
        }




    </style>
@endsection
@section('content')

    <div class="banner">
        <h2>
            <a href="/admin/home">Home</a>
            <i class="fa fa-angle-right"></i>
            <span>Contact Us</span>
        </h2>
    </div>
    <!--//banner-->
    <!--faq-->
    <div class="blank">


        <div class="blank-page">
            <table id="datatable" class="table table-striped table-bordered" cellspacing="0"
                   width="100%">
                <thead>
                <tr>
                    <th class="th-sm">Full Name
                    </th>
                    <th class="th-sm">Email
                    </th>
                    <th>Contact</th>
                    <th>topic</th>
                    <th> Message</th>
                    <th class="th-sm">Actions
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($contacts as $contact)
                    <tr>
                        <td>{{$contact['name']}}</td>
                        <td>{{$contact['email']}}</td>
                        <td>{{$contact['phone']}}</td>
                        <td>{{$contact['topic']}}</td>
                        <td> @if(strlen($contact['message'])>300)
                                {!! substr($contact['message'],0,strpos($contact['message'],".",300))!!}
                            @else
                                {!!$contact['message']!!}
                            @endif</td>
                        <td>
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#branchedit{{$contact->id}}">
                                <i class="fa fa-eye"></i>
                            </button>
                        </td>
                        <div class="modal fade" style="margin-left: 125px" id="branchedit{{$contact->id}}"
                             tabindex="-1"
                             role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Contact Us</h5>
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="mail-body text-center">
                                        <div class="mail-content">
                                            <div class="mail-heading">
                                                <h3>Contact Information</h3>
                                            </div>
                                            <div class="book-info">
                                                <div class="row col-md-12">
                                                    <br>
                                                    <div class="client-name col-md-4">Date</div>
                                                    <div class="col-md-1">:</div>
                                                    <div class="cli-name col-md-7">{{$contact['created_at']->format('dS F Y')}}</div>

                                                </div>
                                                <div class="row col-md-12">
                                                    <div class="client-name col-md-4">Full Name</div>
                                                    <div class="col-md-1">:</div>
                                                    <div class="cli-name col-md-7">{{$contact['name']}}</div>

                                                </div>
                                                <div class="row col-md-12">
                                                    <div class="client-name col-md-4">Contact No.</div>
                                                    <div class="col-md-1">:</div>
                                                    <div class="cli-name col-md-7">{{$contact['phone']}}</div>

                                                </div>
                                                <div class="row col-md-12">
                                                    <div class="client-name col-md-4">Email</div>
                                                    <div class="col-md-1">:</div>
                                                    <div class="cli-name col-md-7">{{$contact['email']}}</div>

                                                </div>
                                                 <div class="row col-md-12">
                                                    <div class="client-name col-md-4">Topic</div>
                                                    <div class="col-md-1">:</div>
                                                    <div class="cli-name col-md-7">{{$contact['topic']}}</div>

                                                </div>
                                                 <div class="row col-md-12">
                                                    <div class="client-name col-md-4">Business Name</div>
                                                    <div class="col-md-1">:</div>
                                                    <div class="cli-name col-md-7">{{$contact['business_name']}}</div>

                                                </div>
                                                 <div class="row col-md-12">
                                                    <div class="client-name col-md-4">Business Sector</div>
                                                    <div class="col-md-1">:</div>
                                                    <div class="cli-name col-md-7">{{$contact['business_sector']}}</div>

                                                </div>
                                                <div class="row col-md-12">
                                                    <div class="client-name col-md-4">Message</div>
                                                    <div class="col-md-1">:</div>
                                                    <div class="cli-name col-md-7">{{$contact['message']}}</div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </tr>

                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th class="th-sm">Full Name
                    </th>
                    <th class="th-sm">Email
                    </th>
                    <th>Contact</th>
                    <th> Message</th>
                    <th class="th-sm">Actions
                    </th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
    </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection
@section('script')
@endsection