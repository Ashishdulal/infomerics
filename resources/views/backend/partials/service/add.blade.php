<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
style="display: none;">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4>Add service</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
            <form method="POST" action="{{route('service.store')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="exampleInputFile">Select Icon Image</label>
                    <input type="file" value="{{old('icon')}}" name="icon" id="exampleInputFile" required>
                    <p class="help-block">File Must Be In Icon Image Format. Insert Icon Image of 3 X 2</p>
                </div>
                <div class="form-group">
                    <label for="name">service Title</label>
                    <input type="text" value="{{old('name')}}" name="name" class="form-control" id="name"
                    placeholder="Enter Service Title" required>
                </div>
<!--                 <div class="form-group d-none">
                    <label for="title">Select Category</label>
                    <select class="form-control" name="cat_id">
                        <option value="1">Rating Services</option>
                        <option value="2">Grading Services</option>
                    </select>
                </div> -->
                <div class="form-group">
                    <label for="description">service Description</label>
                    <textarea name="description" value="{{old('description')}}" class="form-control"
                    id="description" cols="5" rows="10"></textarea>
                </div>
                <div class="form-group">
                    <label for="title">Button Name</label>
                    <input type="text" value="{{old('button_name','Read More')}}" name="button_name" class="form-control" id="button_name" placeholder="Enter Button Name" required>
                </div>
                <div class="form-group">
                    <label for="title">Button URL</label>
                    <input type="text" value="{{old('button_url','/')}}" name="button_url" class="form-control" id="button_url" placeholder="Enter button url" required>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>
