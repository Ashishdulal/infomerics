@extends('backend.layouts.app')
@section('content')
    <div class="banner">
        <h2>
            <a href="/admin/home">Home</a>
            <i class="fa fa-angle-right"></i>
            <span>service</span>
        </h2>
    </div>
    <!--//banner-->
    <!--faq-->
    <div class="blank">


        <div class="blank-page">

            <div class="grid_3 grid_5">
                <div class="bs-example2 bs-example-padded-bottom">
                    <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#addModal">
                        Add Service
                    </button>
                </div>
                @include('backend.partials.service.add')
            </div>
            <table class="table" id="datatable">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Icon</th>
                    <th scope="col">Title</th>
                    <th scope="col">Description</th>
                    <th scope="col">Button Name</th>
                    <th scope="col">Button link</th>
                    <!-- <th scope="col">Category</th> -->
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($services as $service)
                    <tr>

                        <th scope="row">{{$loop->iteration}}</th>
                        <td>
                            <div class="col-md">
                                <div class="gallery-img">
                                    <a href="{{asset('images/service/'.$service['icon'])}}"
                                       class="b-link-stripe b-animate-go  swipebox" title="Image Title">
                                        <img style="max-width: 100%;" class="img-icon" src="{{asset('images/service/'.$service['icon'])}}" alt="">
                                        <span class="zoom-icon"> </span> </a>

                                </div>
                            </div>
                        </td>
                        <td>{{$service['name']}}</td>
                        <td> @if(strlen($service['description'])>300)
                                {!! substr($service['description'],0,strpos($service['description'],".",300))!!}
                            @else
                                {!!$service['description']!!}
                            @endif</td>
                            <td>{{$service['button_name']}}</td>
                            <td><a target="_blank" href="{{$service['button_url']}}">{{$service['button_url']}}</a></td>
                            <!-- <td>
                                @if($service['cat_id'] == 1)
                                Rating Services
                                @elseif($service['cat_id'] == 2)
                                Grading Services
                                @else 
                                N/A
                                @endif
                            </td> -->
                        <td><button data-toggle="modal" data-target="#editModal{{$service['id']}}">
                                <i class="fa fa-pencil-square text-primary fa-2x"></i>
                            </button>&ensp;&ensp;
                            <form action="{{ route('service.destroy', ['service'=>$service['id']]) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button onclick="return confirm('Are you sure you want to delete?')">  <i class="fa fa-trash text-danger fa-2x"></i></button>
                            </form>
                           </td>
                        <div class="modal fade" id="editModal{{$service['id']}}" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel"
                             aria-hidden="true" style="display: none;">
                            @include('backend.partials.service.edit')
                        </div>

                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection