    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Edit Slider</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{route('slider.update',['slider'=>$slider['id']])}}" enctype="multipart/form-data">
                    {{method_field('patch')}}
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="Image_input">Select Image</label>
                        <input type="file" name="slider_image" >
                        <p class="help-block">File Must Be In Image Format. Insert Image of 3 X 2</p>
                    </div>
                    <div class="form-group">
                        <label for="title">slider Title</label>
                        <input type="text" name="title" value="{{$slider['title']}}" class="form-control" placeholder="Enter Title">
                    </div>
                    <div class="form-group">
                        <label for="description">slider Description</label>
                        <textarea name="description" class="form-control" cols="5" rows="10">{{$slider['description']}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="title">Button Name</label>
                        <input type="text" value="{{$slider['button_name']}}" name="button_name" class="form-control"  placeholder="Enter Button Name" required>
                    </div>
                    <div class="form-group">
                        <label for="title">Button URL</label>
                        <input type="text" value="{{$slider['url_link']}}" name="url_link" class="form-control"  placeholder="Enter button url" required>
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
