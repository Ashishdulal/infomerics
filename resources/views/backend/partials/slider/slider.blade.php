@extends('backend.layouts.app')
@section('content')
    <div class="banner">
        <h2>
            <a href="/admin/home">Home</a>
            <i class="fa fa-angle-right"></i>
            <span>Slider</span>
        </h2>
    </div>
    <!--//banner-->
    <!--faq-->
    <div class="blank">


        <div class="blank-page">

            <div class="grid_3 grid_5">
                <div class="bs-example2 bs-example-padded-bottom">
                    <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#addModal">
                        Add Slider
                    </button>
                </div>
                @include('backend.partials.slider.add')
            </div>
            <table class="table" id="datatable">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Image</th>
                    <th scope="col">Title</th>
                    <th scope="col">Description</th>
                    <th scope="col">Button Name</th>
                    <th scope="col">Button link</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($sliders as $slider)
                    <tr>

                        <th scope="row">{{$loop->iteration}}</th>
                        <td>
                            <div class="col-md">
                                <div class="gallery-img">
                                    <a href="{{asset('images/slider/'.$slider['slider_image'])}}"
                                       class="b-link-stripe b-animate-go  swipebox" title="Image Title">
                                        <img class="img-responsive" src="{{asset('images/slider/'.$slider['slider_image'])}}" alt="">
                                        <span class="zoom-icon"> </span> </a>

                                </div>
                            </div>
                        </td>
                        <td>{{$slider['title']}}</td>
                        <td> @if(strlen($slider['description'])>300)
                                {!! substr($slider['description'],0,strpos($slider['description'],".",300))!!}
                            @else
                                {!!$slider['description']!!}
                            @endif</td>
                            <td>{{$slider['button_name']}}</td>
                            <td><a target="_blank" href="{{$slider['url_link']}}">{{$slider['url_link']}}</a></td>
                        <td><button data-toggle="modal" data-target="#editModal{{$slider['id']}}">
                                <i class="fa fa-pencil-square text-primary fa-2x"></i>
                            </button>&ensp;&ensp;
                            <form action="{{ route('slider.destroy', ['slider'=>$slider['id']]) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button onclick="return confirm('Are you sure you want to delete?')">  <i class="fa fa-trash text-danger fa-2x"></i></button>
                            </form>
                           </td>
                        <div class="modal fade" id="editModal{{$slider['id']}}" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel"
                             aria-hidden="true" style="display: none;">
                            @include('backend.partials.slider.edit')
                        </div>

                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection