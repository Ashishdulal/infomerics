<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4>Edit Category</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
            <form method="POST" action="{{route('category.update',['category'=>$category['id']])}}"
                  enctype="multipart/form-data">
                {{method_field('patch')}}
                {{csrf_field()}}

                <div class="form-group">
                    <label for="title">Category Title</label>
                    <input type="text" name="title" value="{{$category['title']}}" class="form-control" id="title"
                           placeholder="Enter Title">
                </div>

                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
