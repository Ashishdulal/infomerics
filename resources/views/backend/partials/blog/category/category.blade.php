@extends('backend.layouts.app')
@section('content')
    <div class="banner">
        <h2>
            <a href="/admin/home">Home</a>
            <i class="fa fa-angle-right"></i>
            <span>Blogs</span>
            <i class="fa fa-angle-right"></i>
            <span>Category</span>
        </h2>
    </div>
    <!--//banner-->
    <!--faq-->
    <div class="blank">


        <div class="blank-page">

            <div class="grid_3 grid_5">
                <div class="bs-example2 bs-example-padded-bottom">
                    <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#addModal">
                        Add Category
                    </button>
                </div>
                @include('backend.partials.blog.category.add')
            </div>
            <table class="table" id="datatable">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>

                        <th scope="row">1</th>


                        <td>{{$category['title']}}</td>

                        <td><button data-toggle="modal" data-target="#editModal{{$category['id']}}">
                                <i class="fa fa-pencil-square text-primary fa-2x"></i>
                            </button>&ensp;&ensp;
                            <form action="{{ route('category.destroy', ['category'=>$category['id']]) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button onclick="return confirm('Are you sure you want to delete?')">  <i class="fa fa-trash text-danger fa-2x"></i></button>
                            </form>
                        </td>
                        <div class="modal fade" id="editModal{{$category['id']}}" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel"
                             aria-hidden="true" style="display: none;">
                            @include('backend.partials.blog.category.edit')
                        </div>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection