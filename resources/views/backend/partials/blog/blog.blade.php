@extends('backend.layouts.app')
@section('content')
    <div class="banner">
        <h2>
            <a href="/admin/home">Home</a>
            <i class="fa fa-angle-right"></i>
            <span>Blogs</span>
        </h2>
    </div>
    <!--//banner-->
    <!--faq-->
    <div class="blank">


        <div class="blank-page">

            <div class="grid_3 grid_5">
                <div class="bs-example2 bs-example-padded-bottom">
                    <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#addModal">
                        Add Blog
                    </button>
                </div>
                @include('backend.partials.blog.add')
            </div>
            <table class="table" id="datatable">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Image</th>
                    <th scope="col">Category</th>
                    <th scope="col">Title</th>
                    <th scope="col">Description</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($blogs as $blog)
                    <tr>

                        <th scope="row">1</th>
                        <td>
                            <div class="col-md">
                                <div class="gallery-img">
                                    <a href="{{asset('images/blog/'.$blog['image'])}}"
                                       class="b-link-stripe b-animate-go  swipebox" title="Image Title">
                                        <img class="img-responsive" src="{{asset('images/blog/'.$blog['image'])}}" alt="">
                                        <span class="zoom-icon"> </span> </a>

                                </div>
                            </div>
                        </td>
                        <td>{{$blog->category['title']}}</td>
                        <td>{{$blog['title']}}</td>
                        <td> @if(strlen($blog['description'])>300)
                                {!! substr($blog['description'],0,strpos($blog['description'],".",300))!!}
                            @else
                                {!!$blog['description']!!}
                            @endif</td>
                        <td><button data-toggle="modal" data-target="#editModal{{$blog['id']}}">
                                <i class="fa fa-pencil-square text-primary fa-2x"></i>
                            </button>&ensp;&ensp;
                            <form action="{{ route('blog.destroy', ['blog'=>$blog['id']]) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button onclick="return confirm('Are you sure you want to delete?')">  <i class="fa fa-trash text-danger fa-2x"></i></button>
                            </form>
                           </td>
                        <div class="modal fade" id="editModal{{$blog['id']}}" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel"
                             aria-hidden="true" style="display: none;">
                            @include('backend.partials.blog.edit')
                        </div>

                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection