    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Edit Events And News</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{route('blog.update',['blog'=>$blog['id']])}}" enctype="multipart/form-data">
                    {{method_field('patch')}}
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="exampleInputFile">Select Image</label>
                        <input type="file" name="image" id="exampleInputFile">
                        <p class="help-block">File Must Be In Image Format. Insert Image of 3 X 2</p>
                    </div>
                    <div class="form-group">
                        <label for="category">Category</label>
                        <select name="category" class="form-control" id="">
                            <option value="{{$blog->category['id']}}" selected>{{$blog->category['title']}}</option>
                            @foreach($categories as $category)
                                <option value="{{$category['id']}}">{{$category['title']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="title">Author Name</label>
                        <input type="text" value="{{$blog['author_name']}}" name="author_name" class="form-control" id="title"
                               placeholder="Enter Title" required>
                    </div>
                    <div class="form-group">
                        <label for="title">Blog Title</label>
                        <input type="text" name="title" value="{{$blog['title']}}" class="form-control" id="title" placeholder="Enter Title">
                    </div>
                    <div class="form-group">
                        <label for="description">Blog Description</label>
                        <textarea name="description" class="form-control" id="description" cols="5" rows="10">{{$blog['description']}}</textarea>
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
