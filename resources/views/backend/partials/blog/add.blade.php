<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Add Events And News</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{route('blog.store')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="exampleInputFile">Select Image</label>
                        <input type="file" value="{{old('image')}}" name="image" id="exampleInputFile" required>
                        <p class="help-block">File Must Be In Image Format. Insert Image of 3 X 2</p>
                    </div>
                    <div class="form-group">
                        <label for="category">Category</label>
                        <select name="category" class="form-control" id="">
                            <option value="" selected disabled>Select Category</option>
                            @foreach($categories as $category)
                                <option value="{{$category['id']}}">{{$category['title']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="title">Author Name</label>
                        <input type="text" value="{{old('author_name')}}" name="author_name" class="form-control" id="title"
                               placeholder="Enter Title" required>
                    </div>
                    <div class="form-group">
                        <label for="title">Blog Title</label>
                        <input type="text" value="{{old('title')}}" name="title" class="form-control" id="title"
                               placeholder="Enter Title" required>
                    </div>
                    <div class="form-group">
                        <label for="description">Blog Description</label>
                        <textarea name="description" value="{{old('description')}}" class="form-control"
                                  id="description" cols="5" rows="10"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
