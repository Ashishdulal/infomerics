    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4>Edit webInfo</h4>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{route('webinfo.update',['webinfo'=>($webInfo ? $webInfo['id'] : 'create')])}}" enctype="multipart/form-data">
                    {{method_field('patch')}}
                    {{csrf_field()}}
                    <button style="float: right;" type="submit" class="btn btn-default">Submit</button> <br>
                    <div class="form-group">
                        <label for="career_text">Career Text</label>
                        <textarea class="form-control" name="career_text">{{$webInfo ? $webInfo['career_text'] : ''}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="career_image">Career Image</label>
                        <input type="file" name="career_image" accept=" .jpg, .jpeg, .png, .svg" class="form-control" id="career_image">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
