@extends('backend.layouts.app')
@section('content')
<div class="banner">
    <h2>
        <a href="/admin/home">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Job Opportunity</span>
    </h2>
</div>
<!--//banner-->
<!--faq-->
<div class="blank">


    <div class="blank-page">

        <div class="grid_3 grid_5">
            <div class="bs-example2 bs-example-padded-bottom">
                <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#addModal">
                    Add Job Opportunity
                </button>
            </div>
            @include('backend.partials.career.list.add')
        </div>
        <table class="table datatable1">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Job Title</th>
                    <th scope="col">Job Description</th>
                    <th scope="col">Job Responsibilities</th>
                    <th scope="col">Date</th>
                    <th scope="col">Status</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody id="sortable">
                @foreach($careers as $career)
                <tr class="row1" data-id="{{ $career->id }}">
                    <td scope="row">{{$career->position}}</td>
                    <td>{{$career['job_title']}}</td>
                    <td>
                        {!!$career['job_description']!!}
                    </td>
                    <td>
                        {!!$career['job_responsibilities']!!}
                    </td>
                    <td>Start Date:{{$career['start_date']}}<br>End Date:{{$career['end_date']}}</td>
                    <td><a href="{{ route('career.status.change', $career->id) }}" onclick="return confirm('Are you sure you change status?')" class="btn @if($career->status == 1)btn-success @else btn-primary @endif btn-sm" title="Click to change the front Active status.">{{ $career->status == 1 ? 'Active' : 'Not Active' }}</a></td>
                    <td><button data-toggle="modal" data-target="#editModal{{$career['id']}}">
                        <i class="fa fa-pencil-square text-primary fa-2x"></i>
                    </button>&ensp;&ensp;
                    <form action="{{ route('career.destroy', ['career'=>$career['id']]) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button onclick="return confirm('Are you sure you want to delete?')">  <i class="fa fa-trash text-danger fa-2x"></i></button>
                    </form>
                </td>
                <div class="modal fade" id="editModal{{$career['id']}}" tabindex="-1" role="dialog"
                aria-labelledby="myModalLabel"
                aria-hidden="true" style="display: none;">
                @include('backend.partials.career.list.edit')
            </div>

        </tr>
        @endforeach

    </tbody>
</table>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">

    $(function () {
      $("#datatable1").DataTable();

      $( "#sortable" ).sortable({
        items: "tr",
        cursor: 'move',
        opacity: 0.6,
        update: function() {
            sendOrderToServer();
        }
    });

      function sendOrderToServer() {
        var carer = [];
        $('tr.row1').each(function(index,element) {
          carer.push({
            id: $(this).attr('data-id'),
            position: index+1
        });
      });
        $.ajax({
          type: "POST", 
          dataType: "json", 
          url: "{{ route('career.sort') }}",
          data: {
            carer:carer,
            _token: '{{csrf_token()}}'
        },
        success: function(response) {
          if (response.status == "success") {
            swal({
              text: response.value,
              icon: "success",
          });
            // console.log(response);
        } else {
            swal({
              text: response.value,
              icon: "error",
          });
            // console.log(response);
        }
    }
});

    }
});

</script>
@endsection
