    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Edit career</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{route('career.update',['career'=>$career['id']])}}" enctype="multipart/form-data">
                    {{method_field('PUT')}}
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="job_title">career Title</label>
                        <input type="text" value="{{$career['job_title']}}" name="job_title" class="form-control" placeholder="Enter career Title" required>
                    </div>
                      <div class="form-group">
                    <label for="candidate">Candidate Required</label>
                    <input type="number" value="{{$career['candidate']}}" name="candidate" class="form-control" id="candidate" placeholder="Enter Candidates Required" required>
                </div>
                    <div class="form-group">
                        <label for="job_description">career Description</label>
                        <textarea name="job_description" class="form-control" cols="5" rows="10">{{$career['job_description']}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="job_responsibilities">Career Responsibilities</label>
                        <textarea name="job_responsibilities" class="form-control" cols="5" rows="10">{{$career['job_responsibilities']}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="start_date">Start Date</label>
                        <label style="float: right;" for="start_date">End Date</label>
                        <div class="input-group">
                            <input type="date" class="form-control" value="{{$career['start_date']}}" name="start_date">
                            <span class="input-group-addon">to</span>
                            <input type="date" class="form-control" value="{{$career['end_date']}}" name="end_date">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
