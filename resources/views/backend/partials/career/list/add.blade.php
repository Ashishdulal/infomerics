<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
style="display: none;">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4>Add Career Opportunity</h4>
        </div>
        <div class="modal-body">
            <form method="POST" action="{{route('career.store')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="job_title">Career Title</label>
                    <input type="text" value="{{old('job_title')}}" name="job_title" class="form-control" id="job_title"
                    placeholder="Enter career Title" required>
                </div>
                <div class="form-group">
                    <label for="candidate">Candidates Required</label>
                    <input type="number" value="{{old('candidate')}}" name="candidate" class="form-control" id="candidate" placeholder="Enter Candidates Required" required>
                </div>
                <div class="form-group">
                    <label for="job_description">career Description</label>
                    <textarea name="job_description" value="{{old('job_description')}}" class="form-control"
                    id="job_description" cols="5" rows="10"></textarea>
                </div>
                <div class="form-group">
                    <label for="job_responsibilities">Career Responsibilities</label>
                    <textarea name="job_responsibilities" value="{{old('job_responsibilities')}}" class="form-control"
                    id="job_responsibilities" cols="5" rows="10"></textarea>
                </div>
                <div class="form-group">
                    <label for="start_date">Start Date</label>
                    <label style="float: right;" for="start_date">End Date</label>
                    <div class="input-group" data-provide="datepicker">
                        <input type="date" class="form-control" value="{{ date('Y-m-d')}}" name="start_date">
                        <span class="input-group-addon">to</span>
                        <input type="date" class="form-control" value="{{ date('Y-m-d')}}" name="end_date">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>
