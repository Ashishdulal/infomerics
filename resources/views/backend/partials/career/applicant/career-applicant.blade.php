@extends('backend.layouts.app')
@section('content')

<div class="banner">
    <h2>
        <a href="/admin/home">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Career Applicants</span>
    </h2>
</div>
<!--//banner-->
<!--faq-->
<div class="blank">


    <div class="blank-page">

        <table class="table" id="datatable">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Full Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Applied Position</th>
                    <th scope="col">Expected Salary</th>
                    <th scope="col">CV</th>
                    <th scope="col">Cover Letter</th>
                    <th scope="col">Status</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($applicants as $applicant)
                <tr>
                    <td scope="row">{{$loop->iteration}}</td>
                    <td>{{$applicant['full_name']}}</td>
                    <td>{{$applicant['email']}}</td>
                    <td>{{$applicant['phone']}}</td>
                    <td>{{$applicant->career->job_title}}</td>
                    <td>{{$applicant['expected_salary']}}</td>
                    <td>
                        <div class="col-md">
                            <div class="gallery-img">
                                <a target="_blank" href="{{asset('/images/career/applicant/'.$applicant['cv_file'])}}"><i class="fa fa-file-pdf-o make-large" aria-hidden="true"></i></a>

                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="col-md">
                            <div class="gallery-img">
                                <a target="_blank" href="{{asset('/images/career/applicant/'.$applicant['cover_letter'])}}"><i class="fa fa-file-pdf-o make-large" aria-hidden="true"></i></a>

                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <select onchange="sendStatusToServer({{$applicant->id}})" id="statusValue" name="career_status" class="form-control">
                                <option @if($applicant['status'] == 1) selected @endif disabled value="1">Requested</option>
                                <option @if($applicant['status'] == 2) selected @endif value="2">Seen</option>
                                <option @if($applicant['status'] == 3) selected @endif value="3">Approved</option>
                                <option @if($applicant['status'] == 4) selected @endif value="4">Rejected</option>
                            </select>
                        </div>
                    </td>
                    <td>
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                        data-target="#careerEdit{{$applicant->id}}">
                        <i class="fa fa-eye"></i>
                    </button>
                </td>
                <div class="modal fade" style="margin-left: 125px" id="careerEdit{{$applicant->id}}"
                   tabindex="-1"
                   role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                   <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="modal-title" id="exampleModalLongTitle">Applicant Details</h5>
                    </div>
                    <div class="mail-body container-fluid ">
                        <div class="mail-content ">
                            <div class="mail-heading text-uppercase p-4 mb-4 bg-light text-center">
                                <br>
                                <h3>{{$applicant['full_name']}}</h3>
                                <hr>
                            </div>
                            <form>
                              <div class="form-row">
                                <div class="form-group col-md-6">
                                  <label >Email</label>
                                  <input type="text" class="form-control" value="{{$applicant['email']}}"  placeholder="Email" disabled>
                              </div>
                              <div class="form-group col-md-6">
                                  <label >Phone</label>
                                  <input type="text" class="form-control" value="{{$applicant['phone']}}" placeholder="Password" disabled>
                              </div>
                          </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                              <label >Designation</label>
                              <input type="text" class="form-control" value="{{$applicant->career->job_title}}" disabled>
                          </div>
                            <div class="form-group col-md-6">
                              <label >Expected Salary</label>
                              <input type="text" value="Rs. {{$applicant['expected_salary']}}" class="form-control" disabled>
                          </div>
                  </div>
                   <div class="form-row">
                            <div class="form-group col-md-6">
                              <label >CV</label>
                              <div class="gallery-img border">
                                &nbsp; <br><a target="_blank" href="{{asset('/images/career/applicant/'.$applicant['cv_file'])}}"><i class="fa fa-file-pdf-o make-large" aria-hidden="true"></i></a>
                            </div>
                            <br>
                          </div>
                            <div class="form-group col-md-6">
                              <label >Cover Letter</label>
                             <div class="gallery-img border">
                                &nbsp; <br><a target="_blank" href="{{asset('/images/career/applicant/'.$applicant['cover_letter'])}}"><i class="fa fa-file-pdf-o make-large" aria-hidden="true"></i></a>
                            </div>
                            <br>
                          </div>
                  </div>
                   <div class="form-row text-center">
                    <div class="form-group">
                    <label>Status :</label>
                    {!!$applicant->statusPrint($applicant->status)!!}
                  </div>
                   </div>
        </form>
    </div>
</div>
<div class="modal-footer">
</div>
</div>
</div>
</div>
</tr>
@endforeach

</tbody>
</table>
</div>
</div>

@endsection
@section('script')
<script type="text/javascript">
  function sendStatusToServer(id) {
    var p_id = id;
    var status_value = document.getElementById("statusValue").value;
    $.ajax({
      type: "post", 
      dataType: "json", 
      url: "{{ route('career-applicant.status.change') }}",
      data: {
        id:p_id,status:status_value,
        _token: '{{csrf_token()}}'
    },
    success: function(response) {
        console.log(response);
        console.log("response");
        if (response.status == "success") {
            swal({
              text: response.value,
              icon: "success",
          });
        } else {
            swal({
              text: response.value,
              icon: "error",
          });
            // console.log(response);
        }
    }
});

}
</script>
@endsection