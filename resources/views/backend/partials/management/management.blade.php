@extends('backend.layouts.app')
@section('content')
    <div class="banner">
        <h2>
            <a href="/admin/home">Home</a>
            <i class="fa fa-angle-right"></i>
            <span>Team</span>
        </h2>
    </div>
    <!--//banner-->
    <!--faq-->
    <div class="blank">


        <div class="blank-page">

            <div class="grid_3 grid_5">
                <div class="bs-example2 bs-example-padded-bottom">
                    <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#addModal">
                        Add Mangement Team
                    </button>
                </div>
                @include('backend.partials.management.add')
            </div>
            <table class="table" id="datatable">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Image</th>
                    <!-- <th scope="col">Category</th> -->
                    <th scope="col">Name</th>
                    <th scope="col">Designation</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                    @if(isset($managements))
                @foreach($managements as $management)
                    <tr>

                        <th scope="row">{{$loop->iteration}}</th>
                        <td>
                            <div class="col-md">
                                <div class="gallery-img">
                                    <a href="{{asset('images/management/'.$management['image'])}}"
                                       class="b-link-stripe b-animate-go  swipebox" title="Image Title">
                                        <img class="img-responsive" src="{{asset('images/management/'.$management['image'])}}" alt="">
                                        <span class="zoom-icon"> </span> </a>

                                </div>
                            </div>
                        </td>
                        <!-- <td>{{$management['category']}}</td> -->
                        <td>{{$management['name']}}</td>
                        <td>{{$management['designation']}}</td>
                        <td><button data-toggle="modal" data-target="#editModal{{$management['id']}}">
                                <i class="fa fa-pencil-square text-primary fa-2x"></i>
                            </button>&ensp;&ensp;
                            <form action="{{ route('management.destroy', ['management'=>$management['id']]) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button onclick="return confirm('Are you sure you want to delete?')">  <i class="fa fa-trash text-danger fa-2x"></i></button>
                            </form>
                           </td>
                        <div class="modal fade" id="editModal{{$management['id']}}" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel"
                             aria-hidden="true" style="display: none;">
                            @include('backend.partials.management.edit')
                        </div>

                    </tr>
                @endforeach
                @endif

                </tbody>
            </table>
        </div>
    </div>

@endsection