    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Edit Events And News</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{route('management.update',['management'=>$management['id']])}}" enctype="multipart/form-data">
                    {{method_field('patch')}}
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="exampleInputFile">Select Image</label>
                        <input type="file" name="image" id="exampleInputFile">
                        <p class="help-block">File Must Be In Image Format. Insert Image of 3 X 2</p>
                    </div>
<!--                     <div class="form-group">
                        <div class="form-group">
                            <label for="category">Select Category</label>
                            <select class="form-control" name="category">
                                <option value="{{$management['category']!="bod"?"bod":"community"}}"  selected>{{$management['category']=="bod"?"Board Of Directors (BOD) Team":"Community Team"}}</option>
                                <option value="bod" @if($management['bod']=="bod") selected @endif>Board Of Directors (BOD) Team</option>
                                <option value="community" @if($management['category']=="community") selected @endif>Community Team</option>
                            </select>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label for="title">name</label>
                        <input type="text" name="name" value="{{$management['name']}}" class="form-control" id="title" placeholder="Enter Title">
                    </div>
                    <div class="form-group">
                        <label for="title">Designation</label>
                        <input type="text" value="{{$management['designation']}}" name="designation" class="form-control" placeholder="Enter Designation" required>
                    </div>
                    <div class="form-group">
                        <label for="description">Bio</label>
                        <textarea name="bio" class="form-control" id="description" cols="5" rows="10">{{$management['bio']}}</textarea>
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
