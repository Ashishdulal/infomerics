<!--scrolling js-->
<script src="{{asset('backend/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('backend/js/scripts.js')}}"></script>
<!--//scrolling js-->
<script src="{{asset('backend/js/bootstrap.min.js')}}"></script>
@yield('script')
</body>
</html>
