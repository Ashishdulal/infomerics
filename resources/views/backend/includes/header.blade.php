<!DOCTYPE HTML>
<html>
<head>
    <?php 
    $webinfo = App\Model\WebInfo::first();
    ?>
    <title>{{$webinfo ? $webinfo->web_title : ''}}-Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="{{asset('backend/css/bootstrap.min.css')}}" rel='stylesheet' type='text/css'/>
    <!-- Custom Theme files -->
    <link href="{{asset('backend/css/style.css')}}" rel='stylesheet' type='text/css'/>
    <link href="{{asset('backend/css/font-awesome.css')}}" rel="stylesheet">
    <script src="{{asset('backend/js/jquery.min.js')}}"></script>
    
    <!-- Mainly scripts -->
    <script type="text/javascript" src="{{asset('backend/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('backend/js/jquery.metisMenu.js')}}"></script>
    <script src="{{asset('backend/js/jquery.slimscroll.min.js')}}"></script>
    <!-- Custom and plugin javascript -->
    <link href="{{asset('backend/css/custom.css')}}" rel="stylesheet">
    <script src="{{asset('backend/js/custom.js')}}"></script>
    <script src="{{asset('backend/js/screenfull.js')}}"></script>
    <script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.9/tinymce.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('datatable/datatables.min.css')}}"/>
    <script type="text/javascript" src="{{asset('datatable/datatables.min.js')}}"></script>
    @yield('css')

    <script>
        $(document).ready( function () {
            tinymce.init({selector:'textarea'});
            $('#datatable').DataTable();

        } );
        $(function () {
            $('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

            if (!screenfull.enabled) {
                return false;
            }


            $('#toggle').click(function () {
                screenfull.toggle($('#container')[0]);
            });


        });
    </script>

    <!----->

    <!--pie-chart--->
    <script src="{{asset('backend/js/pie-chart.js')}}" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#demo-pie-1').pieChart({
                barColor: '#3bb2d0',
                trackColor: '#eee',
                lineCap: 'round',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

            $('#demo-pie-2').pieChart({
                barColor: '#fbb03b',
                trackColor: '#eee',
                lineCap: 'butt',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

            $('#demo-pie-3').pieChart({
                barColor: '#ed6498',
                trackColor: '#eee',
                lineCap: 'square',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });


        });

    </script>
    <!--skycons-icons-->
    <script src="{{asset('backend/js/skycons.js')}}"></script>
<script src="{{asset('backend/js/sweetalert.min.js')}}"></script>
    <!--//skycons-icons-->
</head>
