<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="/admin/home" class=" hvr-bounce-to-right"><i
                    class="fa fa-dashboard nav_icon "></i><span class="nav-label">Dashboards</span>
                </a>
            </li>
            <li>
                <a href="{{route('slider.index')}}" class=" hvr-bounce-to-right"><i
                    class="fa fa-image nav_icon "></i><span class="nav-label">Slider</span>
                </a>
            </li>
            <li>
                <a href="{{route('rating.index')}}" class=" hvr-bounce-to-right"><i
                    class="fa fa-star nav_icon "></i><span class="nav-label">Ratings</span>
                </a>
            </li>
            <li>
                <a href="{{route('webinfo.index')}}" class=" hvr-bounce-to-right"><i
                    class="fa fa-newspaper-o nav_icon "></i><span class="nav-label">Web Infos</span>
                </a>
            </li>
            <li>
                <a href="{{route('service.index')}}" class=" hvr-bounce-to-right"><i
                    class="fa fa-list-alt nav_icon "></i><span class="nav-label">Services</span>
                </a>
            </li>
            <li>
                <a href="{{route('career.index')}}" class=" hvr-bounce-to-right"><i
                    class="fa fa-newspaper-o nav_icon "></i><span class="nav-label">Career Listing</span>
                </a>
            </li>
            <li>
                <a href="{{route('career-applicant.index')}}" class=" hvr-bounce-to-right"><i
                    class="fa fa-newspaper-o nav_icon "></i><span class="nav-label">Career Applicants</span>
                </a>
            </li>
            <li>
                <a href="{{route('management.index')}}" class=" hvr-bounce-to-right"><i class="fa fa-users nav_icon"></i> <span
                    class="nav-label">Management Team</span> </a>
                </li>

               <!--  <li>
                    <a href="{{route('expert.index')}}" class=" hvr-bounce-to-right"><i class="fa fa-user-plus nav_icon"></i> <span
                        class="nav-label">Expert Level Team</span> </a>
                    </li> -->

                    <!-- <li>
                        <a href="{{route('institutional.index')}}" class=" hvr-bounce-to-right"><i class="fa fa-institution nav_icon"></i> <span
                            class="nav-label">Institutional Member</span> </a>
                        </li> -->
                        <!-- <li>
                            <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span
                                class="nav-label">Expert Blogs</span><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="{{route('category.index')}}" class=" hvr-bounce-to-right"><i class="fa fa-sitemap nav_icon"></i> <span
                                            class="nav-label">Category</span> </a>
                                        </li>

                                        <li>
                                            <a href="{{route('blog.index')}}" class=" hvr-bounce-to-right"><i class="fa fa-paper-plane nav_icon"></i> <span
                                                class="nav-label">Blogs</span> </a>
                                            </li>

                                        </ul>
                                    </li> -->
                                   <!--  <li>
                                        <a href="{{route('testimonial.index')}}" class=" hvr-bounce-to-right"><i class="fa fa-tasks nav_icon"></i> <span
                                            class="nav-label">Testimonials</span> </a>
                                        </li> -->
                                        <li>
                                            <a href="{{route('press-release.index')}}" class=" hvr-bounce-to-right"><i class="fa fa-newspaper-o nav_icon"></i> <span
                                                class="nav-label">Press Release</span> </a>
                                            </li>
                                            <li>
                                                <a href="{{route('contacts.index')}}" class=" hvr-bounce-to-right"><i class="fa fa-phone nav_icon"></i> <span
                                                    class="nav-label">Contact Us</span> </a>
                                                </li>
                                                <li>
                                                    <a href="{{route('setting.change')}}" class=" hvr-bounce-to-right"><i
                                                        class="fa fa-cog nav_icon "></i><span class="nav-label">Settings</span>
                                                    </a>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
