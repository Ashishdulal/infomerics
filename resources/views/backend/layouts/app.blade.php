@include("backend.includes.header")
<div id="wrapper">
@include("backend.includes.navigation")
<!----->
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="content-main">
        @yield('content')
        </div>
        <div class="clearfix"></div>
    </div>
</div>
@include("backend.includes.footer")
