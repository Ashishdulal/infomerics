<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// redirect from register page to home page
Route::get('/admin', function () {
    return redirect('/admin/login');
});

Route::get('/run-migrate', function () {
    Artisan::call('migrate');
    return "Successfully migrated";

});
Route::get('/config-cache', function () {
    Artisan::command('logs:clear', function() {

        exec('rm ' . storage_path('logs/*.log'));

        $this->comment('Logs have been cleared!');

    })->describe('Clear log files');
    return "Successfully cleared logs";

});
Artisan::command('logs:clear', function() {

    exec('rm ' . storage_path('logs/*.log'));

    $this->comment('Logs have been cleared!');

})->describe('Clear log files');

Route::get('/clear-cache', function () {

    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cache is cleared";
});
Route::get('/storage-link', function () {

    Artisan::call('storage:link');
    return "Storage is linked";
});


Route::get('/comming-soon', 'frontend\FrontendController@commingSoon')->name('frontend.comming.soon');
Route::get('/', 'frontend\FrontendController@index')->name('frontend.home');

/*about*/
Route::get('/about', 'frontend\FrontendController@about')->name('frontend.about');

/*services*/
Route::get('/services', 'frontend\FrontendController@service')->name('frontend.service');
Route::get('/service-detail/{id}', 'frontend\FrontendController@serviceDetail')->name('frontend.service-detail');

/*Rating*/
Route::get('/ratings', 'frontend\FrontendController@ratings')->name('frontend.ratings');
Route::get('/ratings/search', 'frontend\FrontendController@ratingsSearch')->name('frontend.ratingsSearch');
Route::get('/rating-details/{id}', 'frontend\FrontendController@ratingsDetails')->name('frontend.ratingsDetail');

/*Press Release*/
Route::get('/press-release', 'frontend\FrontendController@pressRelease')->name('frontend.pressRelease');
Route::get('/press-release-detail/{id}', 'frontend\FrontendController@pressReleaseDetail')->name('frontend.pressReleaseDetail');

/*Career*/
Route::get('/career', 'frontend\FrontendController@careers')->name('frontend.career');
Route::get('/career-details/{id}', 'frontend\FrontendController@careerDetail')->name('frontend.career.detail');
Route::get('/career-apply', 'frontend\FrontendController@careerApply')->name('frontend.career.apply');
Route::post('/career-apply', 'Backend\CareerAppliesController@store')->name('frontend.career.apply.new');

/*Contact*/
Route::get('/contact', 'frontend\FrontendController@contact')->name('frontend.contact');
Route::post('/contact', 'frontend\FrontendController@contactUs')->name('contact.us');


/*Fallback*/
Route::fallback(function() {
    return view('nopage');
});