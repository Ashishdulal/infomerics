<?php

Auth::routes();

Route::get('home', 'HomeController@index')->name('home');
Route::resource('image', 'Backend\ImageController');
Route::resource('video', 'Backend\VideoController');
Route::resource('press-release', 'Backend\EventController');
Route::resource('contacts', 'Backend\ContactController');
Route::resource('management', 'Backend\ManagementController');
Route::resource('expert', 'Backend\ExpertController');
Route::resource('institutional', 'Backend\InstitutionalController');
Route::resource('category', 'Backend\CategoryController');
Route::resource('blog', 'Backend\BlogController');
Route::resource('testimonial', 'Backend\TestimonialController');
Route::resource('slider', 'Backend\SliderController');
Route::resource('rating', 'Backend\RatingController');
Route::get('/rating/status/{id}','Backend\RatingController@statusChange')->name('rating.status.change');
Route::resource('service', 'Backend\ServiceController');
Route::resource('webinfo', 'Backend\WebInfoController');
Route::get('/career/info','Backend\WebInfoController@career')->name('webinfo.career');
Route::resource('career', 'Backend\CareerController');
Route::resource('career-applicant', 'Backend\CareerAppliesController');
Route::post('/career-applicant/status/','Backend\CareerAppliesController@statusUpdate')->name('career-applicant.status.change');
Route::post('/career/sort','Backend\CareerController@sort')->name('career.sort');
Route::get('/career/status/{id}','Backend\CareerController@statusChange')->name('career.status.change');
Route::get('/settings','Backend\WebInfoController@setting')->name('setting.change');