-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2021 at 05:43 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `infomerics`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `author_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `category_id`, `author_name`, `title`, `description`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Whoopi Bean', 'Arthur Elliott', '<p>sdfghjkl;\'</p>', '1610866300.jpg', 1, '2021-01-17 01:06:40', '2021-01-17 01:06:40');

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE `careers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `job_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `candidate` int(11) NOT NULL DEFAULT 1,
  `job_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_responsibilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careers`
--

INSERT INTO `careers` (`id`, `job_title`, `candidate`, `job_description`, `job_responsibilities`, `start_date`, `end_date`, `position`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Developer', 4, '<p style=\"text-align: justify;\"><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; background-color: #ffffff; color: #000000;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</span></p>', '<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify; background-color: #ffffff; color: #000000;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</span></p>', '2021-01-21', '2021-01-31', 4, 1, '2021-01-20 23:12:30', '2021-01-27 03:30:53'),
(2, 'Designer', 3, '<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; text-align: justify; background-color: #ffffff; color: #000000;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</span></p>', '<p style=\"text-align: justify;\"><span style=\"font-family: \'Open Sans\', Arial, sans-serif; font-size: 14px; background-color: #ffffff; color: #000000;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</span></p>', '2021-01-21', '2021-01-27', 2, 1, '2021-01-20 23:12:57', '2021-01-27 03:42:44'),
(3, 'social media', 2, '<ul class=\"ps-4 mb-0\" style=\"box-sizing: border-box; margin-top: 0px; color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; padding-left: 1.5rem !important; margin-bottom: 0px !important;\">\r\n<li style=\"box-sizing: border-box;\">Laborum id odio quo nesciunt, excepturi. Praesentium, nulla.</li>\r\n<li style=\"box-sizing: border-box;\">Odio magnam aperiam tempora, sequi pariatur debitis cumque.</li>\r\n<li style=\"box-sizing: border-box;\">Quisquam, qui? Dolorem repellendus, eaque excepturi illo quos.</li>\r\n<li style=\"box-sizing: border-box;\">Enim exercitationem labore, laboriosam repellat dicta. Non, minus.</li>\r\n<li style=\"box-sizing: border-box;\">Quod vitae, laboriosam. Debitis possimus facere quia hic?</li>\r\n<li style=\"box-sizing: border-box;\">Ea ipsam earum quod quasi aperiam sint natus.</li>\r\n<li style=\"box-sizing: border-box;\">Vel repellendus voluptatem, modi suscipit incidunt, cumque ullam!</li>\r\n<li style=\"box-sizing: border-box;\">Ut excepturi dolorem et illum tenetur fugiat rerum!</li>\r\n<li style=\"box-sizing: border-box;\">Soluta maiores, magni repudiandae sit hic earum perferendis.</li>\r\n<li style=\"box-sizing: border-box;\">Dignissimos ab mollitia, officia odio autem ipsa iure?</li>\r\n</ul>', '<ul class=\"ps-4 mb-0\" style=\"box-sizing: border-box; margin-top: 0px; color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; padding-left: 1.5rem !important; margin-bottom: 0px !important;\">\r\n<li style=\"box-sizing: border-box;\">Autem, odio, vitae. Aut iusto sed quia quam.</li>\r\n<li style=\"box-sizing: border-box;\">Autem ratione voluptatibus optio suscipit, cupiditate nesciunt esse.</li>\r\n<li style=\"box-sizing: border-box;\">Necessitatibus nobis nisi odit, iure, sed harum adipisci!</li>\r\n<li style=\"box-sizing: border-box;\">Quaerat facilis nihil aliquid fuga reprehenderit odio! At.</li>\r\n<li style=\"box-sizing: border-box;\">Quod nemo labore voluptates nisi. Inventore, repellat, ipsum.</li>\r\n<li style=\"box-sizing: border-box;\">Quisquam, quod. Ea corporis vel hic. Hic, illum.</li>\r\n<li style=\"box-sizing: border-box;\">Ipsa veritatis laborum quibusdam saepe, fugit molestias, porro?</li>\r\n<li style=\"box-sizing: border-box;\">Ullam beatae enim ipsam illo optio reiciendis aperiam.</li>\r\n<li style=\"box-sizing: border-box;\">Dolorem quasi harum aut tenetur cumque praesentium minima?</li>\r\n<li style=\"box-sizing: border-box;\">Consequatur ipsum, velit. Amet cumque est alias dolorum.</li>\r\n</ul>', '2021-01-24', '2021-08-31', 3, 1, '2021-01-24 03:23:58', '2021-01-27 03:48:47'),
(4, 'Social Media', 1, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2021-01-26', '2021-01-25', 1, 1, '2021-01-24 04:25:22', '2021-01-27 03:45:53'),
(5, 'Marketting', 1, '<p>asdf</p>', '<p>hello</p>', '1984-02-07', '1976-05-30', 5, 1, '2021-01-24 04:25:44', '2021-01-27 03:02:15'),
(8, 'Sales', 1, '<p>xsxsxsxsxsxsxsxs</p>', '<p>sxsxsxsxsxsxsx</p>', '2021-01-24', '2021-01-24', 6, 1, '2021-01-24 04:27:42', '2021-01-27 03:02:26');

-- --------------------------------------------------------

--
-- Table structure for table `career_applies`
--

CREATE TABLE `career_applies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `career_id` int(11) NOT NULL,
  `expected_salary` int(11) NOT NULL,
  `cv_file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover_letter` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `additional_message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `career_applies`
--

INSERT INTO `career_applies` (`id`, `full_name`, `email`, `phone`, `career_id`, `expected_salary`, `cv_file`, `cover_letter`, `additional_message`, `status`, `created_at`, `updated_at`) VALUES
(1, 'ashish dulal', 'aashish.analogue51@gmail.com', '0987654321', 1, 20000, 'asdf.pdf', 'fdsa.pdf', 'sdfv', 3, '2021-01-25 06:38:06', '2021-01-25 05:40:39'),
(2, 'Isadora Hunt', 'fupis@mailinator.com', '8079773098', 1, 20000, 'applicant1611741816.png', 'applicant1611741816.png', NULL, 1, '2021-01-27 04:18:36', '2021-01-27 04:18:36'),
(3, 'Quentin Rosales', 'tybogy@mailinator.com', '0987654321', 1, 77000, 'applicant1611742230.jpg', 'applicant1611742230.jpg', NULL, 1, '2021-01-27 04:25:30', '2021-01-27 04:25:30'),
(4, 'Ashish Dulal', 'azizdulal.ad@gmail.com', '9843083493', 1, 47000, 'applicant1611745350.jpg', 'applicant1611745350.jpg', NULL, 2, '2021-01-27 05:17:30', '2021-01-28 05:28:39'),
(5, 'Uma Ramirez', 'pawoluqi@mailinator.com', '0919192029', 1, 24, 'applicant1611903516.png', 'applicant1611903516.png', NULL, 1, '2021-01-29 01:13:36', '2021-01-29 01:13:36'),
(6, 'Uma Ramirez', 'pawoluqi@mailinator.com', '0919192029', 1, 24, 'applicant1611903525.png', 'applicant1611903525.png', NULL, 1, '2021-01-29 01:13:45', '2021-01-29 01:13:45'),
(7, 'Uma Ramirez', 'pawoluqi@mailinator.com', '0919192029', 1, 24, 'applicant1611903549.png', 'applicant1611903549.png', NULL, 1, '2021-01-29 01:14:09', '2021-01-29 01:14:09'),
(8, 'Uma Ramirez', 'pawoluqi@mailinator.com', '0919192029', 1, 24, 'applicant1611903577.png', 'applicant1611903577.png', NULL, 1, '2021-01-29 01:14:37', '2021-01-29 01:14:37'),
(9, 'Uma Ramirez', 'pawoluqi@mailinator.com', '0919192029', 1, 24, 'applicant1611903608.png', 'applicant1611903608.png', NULL, 1, '2021-01-29 01:15:08', '2021-01-29 01:15:08'),
(10, 'Uma Ramirez', 'pawoluqi@mailinator.com', '0919192029', 1, 24, 'applicant1611903667.png', 'applicant1611903667.png', NULL, 1, '2021-01-29 01:16:07', '2021-01-29 01:16:07'),
(11, 'Uma Ramirez', 'pawoluqi@mailinator.com', '0919192029', 1, 24, 'applicant1611903679.png', 'applicant1611903679.png', NULL, 1, '2021-01-29 01:16:19', '2021-01-29 01:16:19'),
(12, 'Uma Ramirez', 'pawoluqi@mailinator.com', '0919192029', 1, 24, 'applicant1611903691.png', 'applicant1611903691.png', NULL, 1, '2021-01-29 01:16:31', '2021-01-29 01:16:31'),
(13, 'Uma Ramirez', 'pawoluqi@mailinator.com', '0919192029', 1, 24, 'applicant1611903706.png', 'applicant1611903706.png', NULL, 1, '2021-01-29 01:16:46', '2021-01-29 01:16:46'),
(14, 'Uma Ramirez', 'pawoluqi@mailinator.com', '0919192029', 1, 24, 'applicant1611903734.png', 'applicant1611903734.png', NULL, 1, '2021-01-29 01:17:14', '2021-01-29 01:17:14'),
(15, 'Uma Ramirez', 'pawoluqi@mailinator.com', '0919192029', 1, 24, 'applicant1611903982.png', 'applicant1611903982.png', NULL, 1, '2021-01-29 01:21:22', '2021-01-29 01:21:22'),
(16, 'Uma Ramirez', 'pawoluqi@mailinator.com', '0919192029', 1, 24, 'applicant1611904009.png', 'applicant1611904009.png', NULL, 1, '2021-01-29 01:21:49', '2021-01-29 01:21:49'),
(17, 'Uma Ramirez', 'pawoluqi@mailinator.com', '0919192029', 1, 24, 'applicant1611904175.png', 'applicant1611904175.png', NULL, 1, '2021-01-29 01:24:35', '2021-01-29 01:24:35'),
(18, 'Uma Ramirez', 'pawoluqi@mailinator.com', '0919192029', 1, 24, 'applicant1611904217.png', 'applicant1611904217.png', NULL, 1, '2021-01-29 01:25:17', '2021-01-29 01:25:17'),
(19, 'Uma Ramirez', 'pawoluqi@mailinator.com', '0919192029', 1, 24, 'applicant1611904225.png', 'applicant1611904225.png', NULL, 1, '2021-01-29 01:25:25', '2021-01-29 01:25:25'),
(20, 'Uma Ramirez', 'pawoluqi@mailinator.com', '0919192029', 1, 24, 'applicant1611904255.png', 'applicant1611904255.png', NULL, 1, '2021-01-29 01:25:55', '2021-01-29 01:25:55'),
(21, 'Uma Ramirez', 'pawoluqi@mailinator.com', '0919192029', 1, 24, 'applicant1611904387.png', 'applicant1611904387.png', NULL, 1, '2021-01-29 01:28:07', '2021-01-29 01:28:07'),
(22, 'Uma Ramirez', 'pawoluqi@mailinator.com', '0919192029', 1, 24, 'applicant1611904433.png', 'applicant1611904433.png', NULL, 1, '2021-01-29 01:28:53', '2021-01-29 01:28:53'),
(23, 'Uma Ramirez', 'pawoluqi@mailinator.com', '0919192029', 1, 24, 'applicant1611904500.png', 'applicant1611904500.png', NULL, 1, '2021-01-29 01:30:00', '2021-01-29 01:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `status`, `created_at`, `updated_at`) VALUES
(1, 'tech', 1, '2021-01-17 01:06:09', '2021-01-17 01:06:09');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `topic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_sector` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `phone`, `topic`, `business_name`, `business_sector`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Dakota Decker', 'zicoropyni@mailinator.com', '+1 (986) 384-9835', 'Dolor aut fugit ass', 'Phillip Hendricks', 'Hic reprehenderit u', 'Beatae consequat It', '2021-01-27 06:09:42', '2021-01-27 06:09:42'),
(2, 'Dakota Decker', 'zicoropyni@mailinator.com', '+1 (986) 384-9835', 'Dolor aut fugit ass', 'Phillip Hendricks', 'Hic reprehenderit u', 'Beatae consequat It', '2021-01-27 06:10:17', '2021-01-27 06:10:17'),
(3, 'Leilani Oliver', 'zowu@mailinator.com', '+1 (293) 964-9786', 'Enim modi adipisci m', 'Whoopi Simmons', 'Ipsum debitis distin', 'Esse voluptas praes', '2021-01-29 00:56:55', '2021-01-29 00:56:55'),
(4, 'Samantha Burgess', 'wozatisuta@mailinator.com', '+1 (412) 129-6036', 'Qui est repellendus', 'Bree Little', 'Itaque perferendis a', 'Quia velit animi qu', '2021-01-29 00:57:31', '2021-01-29 00:57:31'),
(5, 'Samantha Burgess', 'wozatisuta@mailinator.com', '+1 (412) 129-6036', 'Qui est repellendus', 'Bree Little', 'Itaque perferendis a', 'Quia velit animi qu', '2021-01-29 00:59:53', '2021-01-29 00:59:53'),
(6, 'Samantha Burgess', 'wozatisuta@mailinator.com', '+1 (412) 129-6036', 'Qui est repellendus', 'Bree Little', 'Itaque perferendis a', 'Quia velit animi qu', '2021-01-29 01:00:16', '2021-01-29 01:00:16'),
(7, 'Samantha Burgess', 'wozatisuta@mailinator.com', '+1 (412) 129-6036', 'Qui est repellendus', 'Bree Little', 'Itaque perferendis a', 'Quia velit animi qu', '2021-01-29 01:02:33', '2021-01-29 01:02:33'),
(8, 'Samantha Burgess', 'wozatisuta@mailinator.com', '+1 (412) 129-6036', 'Qui est repellendus', 'Bree Little', 'Itaque perferendis a', 'Quia velit animi qu', '2021-01-29 01:03:05', '2021-01-29 01:03:05'),
(9, 'Samantha Burgess', 'wozatisuta@mailinator.com', '+1 (412) 129-6036', 'Qui est repellendus', 'Bree Little', 'Itaque perferendis a', 'Quia velit animi qu', '2021-01-29 01:03:24', '2021-01-29 01:03:24'),
(10, 'Samantha Burgess', 'wozatisuta@mailinator.com', '+1 (412) 129-6036', 'Qui est repellendus', 'Bree Little', 'Itaque perferendis a', 'Quia velit animi qu', '2021-01-29 01:03:25', '2021-01-29 01:03:25'),
(11, 'Clare Watts', 'cefawequ@mailinator.com', '+1 (329) 997-6809', 'Sint sed sit id est', 'Shelby Owen', 'Voluptatem laboriosa', 'Dolor modi dolorem p', '2021-01-29 01:03:38', '2021-01-29 01:03:38'),
(12, 'Clare Watts', 'cefawequ@mailinator.com', '+1 (329) 997-6809', 'Sint sed sit id est', 'Shelby Owen', 'Voluptatem laboriosa', 'Dolor modi dolorem p', '2021-01-29 01:04:20', '2021-01-29 01:04:20'),
(13, 'Clare Watts', 'cefawequ@mailinator.com', '+1 (329) 997-6809', 'Sint sed sit id est', 'Shelby Owen', 'Voluptatem laboriosa', 'Dolor modi dolorem p', '2021-01-29 01:04:32', '2021-01-29 01:04:32'),
(14, 'Clare Watts', 'cefawequ@mailinator.com', '+1 (329) 997-6809', 'Sint sed sit id est', 'Shelby Owen', 'Voluptatem laboriosa', 'Dolor modi dolorem p', '2021-01-29 01:04:44', '2021-01-29 01:04:44'),
(15, 'Clare Watts', 'cefawequ@mailinator.com', '+1 (329) 997-6809', 'Sint sed sit id est', 'Shelby Owen', 'Voluptatem laboriosa', 'Dolor modi dolorem p', '2021-01-29 01:05:02', '2021-01-29 01:05:02'),
(16, 'Clare Watts', 'cefawequ@mailinator.com', '+1 (329) 997-6809', 'Sint sed sit id est', 'Shelby Owen', 'Voluptatem laboriosa', 'Dolor modi dolorem p', '2021-01-29 01:05:10', '2021-01-29 01:05:10'),
(17, 'Clare Watts', 'cefawequ@mailinator.com', '+1 (329) 997-6809', 'Sint sed sit id est', 'Shelby Owen', 'Voluptatem laboriosa', 'Dolor modi dolorem p', '2021-01-29 01:05:16', '2021-01-29 01:05:16'),
(18, 'Clare Watts', 'cefawequ@mailinator.com', '+1 (329) 997-6809', 'Sint sed sit id est', 'Shelby Owen', 'Voluptatem laboriosa', 'Dolor modi dolorem p', '2021-01-29 01:05:58', '2021-01-29 01:05:58'),
(19, 'Clare Watts', 'cefawequ@mailinator.com', '+1 (329) 997-6809', 'Sint sed sit id est', 'Shelby Owen', 'Voluptatem laboriosa', 'Dolor modi dolorem p', '2021-01-29 01:06:07', '2021-01-29 01:06:07'),
(20, 'Clare Watts', 'cefawequ@mailinator.com', '+1 (329) 997-6809', 'Sint sed sit id est', 'Shelby Owen', 'Voluptatem laboriosa', 'Dolor modi dolorem p', '2021-01-29 01:06:16', '2021-01-29 01:06:16'),
(21, 'Clare Watts', 'cefawequ@mailinator.com', '+1 (329) 997-6809', 'Sint sed sit id est', 'Shelby Owen', 'Voluptatem laboriosa', 'Dolor modi dolorem p', '2021-01-29 01:07:24', '2021-01-29 01:07:24'),
(22, 'Clare Watts', 'cefawequ@mailinator.com', '+1 (329) 997-6809', 'Sint sed sit id est', 'Shelby Owen', 'Voluptatem laboriosa', 'Dolor modi dolorem p', '2021-01-29 01:07:35', '2021-01-29 01:07:35'),
(23, 'Clare Watts', 'cefawequ@mailinator.com', '+1 (329) 997-6809', 'Sint sed sit id est', 'Shelby Owen', 'Voluptatem laboriosa', 'Dolor modi dolorem p', '2021-01-29 01:08:15', '2021-01-29 01:08:15');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `author_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `author_name`, `title`, `description`, `image`, `status`, `created_at`, `updated_at`) VALUES
(2, 'asdfg', 'nisi ut aliquip Ut enim ad minim veniam ullamco laboris', '<p>Possimus quod minus delectus, nostrum iure ad earum nesciunt vero repellendus ducimus quibusdam perspiciatis sapiente repudiandae asperiores, hic! Ut error ullam quas dolore maxime cum vitae, id in ipsum aliquid corporis alias?</p>\r\n<p>&nbsp;</p>\r\n<p>Nam omnis corporis cupiditate est accusantium labore ipsum atque modi nobis molestiae ipsa voluptates quae numquam, quidem, a, temporibus magnam pariatur neque odit veniam iure reprehenderit quos! Delectus dolor exercitationem voluptates consequuntur eligendi amet beatae rem minima dignissimos reprehenderit nam in doloremque, rerum vero obcaecati. Accusamus nobis, atque corporis animi eveniet dicta ad accusantium obcaecati minus magni nostrum nihil temporibus rerum sunt.</p>\r\n<p>&nbsp;</p>\r\n<p>Totam hic voluptate magni, expedita molestiae! Excepturi architecto quos nostrum nobis maiores enim beatae, harum ad ab cum. Tempora delectus facere quia.</p>\r\n<p>&nbsp;</p>\r\n<p>Hic explicabo mollitia dolores omnis saepe blanditiis. A eum labore deserunt rem repellat earum eaque aperiam molestias enim. Modi, aut. Animi voluptas vitae suscipit, quae? Placeat recusandae dolor culpa officiis ipsum laboriosam, porro pariatur reprehenderit, reiciendis, nesciunt veritatis consectetur quam aspernatur numquam.</p>\r\n<p>&nbsp;</p>\r\n<p>Nam omnis corporis cupiditate est accusantium labore ipsum atque modi nobis molestiae ipsa voluptates quae numquam, quidem, a, temporibus magnam pariatur neque odit veniam iure reprehenderit quos! Delectus dolor exercitationem voluptates consequuntur eligendi amet beatae rem minima dignissimos reprehenderit nam in doloremque, rerum vero obcaecati. Accusamus nobis, atque corporis animi eveniet dicta ad accusantium obcaecati minus magni nostrum nihil temporibus rerum sunt.</p>\r\n<p>&nbsp;</p>\r\n<p>Hic explicabo mollitia dolores omnis saepe blanditiis. A eum labore deserunt rem repellat earum eaque aperiam molestias enim. Modi, aut. Animi voluptas vitae suscipit, quae? Placeat recusandae dolor culpa officiis ipsum laboriosam, porro pariatur reprehenderit, reiciendis, nesciunt veritatis consectetur quam aspernatur numquam.</p>\r\n<p>&nbsp;</p>\r\n<p>Possimus quod minus delectus, nostrum iure ad earum nesciunt vero repellendus ducimus quibusdam perspiciatis sapiente repudiandae asperiores, hic! Ut error ullam quas dolore maxime cum vitae, id in ipsum aliquid corporis alias?</p>', '1610961494.Qa-Ashish.pdf', 1, '2021-01-21 03:33:14', '2021-01-27 01:25:09'),
(3, 'hello world', 'Ut enim ad minim veniam ullamco laboris nisi ut aliquip', '<p>Possimus quod minus delectus, nostrum iure ad earum nesciunt vero repellendus ducimus quibusdam perspiciatis sapiente repudiandae asperiores, hic! Ut error ullam quas dolore maxime cum vitae, id in ipsum aliquid corporis alias?</p>\r\n<p>&nbsp;</p>\r\n<p>Nam omnis corporis cupiditate est accusantium labore ipsum atque modi nobis molestiae ipsa voluptates quae numquam, quidem, a, temporibus magnam pariatur neque odit veniam iure reprehenderit quos! Delectus dolor exercitationem voluptates consequuntur eligendi amet beatae rem minima dignissimos reprehenderit nam in doloremque, rerum vero obcaecati. Accusamus nobis, atque corporis animi eveniet dicta ad accusantium obcaecati minus magni nostrum nihil temporibus rerum sunt.</p>\r\n<p>&nbsp;</p>\r\n<p>Totam hic voluptate magni, expedita molestiae! Excepturi architecto quos nostrum nobis maiores enim beatae, harum ad ab cum. Tempora delectus facere quia.</p>\r\n<p>&nbsp;</p>\r\n<p>Hic explicabo mollitia dolores omnis saepe blanditiis. A eum labore deserunt rem repellat earum eaque aperiam molestias enim. Modi, aut. Animi voluptas vitae suscipit, quae? Placeat recusandae dolor culpa officiis ipsum laboriosam, porro pariatur reprehenderit, reiciendis, nesciunt veritatis consectetur quam aspernatur numquam.</p>\r\n<p>&nbsp;</p>\r\n<p>Nam omnis corporis cupiditate est accusantium labore ipsum atque modi nobis molestiae ipsa voluptates quae numquam, quidem, a, temporibus magnam pariatur neque odit veniam iure reprehenderit quos! Delectus dolor exercitationem voluptates consequuntur eligendi amet beatae rem minima dignissimos reprehenderit nam in doloremque, rerum vero obcaecati. Accusamus nobis, atque corporis animi eveniet dicta ad accusantium obcaecati minus magni nostrum nihil temporibus rerum sunt.</p>\r\n<p>&nbsp;</p>\r\n<p>Hic explicabo mollitia dolores omnis saepe blanditiis. A eum labore deserunt rem repellat earum eaque aperiam molestias enim. Modi, aut. Animi voluptas vitae suscipit, quae? Placeat recusandae dolor culpa officiis ipsum laboriosam, porro pariatur reprehenderit, reiciendis, nesciunt veritatis consectetur quam aspernatur numquam.</p>\r\n<p>&nbsp;</p>\r\n<p>Possimus quod minus delectus, nostrum iure ad earum nesciunt vero repellendus ducimus quibusdam perspiciatis sapiente repudiandae asperiores, hic! Ut error ullam quas dolore maxime cum vitae, id in ipsum aliquid corporis alias?</p>', '1610963711.pdf', 1, '2021-01-18 04:09:44', '2021-01-27 01:25:17'),
(4, 'Feruyebde ceuicechece', 'laboris nisi ut aliquip Ut enim ad minim veniam ullamco', '<p>Possimus quod minus delectus, nostrum iure ad earum nesciunt vero repellendus ducimus quibusdam perspiciatis sapiente repudiandae asperiores, hic! Ut error ullam quas dolore maxime cum vitae, id in ipsum aliquid corporis alias?</p>\r\n<p>&nbsp;</p>\r\n<p>Nam omnis corporis cupiditate est accusantium labore ipsum atque modi nobis molestiae ipsa voluptates quae numquam, quidem, a, temporibus magnam pariatur neque odit veniam iure reprehenderit quos! Delectus dolor exercitationem voluptates consequuntur eligendi amet beatae rem minima dignissimos reprehenderit nam in doloremque, rerum vero obcaecati. Accusamus nobis, atque corporis animi eveniet dicta ad accusantium obcaecati minus magni nostrum nihil temporibus rerum sunt.</p>\r\n<p>&nbsp;</p>\r\n<p>Totam hic voluptate magni, expedita molestiae! Excepturi architecto quos nostrum nobis maiores enim beatae, harum ad ab cum. Tempora delectus facere quia.</p>\r\n<p>&nbsp;</p>\r\n<p>Hic explicabo mollitia dolores omnis saepe blanditiis. A eum labore deserunt rem repellat earum eaque aperiam molestias enim. Modi, aut. Animi voluptas vitae suscipit, quae? Placeat recusandae dolor culpa officiis ipsum laboriosam, porro pariatur reprehenderit, reiciendis, nesciunt veritatis consectetur quam aspernatur numquam.</p>\r\n<p>&nbsp;</p>\r\n<p>Nam omnis corporis cupiditate est accusantium labore ipsum atque modi nobis molestiae ipsa voluptates quae numquam, quidem, a, temporibus magnam pariatur neque odit veniam iure reprehenderit quos! Delectus dolor exercitationem voluptates consequuntur eligendi amet beatae rem minima dignissimos reprehenderit nam in doloremque, rerum vero obcaecati. Accusamus nobis, atque corporis animi eveniet dicta ad accusantium obcaecati minus magni nostrum nihil temporibus rerum sunt.</p>\r\n<p>&nbsp;</p>\r\n<p>Hic explicabo mollitia dolores omnis saepe blanditiis. A eum labore deserunt rem repellat earum eaque aperiam molestias enim. Modi, aut. Animi voluptas vitae suscipit, quae? Placeat recusandae dolor culpa officiis ipsum laboriosam, porro pariatur reprehenderit, reiciendis, nesciunt veritatis consectetur quam aspernatur numquam.</p>\r\n<p>&nbsp;</p>\r\n<p>Possimus quod minus delectus, nostrum iure ad earum nesciunt vero repellendus ducimus quibusdam perspiciatis sapiente repudiandae asperiores, hic! Ut error ullam quas dolore maxime cum vitae, id in ipsum aliquid corporis alias?</p>', '1611658296.butwal.pdf', 1, '2021-01-26 05:06:36', '2021-01-27 01:24:44'),
(5, 'asdfg', 'veniam ullamco laboris nisi ut aliquip Ut enim ad minim', '<p>Possimus quod minus delectus, nostrum iure ad earum nesciunt vero repellendus ducimus quibusdam perspiciatis sapiente repudiandae asperiores, hic! Ut error ullam quas dolore maxime cum vitae, id in ipsum aliquid corporis alias?</p>\r\n<p>&nbsp;</p>\r\n<p>Nam omnis corporis cupiditate est accusantium labore ipsum atque modi nobis molestiae ipsa voluptates quae numquam, quidem, a, temporibus magnam pariatur neque odit veniam iure reprehenderit quos! Delectus dolor exercitationem voluptates consequuntur eligendi amet beatae rem minima dignissimos reprehenderit nam in doloremque, rerum vero obcaecati. Accusamus nobis, atque corporis animi eveniet dicta ad accusantium obcaecati minus magni nostrum nihil temporibus rerum sunt.</p>\r\n<p>&nbsp;</p>\r\n<p>Totam hic voluptate magni, expedita molestiae! Excepturi architecto quos nostrum nobis maiores enim beatae, harum ad ab cum. Tempora delectus facere quia.</p>\r\n<p>&nbsp;</p>\r\n<p>Hic explicabo mollitia dolores omnis saepe blanditiis. A eum labore deserunt rem repellat earum eaque aperiam molestias enim. Modi, aut. Animi voluptas vitae suscipit, quae? Placeat recusandae dolor culpa officiis ipsum laboriosam, porro pariatur reprehenderit, reiciendis, nesciunt veritatis consectetur quam aspernatur numquam.</p>\r\n<p>&nbsp;</p>\r\n<p>Nam omnis corporis cupiditate est accusantium labore ipsum atque modi nobis molestiae ipsa voluptates quae numquam, quidem, a, temporibus magnam pariatur neque odit veniam iure reprehenderit quos! Delectus dolor exercitationem voluptates consequuntur eligendi amet beatae rem minima dignissimos reprehenderit nam in doloremque, rerum vero obcaecati. Accusamus nobis, atque corporis animi eveniet dicta ad accusantium obcaecati minus magni nostrum nihil temporibus rerum sunt.</p>\r\n<p>&nbsp;</p>\r\n<p>Hic explicabo mollitia dolores omnis saepe blanditiis. A eum labore deserunt rem repellat earum eaque aperiam molestias enim. Modi, aut. Animi voluptas vitae suscipit, quae? Placeat recusandae dolor culpa officiis ipsum laboriosam, porro pariatur reprehenderit, reiciendis, nesciunt veritatis consectetur quam aspernatur numquam.</p>\r\n<p>&nbsp;</p>\r\n<p>Possimus quod minus delectus, nostrum iure ad earum nesciunt vero repellendus ducimus quibusdam perspiciatis sapiente repudiandae asperiores, hic! Ut error ullam quas dolore maxime cum vitae, id in ipsum aliquid corporis alias?</p>', '1611660971.recipt.pdf', 1, '2021-01-26 05:51:11', '2021-01-27 01:24:34');

-- --------------------------------------------------------

--
-- Table structure for table `experts`
--

CREATE TABLE `experts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bio` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `experts`
--

INSERT INTO `experts` (`id`, `category_id`, `name`, `designation`, `image`, `year`, `bio`, `status`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Wylie Bowman', 'Expedita culpa esse', '1610875334.jpg', '1975', '<p>sdfghjk</p>', 1, '2021-01-17 03:37:14', '2021-01-17 03:37:14');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `institutionals`
--

CREATE TABLE `institutionals` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `proprietor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `involvement` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `institutionals`
--

INSERT INTO `institutionals` (`id`, `name`, `address`, `contact`, `email`, `proprietor`, `position`, `involvement`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Alfonso Kinney', 'Suscipit nulla tempo', 'Ad non voluptatem s', 'hecotati@mailinator.com', 'Impedit unde ut ame', 'Minim distinctio Lo', '<p>asdfgjhgjghjhg.</p>', '1610875364.png', 1, '2021-01-17 03:37:44', '2021-01-17 03:37:44');

-- --------------------------------------------------------

--
-- Table structure for table `managements`
--

CREATE TABLE `managements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bio` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `managements`
--

INSERT INTO `managements` (`id`, `category`, `name`, `designation`, `bio`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Lucius Dodson', 'Iste numquam incidun', '<p>SADSFDGFHGJHKJL;</p>', '1611833339.png', 1, '2021-01-17 03:25:16', '2021-01-28 05:43:59'),
(2, NULL, 'Ingrid Justice', 'Voluptate aliqua Su', '<p>sdfghjkl;.</p>', '1611833332.png', 1, '2021-01-19 23:02:13', '2021-01-28 05:43:52'),
(3, NULL, 'Harrison Stout', 'Voluptatem sunt modi', '<p>dsfghjk</p>', '1611833324.png', 1, '2021-01-19 23:28:05', '2021-01-28 05:43:44'),
(4, NULL, 'Zelda Farley', 'Maxime recusandae I', NULL, '1611833318.png', 1, '2021-01-20 03:58:40', '2021-01-28 05:43:38'),
(5, NULL, 'Carson Wellsaaa', 'Distinctio Corrupti', '<p>aasaasassa</p>', '1611833303.png', 1, '2021-01-28 05:32:07', '2021-01-28 05:43:23');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_09_26_052751_create_images_table', 1),
(4, '2019_09_26_052820_create_videos_table', 1),
(5, '2019_09_26_052909_create_events_table', 1),
(7, '2019_11_04_051258_create_managements_table', 1),
(8, '2019_11_04_051320_create_experts_table', 1),
(9, '2019_11_04_051551_create_institutionals_table', 1),
(10, '2019_11_04_055049_create_categories_table', 1),
(11, '2019_11_04_093438_create_blogs_table', 1),
(12, '2019_11_04_103640_create_testimonials_table', 1),
(15, '2021_01_17_063754_create_sliders_table', 2),
(18, '2021_01_17_110717_create_services_table', 4),
(27, '2021_01_20_083725_create_careers_table', 6),
(32, '2021_01_24_101517_create_career_applies_table', 7),
(34, '2019_10_14_053328_create_contacts_table', 8),
(36, '2021_01_17_092513_create_ratings_table', 9),
(37, '2021_01_19_094740_create_web_infos_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `industry` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instrument_category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `outlook` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pdf` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`id`, `company_name`, `industry`, `instrument_category`, `rating`, `outlook`, `details`, `pdf`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Perez and Boone Associates', 'Bender and Cannon Plc', 'Proident qui iste q', 'Ramirez and Mosley Trading', '<p>asasasasa</p>', '<p>sasasasasas</p>', '1611825592.recipt.pdf', 1, '2021-01-28 03:33:49', '2021-01-28 03:33:49'),
(2, 'Perez and Boone Associates', 'Bender and Cannon Plc', 'Proident qui iste q', 'Ramirez and Mosley Trading', '<p>asasasasa</p>', '<p>sasasasasas</p>', '1611825592.recipt.pdf', 1, '2021-01-28 03:34:52', '2021-01-28 04:00:21'),
(3, 'its okey now', 'Bender and Cannon Plc', 'Proident qui iste q', 'Ramirez and Mosley Trading', '<p>asasasasa</p>', '<p>sasasasasas</p>', '1611825592.recipt.pdf', 1, '2021-01-28 04:06:49', '2021-01-28 04:08:17'),
(4, 'yuppy', 'Bender and Cannon Plc', 'Proident qui iste q', 'Ramirez and Mosley Trading', '<p>asasasasa</p>', '<p>sasasasasas</p>', '1611825592.recipt.pdf', 1, '2021-01-28 04:06:55', '2021-01-28 04:06:55'),
(5, 'Infomerics Nepal Pvt. Ltd.', 'Credit Rating', 'Long Term', 'INMR BBB-A', '<p class=\"fw-bold h4 text-primary-dark mb-0\" style=\"box-sizing: border-box; margin-top: 0px; line-height: 1; color: #001d2c; font-size: 1.5625rem; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; background-color: #e6f4fc; margin-bottom: 0px !important; font-weight: 700 !important;\">Stable</p>\r\n<p class=\"small text-muted\" style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-size: 0.875em; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; background-color: #e6f4fc; color: #7e8f99 !important;\">as of January 04, 2021</p>', '<ul class=\"ps-3 mb-0 small\" style=\"box-sizing: border-box; margin-top: 0px; font-size: 0.875em; color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; background-color: #e6f4fc; padding-left: 1rem !important; margin-bottom: 0px !important;\">\r\n<li style=\"box-sizing: border-box;\">Rs. .5 Crore Proposed Long Term Bank Loan Facility</li>\r\n<li style=\"box-sizing: border-box;\">Rs. 15 Crore Cash Credit</li>\r\n</ul>', '1611825592.recipt.pdf', 1, '2021-01-28 04:07:07', '2021-01-28 04:15:32');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `button_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `button_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `icon`, `description`, `button_name`, `button_url`, `created_at`, `updated_at`) VALUES
(2, 'Non-Banking Financial Company (NBFC)', 'service1611654585.png', '<p>On a macro basis NBFCs supplement the role of banks and i most cases are working in partnership with them. Some of the lending segments for these companies include.</p>\r\n<p>Possimus quod minus delectus, nostrum iure ad earum nesciunt vero repellendus ducimus quibusdam perspiciatis sapiente repudiandae asperiores, hic! Ut error ullam quas dolore maxime cum vitae, id in ipsum aliquid corporis alias?</p>\r\n<p>&nbsp;</p>\r\n<p>Nam omnis corporis cupiditate est accusantium labore ipsum atque modi nobis molestiae ipsa voluptates quae numquam, quidem, a, temporibus magnam pariatur neque odit veniam iure reprehenderit quos! Delectus dolor exercitationem voluptates consequuntur eligendi amet beatae rem minima dignissimos reprehenderit nam in doloremque, rerum vero obcaecati. Accusamus nobis, atque corporis animi eveniet dicta ad accusantium obcaecati minus magni nostrum nihil temporibus rerum sunt.</p>\r\n<p>&nbsp;</p>\r\n<p>Totam hic voluptate magni, expedita molestiae! Excepturi architecto quos nostrum nobis maiores enim beatae, harum ad ab cum. Tempora delectus facere quia.</p>\r\n<p>&nbsp;</p>\r\n<p>Hic explicabo mollitia dolores omnis saepe blanditiis. A eum labore deserunt rem repellat earum eaque aperiam molestias enim. Modi, aut. Animi voluptas vitae suscipit, quae? Placeat recusandae dolor culpa officiis ipsum laboriosam, porro pariatur reprehenderit, reiciendis, nesciunt veritatis consectetur quam aspernatur numquam.</p>\r\n<p>&nbsp;</p>\r\n<p>Nam omnis corporis cupiditate est accusantium labore ipsum atque modi nobis molestiae ipsa voluptates quae numquam, quidem, a, temporibus magnam pariatur neque odit veniam iure reprehenderit quos! Delectus dolor exercitationem voluptates consequuntur eligendi amet beatae rem minima dignissimos reprehenderit nam in doloremque, rerum vero obcaecati. Accusamus nobis, atque corporis animi eveniet dicta ad accusantium obcaecati minus magni nostrum nihil temporibus rerum sunt.</p>\r\n<p>&nbsp;</p>\r\n<p>Hic explicabo mollitia dolores omnis saepe blanditiis. A eum labore deserunt rem repellat earum eaque aperiam molestias enim. Modi, aut. Animi voluptas vitae suscipit, quae? Placeat recusandae dolor culpa officiis ipsum laboriosam, porro pariatur reprehenderit, reiciendis, nesciunt veritatis consectetur quam aspernatur numquam.</p>\r\n<p>&nbsp;</p>\r\n<p>Possimus quod minus delectus, nostrum iure ad earum nesciunt vero repellendus ducimus quibusdam perspiciatis sapiente repudiandae asperiores, hic! Ut error ullam quas dolore maxime cum vitae, id in ipsum aliquid corporis alias?</p>', 'Read More', '/services', '2021-01-17 23:04:03', '2021-01-26 23:31:58'),
(3, 'Bank Loan Rating (BLR)', 'service1611654562.png', '<p>A bank loan rating (BLR) indicates the degree of risk regarding timely servicing of the bank facility being rated; the facility includes principal and interest, if any problem please feel free to contact us.</p>\r\n<p>Possimus quod minus delectus, nostrum iure ad earum nesciunt vero repellendus ducimus quibusdam perspiciatis sapiente repudiandae asperiores, hic! Ut error ullam quas dolore maxime cum vitae, id in ipsum aliquid corporis alias?</p>\r\n<p>&nbsp;</p>\r\n<p>Nam omnis corporis cupiditate est accusantium labore ipsum atque modi nobis molestiae ipsa voluptates quae numquam, quidem, a, temporibus magnam pariatur neque odit veniam iure reprehenderit quos! Delectus dolor exercitationem voluptates consequuntur eligendi amet beatae rem minima dignissimos reprehenderit nam in doloremque, rerum vero obcaecati. Accusamus nobis, atque corporis animi eveniet dicta ad accusantium obcaecati minus magni nostrum nihil temporibus rerum sunt.</p>\r\n<p>&nbsp;</p>\r\n<p>Totam hic voluptate magni, expedita molestiae! Excepturi architecto quos nostrum nobis maiores enim beatae, harum ad ab cum. Tempora delectus facere quia.</p>\r\n<p>&nbsp;</p>\r\n<p>Hic explicabo mollitia dolores omnis saepe blanditiis. A eum labore deserunt rem repellat earum eaque aperiam molestias enim. Modi, aut. Animi voluptas vitae suscipit, quae? Placeat recusandae dolor culpa officiis ipsum laboriosam, porro pariatur reprehenderit, reiciendis, nesciunt veritatis consectetur quam aspernatur numquam.</p>\r\n<p>&nbsp;</p>\r\n<p>Nam omnis corporis cupiditate est accusantium labore ipsum atque modi nobis molestiae ipsa voluptates quae numquam, quidem, a, temporibus magnam pariatur neque odit veniam iure reprehenderit quos! Delectus dolor exercitationem voluptates consequuntur eligendi amet beatae rem minima dignissimos reprehenderit nam in doloremque, rerum vero obcaecati. Accusamus nobis, atque corporis animi eveniet dicta ad accusantium obcaecati minus magni nostrum nihil temporibus rerum sunt.</p>\r\n<p>&nbsp;</p>\r\n<p>Hic explicabo mollitia dolores omnis saepe blanditiis. A eum labore deserunt rem repellat earum eaque aperiam molestias enim. Modi, aut. Animi voluptas vitae suscipit, quae? Placeat recusandae dolor culpa officiis ipsum laboriosam, porro pariatur reprehenderit, reiciendis, nesciunt veritatis consectetur quam aspernatur numquam.</p>\r\n<p>&nbsp;</p>\r\n<p>Possimus quod minus delectus, nostrum iure ad earum nesciunt vero repellendus ducimus quibusdam perspiciatis sapiente repudiandae asperiores, hic! Ut error ullam quas dolore maxime cum vitae, id in ipsum aliquid corporis alias?</p>', 'Read More', '/services', '2021-01-18 01:03:37', '2021-01-26 23:31:52'),
(4, 'Insurance', 'service1611655992.png', '<p>On a macro basis NBFCs supplement the role of banks and i most cases are working in partnership with them. Some of the lending segments for these companies include.</p>\r\n<p>Possimus quod minus delectus, nostrum iure ad earum nesciunt vero repellendus ducimus quibusdam perspiciatis sapiente repudiandae asperiores, hic! Ut error ullam quas dolore maxime cum vitae, id in ipsum aliquid corporis alias?</p>\r\n<p>&nbsp;</p>\r\n<p>Nam omnis corporis cupiditate est accusantium labore ipsum atque modi nobis molestiae ipsa voluptates quae numquam, quidem, a, temporibus magnam pariatur neque odit veniam iure reprehenderit quos! Delectus dolor exercitationem voluptates consequuntur eligendi amet beatae rem minima dignissimos reprehenderit nam in doloremque, rerum vero obcaecati. Accusamus nobis, atque corporis animi eveniet dicta ad accusantium obcaecati minus magni nostrum nihil temporibus rerum sunt.</p>\r\n<p>&nbsp;</p>\r\n<p>Totam hic voluptate magni, expedita molestiae! Excepturi architecto quos nostrum nobis maiores enim beatae, harum ad ab cum. Tempora delectus facere quia.</p>\r\n<p>&nbsp;</p>\r\n<p>Hic explicabo mollitia dolores omnis saepe blanditiis. A eum labore deserunt rem repellat earum eaque aperiam molestias enim. Modi, aut. Animi voluptas vitae suscipit, quae? Placeat recusandae dolor culpa officiis ipsum laboriosam, porro pariatur reprehenderit, reiciendis, nesciunt veritatis consectetur quam aspernatur numquam.</p>\r\n<p>&nbsp;</p>\r\n<p>Nam omnis corporis cupiditate est accusantium labore ipsum atque modi nobis molestiae ipsa voluptates quae numquam, quidem, a, temporibus magnam pariatur neque odit veniam iure reprehenderit quos! Delectus dolor exercitationem voluptates consequuntur eligendi amet beatae rem minima dignissimos reprehenderit nam in doloremque, rerum vero obcaecati. Accusamus nobis, atque corporis animi eveniet dicta ad accusantium obcaecati minus magni nostrum nihil temporibus rerum sunt.</p>\r\n<p>&nbsp;</p>\r\n<p>Hic explicabo mollitia dolores omnis saepe blanditiis. A eum labore deserunt rem repellat earum eaque aperiam molestias enim. Modi, aut. Animi voluptas vitae suscipit, quae? Placeat recusandae dolor culpa officiis ipsum laboriosam, porro pariatur reprehenderit, reiciendis, nesciunt veritatis consectetur quam aspernatur numquam.</p>\r\n<p>&nbsp;</p>\r\n<p>Possimus quod minus delectus, nostrum iure ad earum nesciunt vero repellendus ducimus quibusdam perspiciatis sapiente repudiandae asperiores, hic! Ut error ullam quas dolore maxime cum vitae, id in ipsum aliquid corporis alias?</p>', 'Read More', '/services', '2021-01-26 04:28:12', '2021-01-26 23:31:40'),
(5, 'Public Ipo Rating', 'service1611723430.png', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>Possimus quod minus delectus, nostrum iure ad earum nesciunt vero repellendus ducimus quibusdam perspiciatis sapiente repudiandae asperiores, hic! Ut error ullam quas dolore maxime cum vitae, id in ipsum aliquid corporis alias?</p>\r\n<p>&nbsp;</p>\r\n<p>Nam omnis corporis cupiditate est accusantium labore ipsum atque modi nobis molestiae ipsa voluptates quae numquam, quidem, a, temporibus magnam pariatur neque odit veniam iure reprehenderit quos! Delectus dolor exercitationem voluptates consequuntur eligendi amet beatae rem minima dignissimos reprehenderit nam in doloremque, rerum vero obcaecati. Accusamus nobis, atque corporis animi eveniet dicta ad accusantium obcaecati minus magni nostrum nihil temporibus rerum sunt.</p>\r\n<p>&nbsp;</p>\r\n<p>Totam hic voluptate magni, expedita molestiae! Excepturi architecto quos nostrum nobis maiores enim beatae, harum ad ab cum. Tempora delectus facere quia.</p>\r\n<p>&nbsp;</p>\r\n<p>Hic explicabo mollitia dolores omnis saepe blanditiis. A eum labore deserunt rem repellat earum eaque aperiam molestias enim. Modi, aut. Animi voluptas vitae suscipit, quae? Placeat recusandae dolor culpa officiis ipsum laboriosam, porro pariatur reprehenderit, reiciendis, nesciunt veritatis consectetur quam aspernatur numquam.</p>\r\n<p>&nbsp;</p>\r\n<p>Nam omnis corporis cupiditate est accusantium labore ipsum atque modi nobis molestiae ipsa voluptates quae numquam, quidem, a, temporibus magnam pariatur neque odit veniam iure reprehenderit quos! Delectus dolor exercitationem voluptates consequuntur eligendi amet beatae rem minima dignissimos reprehenderit nam in doloremque, rerum vero obcaecati. Accusamus nobis, atque corporis animi eveniet dicta ad accusantium obcaecati minus magni nostrum nihil temporibus rerum sunt.</p>\r\n<p>&nbsp;</p>\r\n<p>Hic explicabo mollitia dolores omnis saepe blanditiis. A eum labore deserunt rem repellat earum eaque aperiam molestias enim. Modi, aut. Animi voluptas vitae suscipit, quae? Placeat recusandae dolor culpa officiis ipsum laboriosam, porro pariatur reprehenderit, reiciendis, nesciunt veritatis consectetur quam aspernatur numquam.</p>\r\n<p>&nbsp;</p>\r\n<p>Possimus quod minus delectus, nostrum iure ad earum nesciunt vero repellendus ducimus quibusdam perspiciatis sapiente repudiandae asperiores, hic! Ut error ullam quas dolore maxime cum vitae, id in ipsum aliquid corporis alias?</p>', 'Read More', '/services', '2021-01-26 23:12:10', '2021-01-26 23:31:32');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slider_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `button_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `slider_image`, `title`, `description`, `url_link`, `button_name`, `created_at`, `updated_at`) VALUES
(1, '1610872205.jpg', 'Title Goes Here', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nec pulvinar sapien. Quisque a turpis lacus. Etiam ac bibendum libero. Sed maximus hendrerit tortor et iaculis. Morbi eros justo, consectetur id metus vel, dictum aliquet turpis. Mauris fermentum lacus vitae nibh molestie hendrerit. Donec quis libero ultrices, auctor nulla viverra, commodo augue. Vestibulum metus ligula, scelerisque sed ipsum ac, auctor porttitor quam. Etiam nec ultrices quam, sit amet malesuada odio. Ut nec libero et nisl egestas egestas porta non ligula. Nam semper nisl lorem, in vulputate ligula tempor eget. Nunc bibendum enim ligula. Pellentesque vitae auctor tortor.</p>', '/about', 'About', '2021-01-17 02:45:05', '2021-01-26 06:02:09'),
(3, '1610873222.jpg', 'Title Goes Here', '<p>Semper sem vitae cursus magna. Vitae, diam felis risus praesent. Scelerisque scelerisque dictum dignissim nulla ornare.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nec pulvinar sapien. Quisque a turpis lacus. Etiam ac bibendum libero. Sed maximus hendrerit tortor et iaculis. Morbi eros justo, consectetur id metus vel, dictum aliquet turpis. Mauris fermentum lacus vitae nibh molestie hendrerit. Donec quis libero ultrices, auctor nulla viverra, commodo augue. Vestibulum metus ligula, scelerisque sed ipsum ac, auctor porttitor quam. Etiam nec ultrices quam, sit amet malesuada odio. Ut nec libero et nisl egestas egestas porta non ligula. Nam semper nisl lorem, in vulputate ligula tempor eget.&nbsp;</p>', '/services', 'Services', '2021-01-17 03:02:02', '2021-01-27 05:23:53');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `designation`, `testimonial`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Lee Carr', 'Perspiciatis conseq', '<p>hello test</p>', '1610875383.png', 1, '2021-01-17 03:38:03', '2021-01-17 03:38:03');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', NULL, '$2y$10$KQs3UYMV/uc2ZiD7aPdx6uzTLunoHY8xmEpp6g880b9U5Ah6Oj8ay', 'RpjzqrXGmmqUMNJj5e2b8Ft58WwHcmTYN3zsosFTyCW0woLLz7sJA6pg0m56', '2021-01-17 00:49:23', '2021-01-17 00:49:23');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `web_infos`
--

CREATE TABLE `web_infos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `our_mission` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `our_mission_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `our_vision` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `our_vision_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `life_at_infomerics` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `life_at_infomerics_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `our_people` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `our_people_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo_white` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_us` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_us_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nepal_contact` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `india_contact` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `map` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `web_infos`
--

INSERT INTO `web_infos` (`id`, `our_mission`, `our_mission_image`, `our_vision`, `our_vision_image`, `life_at_infomerics`, `life_at_infomerics_image`, `our_people`, `our_people_image`, `logo`, `logo_white`, `mail`, `meta_keywords`, `meta_description`, `about_us`, `about_us_image`, `nepal_contact`, `india_contact`, `map`, `rating`, `web_title`, `text1`, `text2`, `created_at`, `updated_at`) VALUES
(1, '<p>Molestiae autem facilis nisi architecto dolore ducimus? Fugiat sed obcaecati impedit magni quod itaque praesentium eius, ipsam, voluptates quo dignissimos facilis, cum! Dolore natus doloremque adipisci minus labore maxime harum commodi perferendis eligendi tenetur iusto accusamus voluptatem sit facilis facere nulla temporibus recusandae, cumque veniam. Adipisci id perferendis ea dolore? Ipsa, quasi.</p>\r\n<p>Ut dolorum fugiat ipsa, cupiditate quibusdam eveniet sed perferendis commodi autem, dolores vero ea mollitia aliquam rerum, eius, illum et minima atque? Nobis eligendi accusamus porro quisquam quae. Tempora repellendus sunt quibusdam, consequatur at modi.</p>', 'mission1611897053.jpg', '<p>Molestiae autem facilis nisi architecto dolore ducimus? Fugiat sed obcaecati impedit magni quod itaque praesentium eius, ipsam, voluptates quo dignissimos facilis, cum! Dolore natus doloremque adipisci minus labore maxime harum commodi perferendis eligendi tenetur iusto accusamus voluptatem sit facilis facere nulla temporibus recusandae, cumque veniam. Adipisci id perferendis ea dolore? Ipsa, quasi.</p>\r\n<p>Ut dolorum fugiat ipsa, cupiditate quibusdam eveniet sed perferendis commodi autem, dolores vero ea mollitia aliquam rerum, eius, illum et minima atque? Nobis eligendi accusamus porro quisquam quae. Tempora repellendus sunt quibusdam, consequatur at modi.</p>', 'vision1611897053.jpg', '<p>Accusamus veritatis expedita odit, in ab, suscipit repellat tempora voluptatem perspiciatis qui, a repellendus sint corporis. Expedita eos dolores ullam asperiores, dolore sapiente tenetur nostrum dolor, error neque quisquam ipsa inventore. Corporis eveniet at unde, sequi minus expedita rerum perspiciatis hic distinctio fuga vel sint incidunt quos animi. Voluptatem exercitationem ad, incidunt.</p>\r\n<p>&nbsp;</p>\r\n<p>Semper sem vitae cursus magna. Vitae, diam felis risus praesent. Scelerisque scelerisque dictum dignissim nulla ornare. Vitae et vel odio tincidunt viverra rhoncus. Erat nunc, interdum aenean cras orci, auctor sagittis nunc. Volutpat, magnis et a et gravida ornare.</p>', 'life1611897215.jpg', '<p>Accusamus veritatis expedita odit, in ab, suscipit repellat tempora voluptatem perspiciatis qui, a repellendus sint corporis. Expedita eos dolores ullam asperiores, dolore sapiente tenetur nostrum dolor, error neque quisquam ipsa inventore. Corporis eveniet at unde, sequi minus expedita rerum perspiciatis hic distinctio fuga vel sint incidunt quos animi. Voluptatem exercitationem ad, incidunt.</p>\r\n<p>Semper sem vitae cursus magna. Vitae, diam felis risus praesent. Scelerisque scelerisque dictum dignissim nulla ornare. Vitae et vel odio tincidunt viverra rhoncus. Erat nunc, interdum aenean cras orci, auctor sagittis nunc. Volutpat, magnis et a et gravida ornare.</p>', 'people1611897215.jpg', 'logo1611860068.png', 'logo_white1611860068.png', 'azizdulal.ad@gmail.com', 'infomerics, info', 'this is infomerics', '<p>Semper sem vitae cursus magna. Vitae, diam felis risus praesent. Scelerisque scelerisque dictum dignissim nulla ornare. Vitae et vel odio tincidunt viverra rhoncus. Erat nunc, interdum aenean cras orci, auctor sagittis nunc. Volutpat, magnis et a et gravida ornare. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>&nbsp;</p>\r\n<p>Accusamus veritatis expedita odit, in ab, suscipit repellat tempora voluptatem perspiciatis qui, a repellendus sint corporis. Expedita eos dolores ullam asperiores, dolore sapiente tenetur nostrum dolor, error neque quisquam ipsa inventore. Corporis eveniet at unde, sequi minus expedita rerum perspiciatis hic distinctio fuga vel sint incidunt quos animi. Voluptatem exercitationem ad, incidun.</p>', 'info1611862158.jpg', '<p><span style=\"color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; background-color: #ffffff;\">Street Address, House no.</span><br style=\"box-sizing: border-box; color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; background-color: #ffffff;\" /><span style=\"color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; background-color: #ffffff;\">Lalitpur, Nepal</span><br style=\"box-sizing: border-box; color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; background-color: #ffffff;\" /><span style=\"box-sizing: border-box; font-weight: bolder; color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; background-color: #ffffff;\">Phone:</span><span style=\"color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; background-color: #ffffff;\">&nbsp;+977 1 4412345</span><br style=\"box-sizing: border-box; color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; background-color: #ffffff;\" /><span style=\"box-sizing: border-box; font-weight: bolder; color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; background-color: #ffffff;\">Email:</span><span style=\"color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; background-color: #ffffff;\">&nbsp;info@infomericsnepal.com.np</span></p>', '<p><span style=\"color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; background-color: #ffffff;\">Street Address, House no.</span><br style=\"box-sizing: border-box; color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; background-color: #ffffff;\" /><span style=\"color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; background-color: #ffffff;\">Delhi, India</span><br style=\"box-sizing: border-box; color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; background-color: #ffffff;\" /><span style=\"box-sizing: border-box; font-weight: bolder; color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; background-color: #ffffff;\">Phone:</span><span style=\"color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; background-color: #ffffff;\">&nbsp;+91 123 456 7890</span><br style=\"box-sizing: border-box; color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; background-color: #ffffff;\" /><span style=\"box-sizing: border-box; font-weight: bolder; color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; background-color: #ffffff;\">Email:</span><span style=\"color: #546066; font-family: Compose, system-ui, -apple-system, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, \'Noto Sans\', \'Liberation Sans\', sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; background-color: #ffffff;\">&nbsp;info@infomerics.com</span></p>', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14130.946795029693!2d85.34463115!3d27.694531700000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2snp!4v1611221212090!5m2!1sen!2snp\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>', '<p>Ashish Similique pariatur repellat at officiis. Dolorum earum, in aspernatur provident porro fugit et quae ad atque non animi quaerat, omnis iure aut exercitationem eveniet beatae quia consectetur sint quisquam.</p>', 'Infomerics Credit Rating Nepal Ltd', '<p><span style=\"color: #3598db;\">Volutpat convallis lorem vitae augue. Urna, tristique sit sed arcu malesuada viverra id sed. Sit mattis pellentesque luctus quis aenean aliquet.</span></p>\r\n<p>Semper sem vitae cursus magna. Vitae, diam felis risus praesent. Scelerisque scelerisque dictum dignissim nulla ornare. Vitae et vel odio tincidunt viverra rhoncus. Erat nunc, interdum aenean cras orci, auctor sagittis nunc. Volutpat, magnis et a et gravida ornare. Purus ornare aliquet at tincidunt. Suspendisse orci.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis ipsam numquam, facere quos aliquam culpa, quia et consequuntur voluptates architecto est quaerat voluptatem optio provident repellendus ad rem.</p>', NULL, '2021-01-31 04:44:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `career_applies`
--
ALTER TABLE `career_applies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `experts`
--
ALTER TABLE `experts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institutionals`
--
ALTER TABLE `institutionals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `managements`
--
ALTER TABLE `managements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_infos`
--
ALTER TABLE `web_infos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `career_applies`
--
ALTER TABLE `career_applies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `experts`
--
ALTER TABLE `experts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `institutionals`
--
ALTER TABLE `institutionals`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `managements`
--
ALTER TABLE `managements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `web_infos`
--
ALTER TABLE `web_infos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
