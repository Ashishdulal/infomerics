 <div class="footer">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 site-logos">
        <p class="mb-4"><img src="images/new images/logo mercyland.png" alt="Image" class="img-fluid"></p>
        <p class=" text text-footer">Mercyland Foundation is an Evangelical Christian trust with a vision of building the KINGDOM OF GOD..... <a href="about-mercyland.php">Read More</a></p>  
      </div>
      <div class="col-lg-3">
        <h3 class="footer-heading"><span>Quick links</span></h3>
        <ul class="list-unstyled">
          <li><a href="#">About Mercyland</a></li>
          <li><a href="#">Our Mission</a></li>
          <li><a href="#">Message from Trustee</a></li>
          <li><a href="#">Message from Director</a></li>
          <li><a href="#">News & Events</a></li>
          <li><a href="#">Donate US</a></li>           
        </ul>
      </div>
      <div class="col-lg-4">
        <h3 class="footer-heading"><span>Contact Us</span></h3>
        <p class="text"><span>Address :</span> Chabahil-7, Kathmandu, Nepal </p>
        <p class="text"><span> Phone :</span> +977 9851227632</p>
        <p class="text"><span> Email : </span>info@mercylandfoundation.org</p>
        <p class="text"><span>      &ensp;&ensp;  &ensp; &ensp; </span>foundationmercyland@gmail.com</p>
      </div>

      <div class="col-lg-2">
        <h3 class="footer-heading"><span>Also Find Us at:</span></h3>

        <div class="ml-auto">
          <div class="social-wrap">
            <a href="#"><span class="icon-facebook"></span></a>
            <a href="#"><span class="icon-twitter"></span></a>
            <a href="#"><span class="icon-linkedin"></span></a>

            <a href="#" class="d-inline-block d-lg-none site-menu-toggle js-menu-toggle text-black"><span
              class="icon-menu h3"></span></a>
            </div>
          </div>
        </div>

      </div>

      <div class="row">
        <div class="col-12">
          <div class="copyright">
            <p>
    
              Copyright &copy;2019 All rights reserved |<i aria-hidden="true"></i> <a href="https://www.quackfoot.com" target="_blank" > Designed By: Quackfoot</a>
             
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>


</div>
<!-- .site-wrap -->


<!-- loader -->
<div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#51be78"/></svg></div>

<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/jquery-migrate-3.0.1.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/aos.js"></script>
<script src="js/jquery.fancybox.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/jquery.mb.YTPlayer.min.js"></script>




<script src="js/main.js"></script>

</body>

</html>