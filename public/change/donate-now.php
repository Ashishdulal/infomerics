<?php include('header.php'); ?>

    
    <div class="site-section  site-blocks-cover pag-title">
        <div class="container">
          <div class="row align-items-end">
            <div class="col-lg-7 colo">
              <h2 class="mb-0">Donate US</h2>
              <p>Your small donation can help someone.</p>
            </div>
          </div>
        </div>
      </div> 
    

    <div class="custom-breadcrumns border-bottom">
      <div class="container">
        <a href="index.html">Home</a>
        <span class="mx-3 icon-keyboard_arrow_right"></span>
        <span class="current">Donate US</span>
      </div>
    </div>

    <div class="site-section">
        <div class="container">
            <div class="row mb-5">
                <div class="col-lg-6 mb-lg-0 mb-4">
                    <a href="mercyland-hospital.php"><img src="images/new images/CITIZENBANKLOGO.png" alt="Image" class="img-fluid"> </a>
                </div>
                <div class="col-lg-5 ml-auto align-self-center">
                    <h2 class="section-title-underline mb-2">
                        Citizen Bank International Ltd.
                    </h2>
                    <h3 class="section-title-underline mb-2" >Account Holder Name: Mercyland Foundation</h3>
                    <h4 class="section-title-underline mb-2">Account Number: 0540100000109201</h4>
                    <h4 class="section-title-underline mb-2">CTZNNPKA</h4>

                </div>
            </div>

           
                    </div>
                </div> 
        </div>
    </div>

  
      
<?php include('footer.php'); ?>