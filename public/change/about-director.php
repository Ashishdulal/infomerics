
<?php include('header.php'); ?>
<div class="site-section  site-blocks-cover pag-title">
  <div class="container">
    <div class="row align-items-end">
      <div class="col-lg-7 colo">
        <h2 class="mb-0"><span>About Mercyland Foundation<span></h2>
        <p>Find more in details about Mercyland Foundation</p>
      </div>
    </div>
  </div>
</div> 


<div class="custom-breadcrumns border-bottom">
  <div class="container">
    <a href="#">Home</a>
    <span class="mx-3 icon-keyboard_arrow_right"></span>
    <a href="#">About Us</a>
    <span class="mx-3 icon-keyboard_arrow_right"></span>
    <span class="current">About Director</span>
  </div>
</div>

<div class="container pt-5 mb-5">
  <div class="row">
    <div class="col-lg-4">
      <h2 class="section-title-underline">
      <span>Prakash Story</span>
      </h2>
    </div>
    <div class="col-lg-8">
      <p>Prakash spent some of his precious years as a street kid collecting garbages for the scrap dealer until he was picked up by an American Baptist Missionary Martin Jonathan Collins who put him in a Children's Home where he received Jesus as his Lord and Savior. Collins along with his wife and five children were American missionaries from Good News Baptist Church in South Carolina to Nepal who were all killed in a tragic plane crash on July 11, 1990 while traveling to Nepal via Thia Airlines.</p>
    </div>
  </div>
</div>

<div class="site-section">
  <div class="container">
    <div class="row mb-5">
      <div class="col-lg-5 mb-lg-0 mb-4">
        <img src="images/new images/helpinghand.jpg" alt="Image" class="img-fluid"> 
      </div>
      <div class="col-lg-6 ml-auto align-self-center">
        <!-- <h2 class="section-title-underline mb-5">
          <span>About Prakash</span>
        </h2> -->
        <p>No one had guessed that Prakash would emerge as a promising kid with a heart for the poor and helpless people, so everyone was taken aback when he started a Community Hospital in 2010 with a view to serve the destitute and poverty-stricken people, especially of village. He conducted different health camps in the villages of Nepal to the relief of many.
        </p>
        <p>Responsive image But unfortunately, he was falsely alleged by some of his committee members who were Hindus. With the help of their political connection, they filed a case against him and he ended up in a prison and he, being innocent, spent 4 years in the prison. After he went to the prison, the hospital was shut down completely. All those years, God was really teaching him a great lesson and after he was released, he was committed to serve Him even with a great zeal and passion. He forgave all those who wronged him with Christ-like forgiveness.
        </p>
        <p>Today, Prakash is volunteering and supporting to help many poor and needy people such as mentally retarded and disabled people even though he has no means to do that.
        </p>
        <p>Dauntless, Prakash still envisions to do the community health evangelism by starting a Community Hospital with a spirit of a good Samaritan as he has witnessed the terribly precarious health situation of many village people in Nepal living behind the rugged hills and mountains who, without having the access to basic health, are dying daily.
        </p>
      </div>
    </div>


<!-- 
            <div class="row">
                    <div class="col-lg-6 order-1 order-lg-2 mb-4 mb-lg-0">
                        <img src="images/new images/mapofnepal.jpg" alt="Image" class="img-fluid"> 
                    </div>
                    <div class="col-lg-5 mr-auto align-self-center order-2 order-lg-1">
                        <h2 class="section-title-underline mb-5">
                            <span>About Nepal</span>
                        </h2>
                        <p>Nepal is a relatively small (28 million) land-locked country, bordered by the two biggest countries in the world, India and China. Its renowned physical beauty makes it very fragmented and many parts are inaccessible by modern transport and communication facilities. There are few cities and 86% of the population lives in rural areas. Despite its richness in biodiversity, natural resources and cultural multiplicity, Nepal has still remained a developing country where 70 percent of the people live under the poverty line. The impact of the poverty is manifest in all other sectors like health and hygiene, education, disaster and conflict preparedness, access to and distribution of resources, gender equity, as well as respect for humanitarian values. The transitional period of developmental change in the legal, political, financial and diplomatic scenario indicate that the country needs more efforts in the fields of social reformation, rehabilitation and development.</p>
                    </div>
                  </div> -->
                </div>
              </div>



    <?php include('footer.php'); ?>