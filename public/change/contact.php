<?php include('header.php'); ?>

 <div class="site-section  site-blocks-cover pag-title">
        <div class="container">
          <div class="row align-items-end">
            <div class="col-lg-7 colo">
              <h2 class="mb-0"><span>Contact Us<span></h2>
              <p>For any Queries Please feel free to contact Us.</p>
            </div>
          </div>
        </div>
      </div> 

<div class="custom-breadcrumns border-bottom">
  <div class="container">
    <a href="index.html">Home</a>
    <span class="mx-3 icon-keyboard_arrow_right"></span>
    <span class="current">Contact Us</span>
</div>
</div>

<div class="site-section">
    <div class="container">




      <div class="row ">
  <div class="col-md-4 text-center">
  <div class="icon text-primary">
          <span class="icon-map-o "></span>
  </div>
  <h4 class="colo">Address</h4>
  <p class="text-center">Chabahil-7, Kathmandu, Nepal</p>
    
  </div>
  <div class="col-md-4 text-center">
  <div class="icon text-primary">
          <span class="icon-mobile-phone"></span>
  </div>
  <h4 class="colo">Phone</h4>
  <p class="text-center">+977 9851227632</p>
    
  </div>
  <div class="col-md-4 text-center">
  <div class="icon text-primary">
          <span class="icon-envelope-o"></span>
  </div>
  <h4 class="colo">Email</h4>
  <p class="text-center">info@mercylandfoundation.org</br> foundationmercyland@gmail.com</p>
    
  </div>
</div>



      <form class="jumbotron">
        <div class="row ">
            <div class="col-md-6 form-group">
                <label for="fname">First Name</label>
                <input type="text" id="fname" class="form-control form-control-lg">
            </div>
            <div class="col-md-6 form-group">
                <label for="lname">Last Name</label>
                <input type="text" id="lname" class="form-control form-control-lg">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 form-group">
                <label for="eaddress">Email Address</label>
                <input type="text" id="eaddress" class="form-control form-control-lg">
            </div>
            <div class="col-md-6 form-group">
                <label for="tel">Tel. Number</label>
                <input type="text" id="tel" class="form-control form-control-lg">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 form-group">
                <label for="message">Message</label>
                <textarea name="" id="message" cols="30" rows="10" class="form-control"></textarea>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <input type="submit" value="Send Message" class="btn btn-primary btn-lg px-5">
            </div>
        </div>
        </form>
    </div>
</div>
<div class="site-section ftco-subscribe-1" style="background-image: url('images/bg_1.jpg')">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-7">
        <h2>Subscribe to us!</h2>
        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,</p>
    </div>
    <div class="col-lg-5">
        <form action="" class="d-flex">
          <input type="text" class="rounded form-control mr-2 py-3" placeholder="Enter your email">
          <button class="btn btn-primary rounded py-3 px-4" type="submit">Send</button>
      </form>
  </div>
</div>
</div>
</div> 



<?php include('footer.php'); ?>