
<?php include('header.php'); ?>
<div class="site-section  site-blocks-cover pag-title">
  <div class="container">
    <div class="row align-items-end">
      <div class="col-lg-7 colo">
        <h2 class="mb-0">News and Events</h2>
        <p>Find our Activities in detail.</p>
      </div>
    </div>
  </div>
</div> 


<div class="custom-breadcrumns border-bottom">
  <div class="container">
    <a href="#">Home</a>
    <span class="mx-3 icon-keyboard_arrow_right"></span>
    <span class="current">News and Events</span>
  </div>
</div>
<div class="container">

  <div class="row col-md-12">
    <div class="section-heading">
      <a href=""><h2 class="text-black">Latest &amp; News</h2></a>
    </div>
  </div>

  <div class="row col-md-12">
    <div class="col-md-4">
      <div class="row news-border">

        <div class="row post-entry-big horizontal d-flex mb-4 padding-news">
          <div class="col-md-12">
            <a href="news-single-2.php" class="img-fluid mr-4"><img src="images/new images/nepleasewomen.jpg" alt="Image" class="news-list-image"></a>
          </div>
          <div class="post-content col-md-12">
            <div class="post-meta">
              <a href="#">June 6, 2019</a>
              <span class="mx-1">/</span>
              <a href="news-single-2.php">Rural Nepal</a>, <a href="news-single-2.php">Womens Health</a>
            </div>
            <h3 class="post-heading"><a href="news-single-2.php">Rural women hide their diseases in Nepal</a></h3>
            <p>This elderly lady suffering with bone cancer belonged to what is considered to be a backward tribe in Nepal. This particular tribe is so poverty-stricken that they go to jungles looking for eatable wild products to survive.<br>  read more....</p>


          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="row news-border">

        <div class="row post-entry-big horizontal d-flex mb-4 padding-news">
          <div class="col-md-12">
            <a href="news-single-2.php" class="img-fluid mb-14 mr-4"><img src="images/new images/nepleasewomen.jpg" alt="Image" class="news-list-image"></a>
          </div>
          <div class="post-content col-md-12">
            <div class="post-meta">
              <a href="#">June 6, 2019</a>
              <span class="mx-1">/</span>
              <a href="news-single-2.php">Rural Nepal</a>, <a href="news-single-2.php">Womens Health</a>
            </div>
            <h3 class="post-heading"><a href="news-single-2.php">Rural women hide their diseases in Nepal</a></h3>
            <p>This elderly lady suffering with bone cancer belonged to what is considered to be a backward tribe in Nepal. This particular tribe is so poverty-stricken that they go to jungles looking for eatable wild products to survive.<br>  read more....</p>


          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="row news-border">

        <div class="row post-entry-big horizontal d-flex mb-4 padding-news">
          <div class="col-md-12">
            <a href="news-single-2.php" class="img-fluid mr-4"><img src="images/new images/nepleasewomen.jpg" alt="Image" class="news-list-image"></a>
          </div>
          <div class="post-content col-md-12">
            <div class="post-meta">
              <a href="#">June 6, 2019</a>
              <span class="mx-1">/</span>
              <a href="news-single-2.php">Rural Nepal</a>, <a href="news-single-2.php">Womens Health</a>
            </div>
            <h3 class="post-heading"><a href="news-single-2.php">Rural women hide their diseases in Nepal</a></h3>
            <p>This elderly lady suffering with bone cancer belonged to what is considered to be a backward tribe in Nepal. This particular tribe is so poverty-stricken that they go to jungles looking for eatable wild products to survive.<br>  read more....</p>


          </div>
        </div>
      </div>
    </div>
  </div>
  </div>



  <?php include('footer.php'); ?>