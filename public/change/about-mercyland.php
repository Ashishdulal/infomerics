
<?php include('header.php'); ?>
<div class="site-section  site-blocks-cover pag-title">
  <div class="container">
    <div class="row align-items-end">
      <div class="col-lg-7 colo">
        <h2 class="mb-0"><span>About Mercyland Foundation<span></h2>
        <p>Find more in details about Mercyland Foundation</p>
      </div>
    </div>
  </div>
</div> 


<div class="custom-breadcrumns border-bottom">
  <div class="container">
    <a href="#">Home</a>
    <span class="mx-3 icon-keyboard_arrow_right"></span>
    <a href="#">About Us</a>
    <span class="mx-3 icon-keyboard_arrow_right"></span>
    <span class="current">Mercyland Foudation</span>
  </div>
</div>

<div class="container pt-5 mb-5">
  <div class="row">
    <div class="col-lg-4">
      <h2 class="section-title-underline">
        <span>About Mercyland Foundation</span>
      </h2>
    </div>
    <div class="col-lg-4">
      <p>Mercyland Foundation will be a 100 bedded general community hospital with
        super specialty services in NCDs like cardiovascular disease, diabetes,
        hypertension, pulmonary disease, cancer and mental health. It will provide general
        hospital services focusing on child and maternal health, surgical care, geriatric care,
        emergency care to promote the society to be physically, mentally, socially,
        economically and spiritually healthy. Mercyland Foundation aims to provide the best
        modern technology health care to the general people at its coverage at the most
        reasonable and affordable price. 
      </p>
    </div>
    <div class="col-lg-4">
      <p>
        Poor people who are otherwise inaccessible to the
        expensive health care will be provided care at minimal cost or care at free of cost. It
        will focus on recent advances in the health care and technology. It will also focus on
        holistic approach to the disease. The major focus on disease spectrum will be NCDs.
        State of the art technology with recent advances in health care will be introduced to
        treat these diseases. At the same time, it will have fusion of alternative medicine,
        Ayurvedic medicine and other eastern traditional medical practices with the western
        medical practice.
      </p>
    </div>
  </div>
</div> -->

<div class="site-section">
  <div class="container">
    <div class="row mb-5">
      <div class="col-lg-5 mb-lg-0 mb-4">
        <img src="images/new images/saradha.jpg" alt="Image" class="img-fluid"> 
      </div>
      <div class="col-lg-7 ml-auto align-self-center">
        <h2 class="section-title-underline mb-5">
          <span>Message from Trustee</span>
        </h2>
        <p>
          After establishment of printing press, it is my pleasure to announce that our “Mercyland Foundation” is going to set up a community hospital in the near future. There is no need to mention that people in Nepal and neighboring countries die of various communicable and non-communicable diseases due to lack of health knowledge and resources. Cure for diseases are expensive especially in the case of diseases like cancer, cardio-vascular diseases, kidney failure etc. throughout the subcontinent.
          At the same time, the old malice of deaths due to simple communicable diseases is still rampant in remote rural areas of the subcontinent. This is mostly because lack of even a general level of medical service there. Deaths of mothers during delivery are still becoming news in present day time of science and technology.
          I am deeply moved by such dire situation. Hence, I have come forward with a project of running a community hospital and expand its services. I truly believe that with it we can reverse the situation and provide joyful lives to our fellow men and women</p>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12 mr-auto align-self-center order-2 order-lg-1">
          <h2 class="section-title-underline mb-5">
            <span>About Sarada</span>
          </h2>
          <p>Hailed from Ghodaghodi municipality ward no. 01, Kailali of Far-western Nepal, Sarada Neure is an emerging social worker in Nepal. Moved by ignorance illiteracy, superstitions and ill practices like “Chhaupadi” in Nepal. She came to do something worthy for Nepali people. For that she came to kathmadu in 2011 CE and joined Tribhuwan University for Master’s degree. While studying at the University, she along with some activists established an NGO called “Free New Life Nepal” to work for the welfare of drug users and people with HIV. She became its president and ran various programs.
            She cleared her master’s degree in English Literature and currently working as president of “Mercyland Foundation” for intense aspiration to serve those in needs led her to present position. Young, energetic and enthusiastic as she is the needy sections of the Nepali society can rely on her for their betterment.
          </p>
        </div>
      </div>
    </div>
  </div>




    <?php include('footer.php'); ?>